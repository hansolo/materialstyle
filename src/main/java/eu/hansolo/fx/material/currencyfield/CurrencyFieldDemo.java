/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.currencyfield;

import eu.hansolo.fx.material.components.DemoMaterial;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.scene.Scene;


/**
 * User: hansolo
 * Date: 25.05.16
 * Time: 08:17
 */
public class CurrencyFieldDemo extends Application {

    private CurrencyField field1;
    private CurrencyField field2;
    private CurrencyField field3;

    @Override public void init() {
        field1 = new CurrencyField();
        field2 = new CurrencyField();
        field3 = new CurrencyField();

        field1.valueProperty().bindBidirectional(field2.valueProperty());
    }

    @Override public void start(Stage stage) {
        VBox pane = new VBox(field1, field2, field3);
        pane.setSpacing(20);
        pane.setPadding(new Insets(20));

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(CurrencyFieldDemo.class.getResource("../styles.css").toExternalForm());

        stage.setTitle("Title");
        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
