package eu.hansolo.fx.material.gauges.skins;

import eu.hansolo.fx.material.gauges.Fonts;
import eu.hansolo.fx.material.gauges.Gauge;
import eu.hansolo.fx.material.gauges.Section;
import eu.hansolo.fx.material.gauges.tools.Helper;
import javafx.geometry.VPos;
import javafx.scene.CacheHint;
import javafx.scene.control.Skin;
import javafx.scene.control.SkinBase;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.FillRule;
import javafx.scene.shape.Line;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Rotate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * Created by hansolo on 02.12.16.
 */
public class TileKpiSkin extends SkinBase<Gauge> implements Skin<Gauge> {
    private static final double            PREFERRED_WIDTH  = 250;
    private static final double            PREFERRED_HEIGHT = 250;
    private static final double            MINIMUM_WIDTH    = 50;
    private static final double            MINIMUM_HEIGHT   = 50;
    private static final double            MAXIMUM_WIDTH    = 1024;
    private static final double            MAXIMUM_HEIGHT   = 1024;
    private double            size;
    private double            oldValue;
    private Arc               barBackground;
    private Arc               thresholdBar;
    private Rectangle         needleRect;
    private Path              needle;
    private Rotate            needleRotate;
    private Rotate            needleRectRotate;
    private Text              titleText;
    private Text              valueText;
    private Text              unitText;
    private Text              minValueText;
    private Text              maxValueText;
    private Rectangle         thresholdRect;
    private Text              thresholdText;
    private Pane              sectionPane;
    private Region            alertIcon;
    private Tooltip           alertTooltip;
    private Pane              pane;
    private double            angleRange;
    private double            minValue;
    private double            range;
    private double            angleStep;
    private String            formatString;
    private Locale            locale;
    private boolean           sectionsVisible;
    private boolean           highlightSections;
    private List<Section>     sections;
    private Map<Section, Arc> sectionMap;


    // ******************** Constructors **************************************
    public TileKpiSkin(Gauge gauge) {
        super(gauge);
        if (gauge.isAutoScale()) gauge.calcAutoScale();
        angleRange        = Helper.clamp(90.0, 180.0, gauge.getAngleRange());
        oldValue          = gauge.getValue();
        minValue          = gauge.getMinValue();
        range             = gauge.getRange();
        angleStep         = angleRange / range;
        formatString      = new StringBuilder("%.").append(Integer.toString(gauge.getDecimals())).append("f").toString();
        locale            = gauge.getLocale();
        sectionsVisible   = gauge.getSectionsVisible();
        highlightSections = gauge.isHighlightSections();
        sections          = gauge.getSections();
        sectionMap        = new HashMap<>(sections.size());
        for(Section section : sections) { sectionMap.put(section, new Arc()); }

        initGraphics();
        registerListeners();

        rotateNeedle(gauge.getCurrentValue());
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        // Set initial size
        if (Double.compare(getSkinnable().getPrefWidth(), 0.0) <= 0 || Double.compare(getSkinnable().getPrefHeight(), 0.0) <= 0 ||
            Double.compare(getSkinnable().getWidth(), 0.0) <= 0 || Double.compare(getSkinnable().getHeight(), 0.0) <= 0) {
            if (getSkinnable().getPrefWidth() > 0 && getSkinnable().getPrefHeight() > 0) {
                getSkinnable().setPrefSize(getSkinnable().getPrefWidth(), getSkinnable().getPrefHeight());
            } else {
                getSkinnable().setPrefSize(PREFERRED_WIDTH, PREFERRED_HEIGHT);
            }
        }

        barBackground = new Arc(PREFERRED_WIDTH * 0.5, PREFERRED_HEIGHT * 0.696, PREFERRED_WIDTH * 0.275, PREFERRED_WIDTH * 0.275, angleRange * 0.5 + 90, -angleRange);
        barBackground.setType(ArcType.OPEN);
        barBackground.setStrokeWidth(PREFERRED_WIDTH * 0.02819549 * 2);
        barBackground.setStrokeLineCap(StrokeLineCap.BUTT);
        barBackground.getStyleClass().add("gauge-bar-background");

        thresholdBar = new Arc(PREFERRED_WIDTH * 0.5, PREFERRED_HEIGHT * 0.696, PREFERRED_WIDTH * 0.275, PREFERRED_WIDTH * 0.275, -angleRange * 0.5 + 90, 0);
        thresholdBar.setType(ArcType.OPEN);
        thresholdBar.setStrokeWidth(PREFERRED_WIDTH * 0.02819549 * 2);
        thresholdBar.setStrokeLineCap(StrokeLineCap.BUTT);
        Helper.enableNode(thresholdBar, !getSkinnable().getSectionsVisible());
        thresholdBar.getStyleClass().add("gauge-threshold-bar");

        sectionPane = new Pane();
        Helper.enableNode(sectionPane, getSkinnable().getSectionsVisible());

        if (sectionsVisible) { drawSections(); }

        alertIcon = new Region();
        alertIcon.setPrefSize(PREFERRED_WIDTH * 0.155, PREFERRED_WIDTH * 0.1375);
        alertIcon.getStyleClass().add("gauge-alert-icon");
        alertIcon.setVisible(getSkinnable().isAlert());
        alertTooltip = new Tooltip(getSkinnable().getAlertMessage());
        Tooltip.install(alertIcon, alertTooltip);

        needleRotate     = new Rotate((getSkinnable().getValue() - oldValue - minValue) * angleStep);
        needleRectRotate = new Rotate((getSkinnable().getValue() - oldValue - minValue) * angleStep);

        needleRect = new Rectangle();
        needleRect.getTransforms().setAll(needleRectRotate);
        needleRect.getStyleClass().add("gauge-background");

        needle = new Path();
        needle.setFillRule(FillRule.EVEN_ODD);
        needle.getTransforms().setAll(needleRotate);
        needle.setStrokeWidth(0);
        needle.getStyleClass().add("gauge-needle");

        titleText = new Text(getSkinnable().getTitle());
        titleText.getStyleClass().add("gauge-title");
        Helper.enableNode(titleText, !getSkinnable().getTitle().isEmpty());

        valueText = new Text(String.format(locale, formatString, getSkinnable().getCurrentValue()));
        valueText.getStyleClass().add("gauge-value");
        Helper.enableNode(valueText, getSkinnable().isValueVisible() && !getSkinnable().isAlert());

        unitText = new Text(getSkinnable().getUnit());
        unitText.getStyleClass().add("gauge-unit");
        Helper.enableNode(unitText, getSkinnable().isValueVisible() && !getSkinnable().isAlert());

        minValueText = new Text(String.format(locale, "%." + getSkinnable().getTickLabelDecimals() + "f", getSkinnable().getMinValue()));
        minValueText.getStyleClass().add("gauge-min-value");

        maxValueText = new Text(String.format(locale, "%." + getSkinnable().getTickLabelDecimals() + "f", getSkinnable().getMaxValue()));
        maxValueText.getStyleClass().add("gauge-max-value");

        thresholdRect = new Rectangle();
        thresholdRect.getStyleClass().add("gauge-threshold-rect");
        Helper.enableNode(thresholdRect, getSkinnable().isThresholdVisible());

        thresholdText = new Text(String.format(locale, "%." + getSkinnable().getTickLabelDecimals() + "f", getSkinnable().getThreshold()));
        thresholdText.getStyleClass().add("gauge-threshold");
        Helper.enableNode(thresholdText, getSkinnable().isThresholdVisible());

        pane = new Pane(barBackground, thresholdBar, sectionPane, alertIcon, needleRect, needle, titleText, valueText, unitText, minValueText, maxValueText, thresholdRect, thresholdText);
        pane.getStyleClass().add("gauge-background");

        getChildren().setAll(pane);
    }

    private void registerListeners() {
        getSkinnable().widthProperty().addListener(o -> handleEvents("RESIZE"));
        getSkinnable().heightProperty().addListener(o -> handleEvents("RESIZE"));
        getSkinnable().setOnUpdate(e -> handleEvents(e.eventType.name()));
        getSkinnable().currentValueProperty().addListener(o -> rotateNeedle(getSkinnable().getCurrentValue()));
    }


    // ******************** Methods *******************************************
    @Override protected double computeMinWidth(final double HEIGHT, final double TOP, final double RIGHT, final double BOTTOM, final double LEFT)  { return MINIMUM_WIDTH; }
    @Override protected double computeMinHeight(final double WIDTH, final double TOP, final double RIGHT, final double BOTTOM, final double LEFT)  { return MINIMUM_HEIGHT; }
    @Override protected double computePrefWidth(final double HEIGHT, final double TOP, final double RIGHT, final double BOTTOM, final double LEFT) { return super.computePrefWidth(HEIGHT, TOP, RIGHT, BOTTOM, LEFT); }
    @Override protected double computePrefHeight(final double WIDTH, final double TOP, final double RIGHT, final double BOTTOM, final double LEFT) { return super.computePrefHeight(WIDTH, TOP, RIGHT, BOTTOM, LEFT); }
    @Override protected double computeMaxWidth(final double HEIGHT, final double TOP, final double RIGHT, final double BOTTOM, final double LEFT)  { return MAXIMUM_WIDTH; }
    @Override protected double computeMaxHeight(final double WIDTH, final double TOP, final double RIGHT, final double BOTTOM, final double LEFT)  { return MAXIMUM_HEIGHT; }

    private void handleEvents(final String EVENT_TYPE) {
        if ("RESIZE".equals(EVENT_TYPE)) {
            resize();
            redraw();
        } else if ("REDRAW".equals(EVENT_TYPE)) {
            redraw();
        } else if ("RECALC".equals(EVENT_TYPE)) {
            angleRange        = Helper.clamp(90.0, 180.0, getSkinnable().getAngleRange());
            minValue          = getSkinnable().getMinValue();
            range             = getSkinnable().getRange();
            angleStep         = angleRange / range;
            highlightSections = getSkinnable().isHighlightSections();
            redraw();
        } else if ("VISIBILITY".equals(EVENT_TYPE)) {
            Helper.enableNode(titleText, !getSkinnable().getTitle().isEmpty());
            Helper.enableNode(valueText, getSkinnable().isValueVisible());
            Helper.enableNode(sectionPane, getSkinnable().getSectionsVisible());
            Helper.enableNode(thresholdRect, getSkinnable().isThresholdVisible());
            Helper.enableNode(thresholdText, getSkinnable().isThresholdVisible());
            Helper.enableNode(unitText, !getSkinnable().getUnit().isEmpty());
            sectionsVisible = getSkinnable().getSectionsVisible();
        } else if ("SECTION".equals(EVENT_TYPE)) {
            sections = getSkinnable().getSections();
            sectionMap.clear();
            for(Section section : sections) { sectionMap.put(section, new Arc()); }
        } else if ("ALERT".equals(EVENT_TYPE)) {
            Helper.enableNode(valueText, getSkinnable().isValueVisible() && !getSkinnable().isAlert());
            Helper.enableNode(unitText, getSkinnable().isValueVisible() && !getSkinnable().isAlert());
            Helper.enableNode(alertIcon, getSkinnable().isAlert());
            alertTooltip.setText(getSkinnable().getAlertMessage());
        }
    }

    private void rotateNeedle(final double VALUE) {
        double needleStartAngle = angleRange * 0.5;
        double targetAngle = (VALUE - minValue) * angleStep - needleStartAngle;
        targetAngle = Helper.clamp(-needleStartAngle, -needleStartAngle + angleRange, targetAngle);
        needleRotate.setAngle(targetAngle);
        needleRectRotate.setAngle(targetAngle);
        valueText.setText(String.format(locale, formatString, VALUE));
        resizeDynamicText();
        highlightSections(VALUE);
    }

    private void highlightSections(final double VALUE) {
        if (!sectionsVisible || sections.isEmpty()) return;
        if (highlightSections) {
            sections.forEach(section -> sectionMap.get(section).setVisible(section.contains(VALUE)));
        } else {
            sections.forEach(section -> sectionMap.get(section).setOpacity(section.contains(VALUE) ? 1.0 : 0.25));
        }
    }

    private void drawSections() {
        if (!sectionsVisible || sections.isEmpty()) return;
        sectionPane.getChildren().clear();

        double     centerX      = size * 0.5;
        double     centerY      = size * 0.7825;
        double     innerRadius  = size * 0.2775;
        double     outerRadius  = size * 0.3225;
        int        noOfSections = sections.size();
        List<Line> sectionLines = new ArrayList<>(noOfSections);
        for (int i = 0 ; i < noOfSections - 1 ; i++) {
            Section section = sections.get(i);
            double  angle   = Helper.clamp(90.0, 270.0, (section.getStop() - minValue) * angleStep + 90.0);
            Line    line    = new Line(centerX + innerRadius * Math.sin(-Math.toRadians(angle)), centerY + innerRadius * Math.cos(-Math.toRadians(angle)),
                                       centerX + outerRadius * Math.sin(-Math.toRadians(angle)), centerY + outerRadius * Math.cos(-Math.toRadians(angle)));
            line.setStroke(getSkinnable().getBackgroundPaint());
            sectionLines.add(line);
        }
        sectionPane.getChildren().addAll(sectionLines);

        double barRadius = size * 0.3;
        double barWidth  = size * 0.045;
        double maxValue  = getSkinnable().getMaxValue();
        for (Section section : sections) {
            double startAngle = (section.getStart() - minValue) * angleStep - angleRange;
            double length;
            if (section.getStop() > maxValue) {
                length = (maxValue - section.getStart()) * angleStep;
            } else if (Double.compare(section.getStart(), minValue) < 0) {
                length = (section.getStop() - minValue) * angleStep;
            } else {
                length = (section.getStop() - section.getStart()) * angleStep;
            }
            Arc sectionArc = new Arc(centerX, centerY, barRadius, barRadius, -startAngle, -length);

            sectionArc.setType(ArcType.OPEN);
            sectionArc.setStrokeWidth(barWidth);
            sectionArc.setStrokeLineCap(StrokeLineCap.BUTT);
            sectionArc.setVisible(!highlightSections);
            sectionArc.setOpacity(highlightSections ? 1.0 : 0.25);
            sectionArc.getStyleClass().addAll("gauge-section", section.getStyleClass());
            Tooltip sectionTooltip = new Tooltip(section.getText());
            sectionTooltip.setTextAlignment(TextAlignment.CENTER);
            Tooltip.install(sectionArc, sectionTooltip);
            sectionMap.put(section, sectionArc);
        }
        sectionPane.getChildren().addAll(sectionMap.values());
    }

    private void drawNeedle() {
        double needleWidth  = size * 0.05;
        double needleHeight = size * 0.3325;
        needle.setCache(false);

        needle.getElements().clear();
        needle.getElements().add(new MoveTo(0.25 * needleWidth, 0.924812030075188 * needleHeight));
        needle.getElements().add(new CubicCurveTo(0.25 * needleWidth, 0.9022556390977443 * needleHeight,
                                                  0.35 * needleWidth, 0.8872180451127819 * needleHeight,
                                                  0.5 * needleWidth, 0.8872180451127819 * needleHeight));
        needle.getElements().add(new CubicCurveTo(0.65 * needleWidth, 0.8872180451127819 * needleHeight,
                                                  0.75 * needleWidth, 0.9022556390977443 * needleHeight,
                                                  0.75 * needleWidth, 0.924812030075188 * needleHeight));
        needle.getElements().add(new CubicCurveTo(0.75 * needleWidth, 0.9473684210526315 * needleHeight,
                                                  0.65 * needleWidth, 0.9624060150375939 * needleHeight,
                                                  0.5 * needleWidth, 0.9624060150375939 * needleHeight));
        needle.getElements().add(new CubicCurveTo(0.35 * needleWidth, 0.9624060150375939 * needleHeight,
                                                  0.25 * needleWidth, 0.9473684210526315 * needleHeight,
                                                  0.25 * needleWidth, 0.924812030075188 * needleHeight));
        needle.getElements().add(new ClosePath());
        needle.getElements().add(new MoveTo(0.0, 0.924812030075188 * needleHeight));
        needle.getElements().add(new CubicCurveTo(0.0, 0.9699248120300752 * needleHeight,
                                                  0.2 * needleWidth, needleHeight,
                                                  0.5 * needleWidth, needleHeight));
        needle.getElements().add(new CubicCurveTo(0.8 * needleWidth, needleHeight,
                                                  needleWidth, 0.9699248120300752 * needleHeight,
                                                  needleWidth, 0.924812030075188 * needleHeight));
        needle.getElements().add(new CubicCurveTo(needleWidth, 0.8947368421052632 * needleHeight,
                                                  0.85 * needleWidth, 0.8646616541353384 * needleHeight,
                                                  0.65 * needleWidth, 0.849624060150376 * needleHeight));
        needle.getElements().add(new CubicCurveTo(0.65 * needleWidth, 0.849624060150376 * needleHeight,
                                                  0.65 * needleWidth, 0.022556390977443608 * needleHeight,
                                                  0.65 * needleWidth, 0.022556390977443608 * needleHeight));
        needle.getElements().add(new CubicCurveTo(0.65 * needleWidth, 0.007518796992481203 * needleHeight,
                                                  0.6 * needleWidth, 0.0,
                                                  0.5 * needleWidth, 0.0));
        needle.getElements().add(new CubicCurveTo(0.4 * needleWidth, 0.0,
                                                  0.35 * needleWidth, 0.007518796992481203 * needleHeight,
                                                  0.35 * needleWidth, 0.022556390977443608 * needleHeight));
        needle.getElements().add(new CubicCurveTo(0.35 * needleWidth, 0.022556390977443608 * needleHeight,
                                                  0.35 * needleWidth, 0.849624060150376 * needleHeight,
                                                  0.35 * needleWidth, 0.849624060150376 * needleHeight));
        needle.getElements().add(new CubicCurveTo(0.15 * needleWidth, 0.8646616541353384 * needleHeight,
                                                  0.0, 0.8947368421052632 * needleHeight,
                                                  0.0, 0.924812030075188 * needleHeight));
        needle.getElements().add(new ClosePath());
        needle.setCache(true);
        needle.setCacheHint(CacheHint.ROTATE);
    }


    // ******************** Resizing ******************************************
    private void resizeDynamicText() {
        double maxWidth = unitText.isManaged() ? size * 0.725 : size * 0.9;
        double fontSize = size * 0.24;
        valueText.setFont(Fonts.latoRegular(fontSize));
        if (valueText.getLayoutBounds().getWidth() > maxWidth) { Helper.adjustTextSize(valueText, maxWidth, fontSize); }

        maxWidth = size * 0.15;
        fontSize = size * 0.06;
        unitText.setFont(Fonts.latoRegular(fontSize));
        if (unitText.getLayoutBounds().getWidth() > maxWidth) { Helper.adjustTextSize(unitText, maxWidth, fontSize); }

        if (unitText.isVisible()) {
            valueText.relocate((size - valueText.getLayoutBounds().getWidth() - unitText.getLayoutBounds().getWidth()) * 0.5, size * 0.15);
            unitText.relocate(valueText.getLayoutX() + valueText.getLayoutBounds().getWidth() + size * 0.025, size * 0.3275);
        } else {
            valueText.relocate((size - valueText.getLayoutBounds().getWidth()) * 0.5, size * 0.15);
        }

        if (sectionsVisible) {
            fontSize = size * 0.09;
            thresholdText.setFont(Fonts.latoRegular(fontSize));
            thresholdText.setTextOrigin(VPos.CENTER);
            if (thresholdText.getLayoutBounds().getWidth() > maxWidth) { Helper.adjustTextSize(thresholdText, maxWidth, fontSize); }
            thresholdText.setX((size - thresholdText.getLayoutBounds().getWidth()) * 0.5);
            thresholdText.setY(thresholdRect.getLayoutBounds().getMinY() + thresholdRect.getHeight() * 0.5);
        }
    }
    private void resizeStaticText() {
        double maxWidth = size * 0.9;
        double fontSize = size * 0.06;
        double textRadius;
        double sinValue;
        double cosValue;
        double textX;
        double textY;

        titleText.setFont(Fonts.latoRegular(fontSize));
        if (titleText.getLayoutBounds().getWidth() > maxWidth) { Helper.adjustTextSize(titleText, maxWidth, fontSize); }
        titleText.relocate(size * 0.05, size * 0.05);

        maxWidth = size * 0.15;
        fontSize = size * 0.07;
        maxValueText.setFont(Fonts.latoRegular(fontSize));
        if (maxValueText.getLayoutBounds().getWidth() > maxWidth) { Helper.adjustTextSize(maxValueText, maxWidth, fontSize); }
        textRadius = size * 0.3;
        sinValue  = Math.sin(Math.toRadians(90 + (180 - angleRange) * 0.5));
        cosValue  = Math.cos(Math.toRadians(90 + (180 - angleRange) * 0.5));
        textX     = size * 0.5 + textRadius * sinValue;
        textY     = size * 0.83 + textRadius * cosValue;
        maxValueText.setTranslateX(-maxValueText.getLayoutBounds().getWidth() * 0.5);
        maxValueText.setTranslateY(-maxValueText.getLayoutBounds().getHeight() * 0.5);
        maxValueText.relocate(textX, textY);

        minValueText.setFont(Fonts.latoRegular(maxValueText.getFont().getSize()));
        if (minValueText.getLayoutBounds().getWidth() > maxWidth) { Helper.adjustTextSize(minValueText, maxWidth, fontSize); }
        sinValue  = Math.sin(Math.toRadians(-90 - (180 - angleRange) * 0.5));
        cosValue  = Math.cos(Math.toRadians(-90 - (180 - angleRange) * 0.5));
        textX     = size * 0.5 + textRadius * sinValue;
        textY     = size * 0.83 + textRadius * cosValue;
        minValueText.setTranslateX(-minValueText.getLayoutBounds().getWidth() * 0.5);
        minValueText.setTranslateY(-minValueText.getLayoutBounds().getHeight() * 0.5);
        minValueText.relocate(textX, textY);

        if (!sectionsVisible) {
            fontSize = size * 0.09;
            thresholdText.setFont(Fonts.latoRegular(fontSize));
            thresholdText.setTextOrigin(VPos.CENTER);
            if (thresholdText.getLayoutBounds().getWidth() > maxWidth) { Helper.adjustTextSize(thresholdText, maxWidth, fontSize); }
            thresholdText.setX((size - thresholdText.getLayoutBounds().getWidth()) * 0.5);
            thresholdText.setY(thresholdRect.getLayoutBounds().getMinY() + thresholdRect.getHeight() * 0.5);
        }
    }

    private void resize() {
        double width  = getSkinnable().getWidth() - getSkinnable().getInsets().getLeft() - getSkinnable().getInsets().getRight();
        double height = getSkinnable().getHeight() - getSkinnable().getInsets().getTop() - getSkinnable().getInsets().getBottom();
        size          = width < height ? width : height;

        if (width > 0 && height > 0) {
            double centerX   = size * 0.5;
            double centerY   = size * 0.7825;
            double barRadius = size * 0.3;
            double barWidth  = size * 0.045;

            pane.setMaxSize(size, size);
            pane.relocate((width - size) * 0.5, (height - size) * 0.5);

            sectionPane.setMaxSize(size, size);

            barBackground.setCenterX(centerX);
            barBackground.setCenterY(centerY);
            barBackground.setRadiusX(barRadius);
            barBackground.setRadiusY(barRadius);
            barBackground.setStrokeWidth(barWidth);
            barBackground.setStartAngle(angleRange * 0.5 + 90);
            barBackground.setLength(-angleRange);

            thresholdBar.setCenterX(centerX);
            thresholdBar.setCenterY(centerY);
            thresholdBar.setRadiusX(barRadius);
            thresholdBar.setRadiusY(barRadius);
            thresholdBar.setStrokeWidth(barWidth);
            thresholdBar.setStartAngle(90 - angleRange * 0.5);
            thresholdBar.setLength((getSkinnable().getMaxValue() - getSkinnable().getThreshold()) * angleStep);

            if (sectionsVisible) { drawSections(); }

            alertIcon.setPrefSize(size * 0.155, size * 0.1375);
            alertIcon.relocate((size - alertIcon.getPrefWidth()) * 0.5, size * 0.244);

            needleRect.setWidth(size * 0.035);
            needleRect.setHeight(size * 0.05);
            needleRect.relocate((size - needleRect.getWidth()) * 0.5, size * 0.4575);
            needleRectRotate.setPivotX(needleRect.getLayoutBounds().getWidth() * 0.5);
            needleRectRotate.setPivotY(size * 0.325);

            drawNeedle();

            needle.relocate((size - needle.getLayoutBounds().getWidth()) * 0.5, size * 0.475);
            needleRotate.setPivotX(needle.getLayoutBounds().getWidth() * 0.5);
            needleRotate.setPivotY(needle.getLayoutBounds().getHeight() - needle.getLayoutBounds().getWidth() * 0.5);

            resizeStaticText();
            resizeDynamicText();

            thresholdRect.setWidth(thresholdText.getLayoutBounds().getWidth() + size * 0.05);
            thresholdRect.setHeight(thresholdText.getLayoutBounds().getHeight());
            thresholdRect.setX((size - thresholdRect.getWidth()) * 0.5);
            thresholdRect.setY(size * 0.845);
            thresholdRect.setArcWidth(size * 0.025);
            thresholdRect.setArcHeight(size * 0.025);
        }
    }

    private void redraw() {
        locale       = getSkinnable().getLocale();
        formatString = new StringBuilder("%.").append(Integer.toString(getSkinnable().getDecimals())).append("f").toString();

        titleText.setText(getSkinnable().getTitle());
        unitText.setText(getSkinnable().getUnit());
        minValueText.setText(String.format(locale, "%." + getSkinnable().getTickLabelDecimals() + "f", getSkinnable().getMinValue()));
        maxValueText.setText(String.format(locale, "%." + getSkinnable().getTickLabelDecimals() + "f", getSkinnable().getMaxValue()));
        thresholdText.setText(String.format(locale, "%." + getSkinnable().getTickLabelDecimals() + "f", getSkinnable().getThreshold()));
        resizeStaticText();

        highlightSections(getSkinnable().getValue());
    }
}
