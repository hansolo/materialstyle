package eu.hansolo.fx.material.gauges;


public enum TickLabelLocation {
    INSIDE, OUTSIDE
}
