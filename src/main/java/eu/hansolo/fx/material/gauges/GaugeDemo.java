package eu.hansolo.fx.material.gauges;

import eu.hansolo.fx.material.gauges.Gauge.SkinType;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.Scene;

import java.util.Random;

public class GaugeDemo extends Application {
    private static final Random         RND = new Random();
    private              Gauge          gauge1;
    private              Gauge          gauge2;
    private              Gauge          gauge3;
    private              Gauge          gauge4;
    private              long           lastTimerCall;
    private              AnimationTimer timer;


    @Override public void init() {
        gauge1 = createGauge(SkinType.TILE_KPI, "Gauge 1", 100, "No values!",
                             SectionBuilder.create().start(0).stop(33).text("Wenige offene\nProzesse").styleClass("gauge-section-green").build(),
                             SectionBuilder.create().start(33).stop(66).text("Einige offene\nProzesse").styleClass("gauge-section-yellow").build(),
                             SectionBuilder.create().start(66).stop(100).text("Zu viele\noffene Prozesse").styleClass("gauge-section-red").build());

        gauge2 = createGauge(SkinType.TILE_KPI, "Gauge 2", 100, "No values!",
                             SectionBuilder.create().start(0).stop(25).text("Wenige offene\nProzesse").styleClass("gauge-section-blue").build(),
                             SectionBuilder.create().start(25).stop(50).text("Einige offene\nProzesse").styleClass("gauge-section-green").build(),
                             SectionBuilder.create().start(50).stop(75).text("Einige offene\nProzesse").styleClass("gauge-section-yellow").build(),
                             SectionBuilder.create().start(75).stop(100).text("Zu viele\noffene Prozesse").styleClass("gauge-section-red").build());

        gauge3 = createGauge(SkinType.TILE_SPARK_LINE, "Gauge 3", 100, "No values!",
                             SectionBuilder.create().start(0).stop(33).text("Wenige offene\nProzesse").styleClass("gauge-section-green").build(),
                             SectionBuilder.create().start(33).stop(66).text("Einige offene\nProzesse").styleClass("gauge-section-yellow").build(),
                             SectionBuilder.create().start(66).stop(100).text("Zu viele\noffene Prozesse").styleClass("gauge-section-red").build());

        gauge4 = createGauge(SkinType.TILE_TEXT_KPI, "Gauge 4", 50, "No values!",
                             SectionBuilder.create().start(0).stop(66).text("Wenige offene\nProzesse").styleClass("gauge-section-green").build(),
                             SectionBuilder.create().start(66).stop(100).text("Zu viele\noffene Prozesse").styleClass("gauge-section-red").build());

        gauge1.setAlert(true);

        lastTimerCall = System.nanoTime();
        timer = new AnimationTimer() {
            @Override public void handle(long now) {
                if (now > lastTimerCall + 5_000_000_000l) {
                    //gauge1.setValue(RND.nextDouble() * 100);
                    gauge2.setValue(RND.nextDouble() * 100);
                    gauge3.setValue(RND.nextDouble() * 100);
                    gauge4.setValue(RND.nextDouble() * 100);
                    lastTimerCall = now;
                }
            }
        };
    }

    @Override public void start(Stage stage) {
        TilePane pane = new TilePane(Orientation.HORIZONTAL, 10, 10, gauge1, gauge2, gauge3, gauge4);
        pane.setPadding(new Insets(10));
        pane.setPrefColumns(4);
        pane.setPrefTileWidth(250);
        pane.setPrefTileHeight(250);
        pane.setTileAlignment(Pos.TOP_LEFT);
        pane.setBackground(new Background(new BackgroundFill(Color.web("#eeeeee"), CornerRadii.EMPTY, Insets.EMPTY)));

        Scene scene = new Scene(pane);

        stage.setTitle("Gauge");
        stage.setScene(scene);
        stage.show();

        timer.start();
    }

    @Override public void stop() {
        System.exit(0);
    }

    private Gauge createGauge(final SkinType TYPE, final String TITLE, final double MAX_VALUE, final String ALERT_MESSAGE, final Section... SECTIONS) {
        return GaugeBuilder.create()
                           .skinType(TYPE)
                           .minSize(80, 80)
                           .prefSize(150, 150)
                           .maxSize(250, 250)
                           .maxValue(MAX_VALUE)
                           .animated(SkinType.TILE_SPARK_LINE == TYPE? false : true)
                           .subTitle("[10 min]")
                           .sections(SECTIONS)
                           .sectionsVisible(true)
                           .highlightSections(true)
                           .valueVisible(true)
                           .averagingEnabled(SkinType.TILE_SPARK_LINE == TYPE ? true : false)
                           .smoothing(false)
                           .title(TITLE)
                           .alertMessage(ALERT_MESSAGE)
                           .build();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
