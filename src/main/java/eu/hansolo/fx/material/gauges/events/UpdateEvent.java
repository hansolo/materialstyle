package eu.hansolo.fx.material.gauges.events;

import java.util.EventObject;

public class UpdateEvent extends EventObject {
        public final EventType eventType;

    public UpdateEvent(final Object SRC, final EventType EVENT_TYPE) {
        super(SRC);
        eventType = EVENT_TYPE;
    }

public enum EventType {
        RECALC, REDRAW, RESIZE, LED, LCD, VISIBILITY, INTERACTIVITY, FINISHED, SECTION, ALERT, VALUE
    }
}
