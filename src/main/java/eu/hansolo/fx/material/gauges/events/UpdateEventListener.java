package eu.hansolo.fx.material.gauges.events;

import java.util.EventListener;

@FunctionalInterface
public interface UpdateEventListener extends EventListener {
    void onUpdateEvent(final UpdateEvent EVENT);
}
