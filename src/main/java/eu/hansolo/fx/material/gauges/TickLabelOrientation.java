package eu.hansolo.fx.material.gauges;


public enum TickLabelOrientation {
    ORTHOGONAL,  HORIZONTAL, TANGENT
}
