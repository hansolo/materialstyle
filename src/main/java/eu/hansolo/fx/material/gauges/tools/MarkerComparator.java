package eu.hansolo.fx.material.gauges.tools;

import eu.hansolo.fx.material.gauges.Marker;

import java.util.Comparator;

public class MarkerComparator implements Comparator<Marker> {
    @Override
    public int compare(final Marker MARKER_1, final Marker MARKER_2) {
        return MARKER_1.compareTo(MARKER_2);
    }
}
