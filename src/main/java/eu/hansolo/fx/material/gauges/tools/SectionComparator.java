package eu.hansolo.fx.material.gauges.tools;

import eu.hansolo.fx.material.gauges.Section;

import java.util.Comparator;

public class SectionComparator implements Comparator<Section> {
    @Override
    public int compare(final Section SECTION_1, final Section SECTION_2) {
        return SECTION_1.compareTo(SECTION_2);
    }
}
