package eu.hansolo.fx.material.gauges.tools;

import eu.hansolo.fx.material.gauges.Section;
import eu.hansolo.fx.material.gauges.TickLabelOrientation;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Accordion;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;

public class Helper {
    public static final <T extends Number> T clamp(final T MIN, final T MAX, final T VALUE) {
        if (VALUE.doubleValue() < MIN.doubleValue())
            return MIN;
        if (VALUE.doubleValue() > MAX.doubleValue())
            return MAX;
        return VALUE;
    }

    public static final double[] calcAutoScale(final double MIN_VALUE, final double MAX_VALUE) {
        double maxNoOfMajorTicks = 10;
        double maxNoOfMinorTicks = 10;
        double niceMinValue;
        double niceMaxValue;
        double niceRange;
        double majorTickSpace;
        double minorTickSpace;
        niceRange = (calcNiceNumber((MAX_VALUE - MIN_VALUE), false));
        majorTickSpace = calcNiceNumber(niceRange / (maxNoOfMajorTicks - 1), true);
        niceMinValue = (Math.floor(MIN_VALUE / majorTickSpace) * majorTickSpace);
        niceMaxValue = (Math.ceil(MAX_VALUE / majorTickSpace) * majorTickSpace);
        minorTickSpace = calcNiceNumber(majorTickSpace / (maxNoOfMinorTicks - 1), true);
        return new double[] { niceMinValue, niceMaxValue, majorTickSpace, minorTickSpace };
    }

    /**
     * Returns a "niceScaling" number approximately equal to the range. Rounds
     * the number if ROUND == true. Takes the ceiling if ROUND = false.
     *
     * @param RANGE
     *            the value range (maxValue - minValue)
     * @param ROUND
     *            whether to round the result or ceil
     * @return a "niceScaling" number to be used for the value range
     */
    public static final double calcNiceNumber(final double RANGE, final boolean ROUND) {
        double niceFraction;
        double exponent = Math.floor(Math.log10(RANGE)); // exponent of range
        double fraction = RANGE / Math.pow(10, exponent); // fractional part of
                                                          // range

        if (ROUND) {
            if (Double.compare(fraction, 1.5) < 0) {
                niceFraction = 1;
            } else if (Double.compare(fraction, 3) < 0) {
                niceFraction = 2;
            } else if (Double.compare(fraction, 7) < 0) {
                niceFraction = 5;
            } else {
                niceFraction = 10;
            }
        } else {
            if (Double.compare(fraction, 1) <= 0) {
                niceFraction = 1;
            } else if (Double.compare(fraction, 2) <= 0) {
                niceFraction = 2;
            } else if (Double.compare(fraction, 5) <= 0) {
                niceFraction = 5;
            } else {
                niceFraction = 10;
            }
        }
        return niceFraction * Math.pow(10, exponent);
    }

    public static final Color getColorOfSection(final List<Section> SECTIONS, final double VALUE,
            final Color DEFAULT_COLOR) {
        for (Section section : SECTIONS) {
            if (section.contains(VALUE))
                return section.getColor();
        }
        return DEFAULT_COLOR;
    }

    public static final void rotateContextForText(final GraphicsContext CTX, final double START_ANGLE,
            final double ANGLE, final TickLabelOrientation ORIENTATION) {
        switch (ORIENTATION) {
        case ORTHOGONAL:
            if ((360 - START_ANGLE - ANGLE) % 360 > 90 && (360 - START_ANGLE - ANGLE) % 360 < 270) {
                CTX.rotate((180 - START_ANGLE - ANGLE) % 360);
            } else {
                CTX.rotate((360 - START_ANGLE - ANGLE) % 360);
            }
            break;
        case TANGENT:
            if ((360 - START_ANGLE - ANGLE - 90) % 360 > 90 && (360 - START_ANGLE - ANGLE - 90) % 360 < 270) {
                CTX.rotate((90 - START_ANGLE - ANGLE) % 360);
            } else {
                CTX.rotate((270 - START_ANGLE - ANGLE) % 360);
            }
            break;
        case HORIZONTAL:
        default:
            break;
        }
    }

    public static final void adjustTextSize(final Text TEXT, final double MAX_WIDTH, double fontSize) {
        final String FONT_NAME = TEXT.getFont().getName();
        while (TEXT.getLayoutBounds().getWidth() > MAX_WIDTH && fontSize > 0) {
            fontSize -= 0.005;
            TEXT.setFont(new Font(FONT_NAME, fontSize));
        }
    }

    public static DateTimeFormatter getDateFormat(final Locale LOCALE) {
        if (Locale.US == LOCALE) {
            return DateTimeFormatter.ofPattern("MM/dd/YYYY");
        } else if (Locale.CHINA == LOCALE) {
            return DateTimeFormatter.ofPattern("YYYY.MM.dd");
        } else {
            return DateTimeFormatter.ofPattern("dd.MM.YYYY");
        }
    }

    public static DateTimeFormatter getLocalizedDateFormat(final Locale LOCALE) {
        return DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(LOCALE);
    }

    public static void enableNode(final Node NODE, final boolean ENABLE) {
        NODE.setManaged(ENABLE);
        NODE.setVisible(ENABLE);
    }

    public static final String colorToCss(final Color COLOR) {
        return COLOR.toString().replace("0x", "#");
    }

    public static final ThreadFactory getThreadFactory(final String THREAD_NAME, final boolean IS_DAEMON) {
        return runnable -> {
            Thread thread = new Thread(runnable, THREAD_NAME);
            thread.setDaemon(IS_DAEMON);
            return thread;
        };
    }

    public static final void stopTask(ScheduledFuture<?> task) {
        if (null == task)
            return;
        task.cancel(true);
        task = null;
    }

    public static final ImagePattern createCarbonPattern() {
        final double SIZE = 12;
        final Canvas CANVAS = new Canvas(SIZE, SIZE);
        final GraphicsContext CTX = CANVAS.getGraphicsContext2D();

        CTX.setFill(new LinearGradient(0, 0, 0, 0.5 * SIZE, false, CycleMethod.NO_CYCLE,
                new Stop(0, Color.rgb(35, 35, 35)), new Stop(1, Color.rgb(23, 23, 23))));
        CTX.fillRect(0, 0, SIZE * 0.5, SIZE * 0.5);

        CTX.setFill(new LinearGradient(0, 0, 0, 0.416666 * SIZE, false, CycleMethod.NO_CYCLE,
                new Stop(0, Color.rgb(38, 38, 38)), new Stop(1, Color.rgb(30, 30, 30))));
        CTX.fillRect(SIZE * 0.083333, 0, SIZE * 0.333333, SIZE * 0.416666);

        CTX.setFill(new LinearGradient(0, 0.5 * SIZE, 0, SIZE, false, CycleMethod.NO_CYCLE,
                new Stop(0, Color.rgb(35, 35, 35)), new Stop(1, Color.rgb(23, 23, 23))));
        CTX.fillRect(SIZE * 0.5, SIZE * 0.5, SIZE * 0.5, SIZE * 0.5);

        CTX.setFill(new LinearGradient(0, 0.5 * SIZE, 0, 0.916666 * SIZE, false, CycleMethod.NO_CYCLE,
                new Stop(0, Color.rgb(38, 38, 38)), new Stop(1, Color.rgb(30, 30, 30))));
        CTX.fillRect(SIZE * 0.583333, SIZE * 0.5, SIZE * 0.333333, SIZE * 0.416666);

        CTX.setFill(new LinearGradient(0, 0, 0, 0.5 * SIZE, false, CycleMethod.NO_CYCLE,
                new Stop(0, Color.rgb(48, 48, 48)), new Stop(1, Color.rgb(40, 40, 40))));
        CTX.fillRect(SIZE * 0.5, 0, SIZE * 0.5, SIZE * 0.5);

        CTX.setFill(new LinearGradient(0, 0.083333 * SIZE, 0, 0.5 * SIZE, false, CycleMethod.NO_CYCLE,
                new Stop(0, Color.rgb(53, 53, 53)), new Stop(1, Color.rgb(45, 45, 45))));
        CTX.fillRect(SIZE * 0.583333, SIZE * 0.083333, SIZE * 0.333333, SIZE * 0.416666);

        CTX.setFill(new LinearGradient(0, 0.5 * SIZE, 0, SIZE, false, CycleMethod.NO_CYCLE,
                new Stop(0, Color.rgb(48, 48, 48)), new Stop(1, Color.rgb(40, 40, 40))));
        CTX.fillRect(0, SIZE * 0.5, SIZE * 0.5, SIZE * 0.5);

        CTX.setFill(new LinearGradient(0, 0.583333 * SIZE, 0, SIZE, false, CycleMethod.NO_CYCLE,
                new Stop(0, Color.rgb(53, 53, 53)), new Stop(1, Color.rgb(45, 45, 45))));
        CTX.fillRect(SIZE * 0.083333, SIZE * 0.583333, SIZE * 0.333333, SIZE * 0.416666);

        final Image PATTERN_IMAGE = CANVAS.snapshot(new SnapshotParameters(), null);
        final ImagePattern PATTERN = new ImagePattern(PATTERN_IMAGE, 0, 0, SIZE, SIZE, false);

        return PATTERN;
    }

    public static void drawTrapezoid(final GraphicsContext CTX, final double PI1X, final double PI1Y, final double PI2X,
            final double PI2Y, final double PO1X, final double PO1Y, final double PO2X, final double PO2Y) {
        CTX.beginPath();
        CTX.moveTo(PI2X, PI2Y);
        CTX.lineTo(PI1X, PI1Y);
        CTX.lineTo(PO1X, PO1Y);
        CTX.lineTo(PO2X, PO2Y);
        CTX.closePath();
        CTX.fill();
    }

    public static void drawTriangle(final GraphicsContext CTX, final double PIX, final double PIY, final double PO1X,
            final double PO1Y, final double PO2X, final double PO2Y) {
        CTX.beginPath();
        CTX.moveTo(PIX, PIY);
        CTX.lineTo(PO1X, PO1Y);
        CTX.lineTo(PO2X, PO2Y);
        CTX.closePath();
        CTX.fill();
    }

    public static void drawDot(final GraphicsContext CTX, final double CENTER_X, final double CENTER_Y,
            final double SIZE) {
        CTX.fillOval(CENTER_X, CENTER_Y, SIZE, SIZE);
    }

    public static void drawLine(final GraphicsContext CTX, final double P1X, final double P1Y, final double P2X,
            final double P2Y) {
        CTX.strokeLine(P1X, P1Y, P2X, P2Y);
    }

    public static boolean isMonochrome(final Color COLOR) {
        return Double.compare(COLOR.getRed(), COLOR.getGreen()) == 0
                && Double.compare(COLOR.getGreen(), COLOR.getBlue()) == 0;
    }

    public static double colorDistance(final Color COLOR_1, final Color COLOR_2) {
        final double DELTA_R = (COLOR_2.getRed() - COLOR_1.getRed());
        final double DELTA_G = (COLOR_2.getGreen() - COLOR_1.getGreen());
        final double DELTA_B = (COLOR_2.getBlue() - COLOR_1.getBlue());

        return Math.sqrt(DELTA_R * DELTA_R + DELTA_G * DELTA_G + DELTA_B * DELTA_B);
    }

    public static boolean isBright(final Color COLOR) {
        return !isDark(COLOR);
    }

    public static boolean isDark(final Color COLOR) {
        final double DISTANCE_TO_WHITE = colorDistance(COLOR, Color.WHITE);
        final double DISTANCE_TO_BLACK = colorDistance(COLOR, Color.BLACK);
        return DISTANCE_TO_BLACK < DISTANCE_TO_WHITE;
    }

    public static Color getTranslucentColorFrom(final Color COLOR, final double FACTOR) {
        return Color.color(COLOR.getRed(), COLOR.getGreen(), COLOR.getBlue(), Helper.clamp(0d, 1d, FACTOR));
    }

    public static Image createNoiseImage(final double WIDTH, final double HEIGHT, final Color DARK_COLOR,
            final Color BRIGHT_COLOR, final double ALPHA_VARIATION_IN_PERCENT) {
        if (Double.compare(WIDTH, 0) <= 0 || Double.compare(HEIGHT, 0) <= 0)
            return null;
        int width = (int) WIDTH;
        int height = (int) HEIGHT;
        double alphaVariationInPercent = Helper.clamp(0d, 100d, ALPHA_VARIATION_IN_PERCENT);
        final WritableImage IMAGE = new WritableImage(width, height);
        final PixelWriter PIXEL_WRITER = IMAGE.getPixelWriter();
        final Random BW_RND = new Random();
        final Random ALPHA_RND = new Random();
        final double ALPHA_START = alphaVariationInPercent / 100 / 2;
        final double ALPHA_VARIATION = alphaVariationInPercent / 100;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                final Color NOISE_COLOR = BW_RND.nextBoolean() == true ? BRIGHT_COLOR : DARK_COLOR;
                final double NOISE_ALPHA = Helper.clamp(0d, 1d, ALPHA_START + ALPHA_RND.nextDouble() * ALPHA_VARIATION);
                PIXEL_WRITER.setColor(x, y,
                        Color.color(NOISE_COLOR.getRed(), NOISE_COLOR.getGreen(), NOISE_COLOR.getBlue(), NOISE_ALPHA));
            }
        }
        return IMAGE;
    }

    public static <T> T getChildByID(final Parent PARENT, final String ID) {
        String nodeId = null;
        if(PARENT instanceof TitledPane) {
            TitledPane titledPane = (TitledPane) PARENT;
            Node content = titledPane.getContent();
            nodeId = content.idProperty().get();
            if(nodeId != null && nodeId.equals(ID)) { return (T) content; }

            if(content instanceof Parent) {
                T child = getChildByID((Parent) content, ID);
                if(child != null) { return child; }
            }
        }

        for (Node node : PARENT.getChildrenUnmodifiable()) {
            nodeId = node.idProperty().get();
            if(nodeId != null && nodeId.equals(ID)) { return (T) node; }

            if(node instanceof SplitPane) {
                SplitPane splitPane = (SplitPane) node;
                for (Node itemNode : splitPane.getItems()) {
                    nodeId = itemNode.idProperty().get();
                    if(nodeId != null && nodeId.equals(ID)) { return (T) itemNode; }

                    if(itemNode instanceof Parent) {
                        T child = getChildByID((Parent) itemNode, ID);
                        if(child != null) { return child; }
                    }
                }
            } else if(node instanceof Accordion) {
                Accordion accordion = (Accordion) node;
                for (TitledPane titledPane : accordion.getPanes()) {
                    nodeId = titledPane.idProperty().get();
                    if(nodeId != null && nodeId.equals(ID)) { return (T) titledPane; }

                    T child = getChildByID(titledPane, ID);
                    if(child != null) { return child; }
                }
            } else if(node instanceof Parent) {
                T child = getChildByID((Parent) node, ID);
                if(child != null) { return child; }
            }
        }
        return null;
    }
}
