/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material;

import eu.hansolo.fx.material.components.MaterialProgressIndicator;
import eu.hansolo.fx.material.components.MaterialSwitch;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.scene.Scene;


/**
 * User: hansolo
 * Date: 25.11.16
 * Time: 09:50
 */
public class Tmp extends Application {
    private MaterialSwitch materialSwitch;
    private CheckBox       checkBox;

    @Override public void init() {
        materialSwitch = new MaterialSwitch("Check blbabalablablablablab");
        materialSwitch.setIndeterminate(true);

        checkBox = new CheckBox("Check");
        checkBox.setIndeterminate(true);

        materialSwitch.selectedProperty().addListener(o -> System.out.println("Is selected: " + materialSwitch.isSelected()));
        //materialSwitch.indeterminateProperty().addListener((o, ov, nv) -> System.out.println("Is indeterminate: " + nv));
        //materialSwitch.checkedProperty().addListener((o, ov, nv) -> System.out.println("Is checked: " + nv));
        materialSwitch.stateProperty().addListener(o -> System.out.println("State: " + materialSwitch.getState()));

        checkBox.selectedProperty().addListener(o -> System.out.println("CheckBox Is selected: " + checkBox.isSelected()));
    }

    @Override public void start(Stage stage) {
        VBox pane = new VBox(materialSwitch, checkBox);
        pane.setSpacing(24);
        pane.setPadding(new Insets(12));

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(Tmp.class.getResource("styles.css").toExternalForm());

        stage.setTitle("Title");
        stage.setScene(scene);
        stage.show();

        //materialSwitch.setSelected(true);
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
