/*
 * Copyright (c) 2015 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.conversion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * User: hansolo
 * Date: 07.06.14
 * Time: 14:09
 */
public class Unit {
    public enum Type {
        ACCELERATION,
        ANGLE,
        AREA,
        DATA,
        CURRENCY,
        CURRENT,
        ELECTRIC_CHARGE,
        ENERGY,
        FORCE,
        HUMIDITY,
        LENGTH,
        LUMINANCE,
        LUMINOUS_FLUX,
        MASS,
        PRESSURE,
        SPEED,
        TEMPERATURE,
        TEMPERATURE_GRADIENT,
        TIME,
        TORQUE,
        VOLUME,
        VOLTAGE,
        WORK
    }
    public enum Definition {
        // Length
        KILOMETER(new UnitBean(Type.LENGTH, "km", "Kilometer", new BigDecimal("1000.0"))),
        HECTOMETER(new UnitBean(Type.LENGTH, "hm", "Hectometer", new BigDecimal("100"))),
        METER(new UnitBean(Type.LENGTH, "m", "Meter", new BigDecimal("1.0"))),
        DECIMETER(new UnitBean(Type.LENGTH, "dm", "Decimeter", new BigDecimal("0.1"))),
        CENTIMETER(new UnitBean(Type.LENGTH, "cm", "Centimeter", new BigDecimal("0.01"))),
        MILLIMETER(new UnitBean(Type.LENGTH, "mm", "Millimeter", new BigDecimal("0.0010"))),
        MICROMETER(new UnitBean(Type.LENGTH, "\u00b5m", "Micrometer", new BigDecimal("1.0E-6"))),
        NANOMETER(new UnitBean(Type.LENGTH, "nm", "Nanometer", new BigDecimal("1.0E-9"))),
        ANGSTROM(new UnitBean(Type.LENGTH, "\u00c5", "Angstrom", new BigDecimal("1.0E-10"))),
        PICOMETER(new UnitBean(Type.LENGTH, "pm", "Picometer", new BigDecimal("1.0E-12"))),
        FEMTOMETER(new UnitBean(Type.LENGTH, "fm", "Femtometer", new BigDecimal("1.0E-15"))),
        INCHES(new UnitBean(Type.LENGTH, "in", "Inches", new BigDecimal("0.0254"))),
        MILES(new UnitBean(Type.LENGTH, "mi", "Miles", new BigDecimal("1609.344"))),
        NAUTICAL_MILES(new UnitBean(Type.LENGTH, "nmi", "Nautical Miles", new BigDecimal("1852.0"))),
        FEET(new UnitBean(Type.LENGTH, "ft", "Feet", new BigDecimal("0.3048"))),
        YARD(new UnitBean(Type.LENGTH, "yd", "Yard", new BigDecimal("0.9144"))),
        LIGHT_YEAR(new UnitBean(Type.LENGTH, "l.y.", "Light-Year", new BigDecimal("9.46073E15"))),
        PARSEC(new UnitBean(Type.LENGTH, "pc", "Parsec", new BigDecimal("3.085678E16"))),
        PIXEL(new UnitBean(Type.LENGTH, "px", "Pixel", new BigDecimal("0.000264583"))),
        POINT(new UnitBean(Type.LENGTH, "pt", "Point", new BigDecimal("0.0003527778"))),
        PICA(new UnitBean(Type.LENGTH, "p", "Pica", new BigDecimal("0.0042333333"))),
        EM(new UnitBean(Type.LENGTH, "em", "Quad", new BigDecimal("0.0042175176"))),
        // Mass
        TON(new UnitBean(Type.MASS, "t", "Ton", new BigDecimal("1.0E3"))),
        KILOGRAM(new UnitBean(Type.MASS, "kg", "Kilogram", new BigDecimal("1.0"))),
        GRAM(new UnitBean(Type.MASS, "g", "Gram", new BigDecimal("1.0E-3"))),
        MILLIGRAM(new UnitBean(Type.MASS, "mg", "Milligram", new BigDecimal("1.0E-6"))),
        MICROGRAM(new UnitBean(Type.MASS, "µg", "Mikrogram", new BigDecimal("1.0E-6"))),
        NANOGRAM(new UnitBean(Type.MASS, "ng", "Nanogram", new BigDecimal("1.0E-9"))),
        PICOGRAM(new UnitBean(Type.MASS, "pg", "Picogram", new BigDecimal("1.0E-12"))),
        FEMTOGRAM(new UnitBean(Type.MASS, "fg", "Femtogram", new BigDecimal("1.0E-15"))),
        OUNCE(new UnitBean(Type.MASS, "oz", "Ounce (US)", new BigDecimal("0.028"))),
        POUND(new UnitBean(Type.MASS, "lb", "Pounds (US)", new BigDecimal("0.45359237"))),
        // Time
        WEEK(new UnitBean(Type.TIME, "wk", "Week", new BigDecimal("604800"))),
        DAY(new UnitBean(Type.TIME, "d", "Day", new BigDecimal("86400"))),
        HOUR(new UnitBean(Type.TIME, "h", "Hour", new BigDecimal("3600"))),
        MINUTE(new UnitBean(Type.TIME, "m", "Minute", new BigDecimal("60"))),
        SECOND(new UnitBean(Type.TIME, "s", "Second", new BigDecimal("1.0"))),
        MILLISECOND(new UnitBean(Type.TIME, "ms", "Millisecond", new BigDecimal("1E-3"))),
        MICROSECOND(new UnitBean(Type.TIME, "µs", "Microsecond", new BigDecimal("1E-6"))),
        NANOSECOND(new UnitBean(Type.TIME, "ns", "Nanosecond", new BigDecimal("1E-9"))),
        PICOSECOND(new UnitBean(Type.TIME, "ps", "Picosecond", new BigDecimal("1E-12"))),
        FEMTOSECOND(new UnitBean(Type.TIME, "fs", "Femtosecond", new BigDecimal("1E-15"))),
        // Area
        SQUARE_KILOMETER(new UnitBean(Type.AREA, "km²", "Square Kilometer", new BigDecimal("1.0E6"))),
        SQUARE_METER(new UnitBean(Type.AREA, "m²", "Meter", new BigDecimal("1.0"))),
        SQUARE_CENTIMETER(new UnitBean(Type.AREA, "cm²", "Square Centimeter", new BigDecimal("1.0E-4"))),
        SQUARE_MILLIMETER(new UnitBean(Type.AREA, "mm²", "Square Millimeter", new BigDecimal("1.0E-6"))),
        SQUARE_MICROMETER(new UnitBean(Type.AREA, "µm²", "Square Mikrometer", new BigDecimal("1.0E-12"))),
        SQUARE_NANOMETER(new UnitBean(Type.AREA, "nm²", "Square Nanometer", new BigDecimal("1.0E-18"))),
        SQUARE_ANGSTROM(new UnitBean(Type.AREA, "Å²", "Square Ångstrom", new BigDecimal("1.0E-20"))),
        SQUARE_PICOMETER(new UnitBean(Type.AREA, "pm²", "Square Picometer", new BigDecimal("1.0E-24"))),
        SQUARE_FEMTOMETER(new UnitBean(Type.AREA, "fm²", "Square Femtometer", new BigDecimal("1.0E-30"))),
        HECTARE(new UnitBean(Type.AREA, "ha", "Hectare", new BigDecimal("1.0E5"))),
        ACRE(new UnitBean(Type.AREA, "ac", "Acre", new BigDecimal("4046.8564224"))),
        ARES(new UnitBean(Type.AREA, "a", "Ares", new BigDecimal("100"))),
        SQUARE_INCH(new UnitBean(Type.AREA, "in²", "Square Inch", new BigDecimal("0.00064516"))),
        SQUARE_FOOT(new UnitBean(Type.AREA, "ft²", "Square Foot", new BigDecimal("0.09290304"))),
        // Temperature
        KELVIN(new UnitBean(Type.TEMPERATURE, "K", "Kelvin", new BigDecimal("1.0"))),
        CELSIUS(new UnitBean(Type.TEMPERATURE, "°C", "Celsius", new BigDecimal("1.0"), new BigDecimal("273.15"))),
        FAHRENHEIT(new UnitBean(Type.TEMPERATURE, "°F", "Fahrenheit", new BigDecimal("0.555555555555555"), new BigDecimal("459.67"))),
        // Angle
        DEGREE(new UnitBean(Type.ANGLE, "deg", "Degree", (Math.PI / 180.0))),
        RADIAN(new UnitBean(Type.ANGLE, "rad", "Radian", new BigDecimal("1.0"))),
        GRAD(new UnitBean(Type.ANGLE, "grad", "Gradian", new BigDecimal("0.9"))),
        // Volume
        CUBIC_MILLIMETER(new UnitBean(Type.VOLUME, "mm³", "Cubic Millimeter", new BigDecimal("1.0E-9"))),
        MILLILITER(new UnitBean(Type.VOLUME, "ml", "Milliliter", new BigDecimal("1.0E-6"))),
        LITER(new UnitBean(Type.VOLUME, "l", "Liter", new BigDecimal("1.0E-3"))),
        CUBIC_METER(new UnitBean(Type.VOLUME, "m³", "Cubic Meter", new BigDecimal("1.0E0"))),
        GALLON(new UnitBean(Type.VOLUME, "gal", "US Gallon", new BigDecimal("0.0037854118"))),
        CUBIC_FEET(new UnitBean(Type.VOLUME, "ft³", "Cubic Foot", new BigDecimal("0.0283168466"))),
        CUBIC_INCH(new UnitBean(Type.VOLUME, "in³", "Cubic Inch", new BigDecimal("0.0000163871"))),
        // Voltage
        MILLIVOLT(new UnitBean(Type.VOLTAGE, "mV", "Millivolt", new BigDecimal("1.0E-3"))),
        VOLT(new UnitBean(Type.VOLTAGE, "V", "Volt", new BigDecimal("1.0E0"))),
        KILOVOLT(new UnitBean(Type.VOLTAGE, "kV", "Kilovolt", new BigDecimal("1.0E3"))),
        MEGAVOLT(new UnitBean(Type.VOLTAGE, "MV", "Megavolt", new BigDecimal("1.0E6"))),
        // Current
        PICOAMPERE(new UnitBean(Type.CURRENT, "pA", "Picoampere", new BigDecimal("1.0E-12"))),
        NANOAMPERE(new UnitBean(Type.CURRENT, "nA", "Nanoampere", new BigDecimal("1.0E-9"))),
        MICROAMPERE(new UnitBean(Type.CURRENT, "µA", "Microampere", new BigDecimal("1.0E-6"))),
        MILLIAMPERE(new UnitBean(Type.CURRENT, "mA", "Milliampere", new BigDecimal("1.0E-3"))),
        AMPERE(new UnitBean(Type.CURRENT, "A", "Ampere", new BigDecimal("1.0"))),
        KILOAMPERE(new UnitBean(Type.CURRENT, "kA", "Kiloampere", new BigDecimal("1.0E3"))),
        // Speed
        MILLIMETER_PER_SECOND(new UnitBean(Type.SPEED, "mm/s", "Millimeter per second", new BigDecimal("1.0E-3"))),
        METER_PER_SECOND(new UnitBean(Type.SPEED, "m/s", "Meter per second", new BigDecimal("1.0E0"))),
        KILOMETER_PER_HOUR(new UnitBean(Type.SPEED, "km/h", "Kilometer per hour", new BigDecimal("0.2777777778"))),
        MILES_PER_HOUR(new UnitBean(Type.SPEED, "mph", "Miles per hour", new BigDecimal("0.4472271914"))),
        KNOT(new UnitBean(Type.SPEED, "kt", "Knot", new BigDecimal("0.51444444444444"))),
        // TemperatureGradient
        KELVIN_PER_SECOND(new UnitBean(Type.TEMPERATURE_GRADIENT, "K/s", "Kelvin per second", new BigDecimal("1.0"))),
        KELVIN_PER_MINUTE(new UnitBean(Type.TEMPERATURE_GRADIENT, "K/min", "Kelvin per minute", new BigDecimal("0.0166666667"))),
        KEVLIN_PER_HOUR(new UnitBean(Type.TEMPERATURE_GRADIENT, "K/h", "Kelvin per hour", new BigDecimal("0.0002777778"))),
        // ElectricCharge
        ELEMENTARY_CHARGE(new UnitBean(Type.ELECTRIC_CHARGE, "e-", "Elementary charge", new BigDecimal("1.6022E-19"))),
        PICOCOULOMB(new UnitBean(Type.ELECTRIC_CHARGE, "pC", "Picocoulomb", new BigDecimal("1.0E-12"))),
        NANOCOULOMB(new UnitBean(Type.ELECTRIC_CHARGE, "nC", "Nanocoulomb", new BigDecimal("1.0E-9"))),
        MICROCOULOMB(new UnitBean(Type.ELECTRIC_CHARGE, "µC", "Microcoulomb", new BigDecimal("1.0E-6"))),
        MILLICOULOMB(new UnitBean(Type.ELECTRIC_CHARGE, "mC", "Millicoulomb", new BigDecimal("1.0E-3"))),
        COULOMB(new UnitBean(Type.ELECTRIC_CHARGE, "C", "Coulomb", new BigDecimal("1.0E0"))),
        // Energy
        MILLIJOULE(new UnitBean(Type.ENERGY, "mJ", "Millijoule", new BigDecimal("1.0E-3"))),
        JOULE(new UnitBean(Type.ENERGY, "J", "Joule", new BigDecimal("1.0E0"))),
        KILOJOULE(new UnitBean(Type.ENERGY, "kJ", "Kilojoule", new BigDecimal("1.0E3"))),
        MEGAJOULE(new UnitBean(Type.ENERGY, "MJ", "Megajoule", new BigDecimal("1.0E6"))),
        CALORY(new UnitBean(Type.ENERGY, "cal", "Calory", new BigDecimal("4.1868"))),
        KILOCALORY(new UnitBean(Type.ENERGY, "kcal", "Kilocalory", new BigDecimal("4186.8"))),
        WATT_SECOND(new UnitBean(Type.ENERGY, "W*s", "Watt second", new BigDecimal("1.0E0"))),
        WATT_HOUR(new UnitBean(Type.ENERGY, "W*h", "Watt hour",new BigDecimal("3600"))),
        KILOWATT_SECOND(new UnitBean(Type.ENERGY, "kW*s", "Kilowatt second", new BigDecimal("1000"))),
        KILOWATT_HOUR(new UnitBean(Type.ENERGY, "kW*h", "Kilowatt hour", new BigDecimal("3600000"))),
        // Force
        NEWTON(new UnitBean(Type.FORCE, "N", "Newton", new BigDecimal("1.0"))),
        KILOGRAM_FORCE(new UnitBean(Type.FORCE, "kp", "Kilogram-Force", new BigDecimal("9.80665"))),
        POUND_FORCE(new UnitBean(Type.FORCE, "lbf", "Pound-Force", new BigDecimal("4.4482216153"))),
        // Humidity
        PERCENTAGE(new UnitBean(Type.HUMIDITY, "%", "Percentage", new BigDecimal(1.0))),
        // Acceleration
        METER_PER_SQUARE_SECOND(new UnitBean(Type.ACCELERATION, "m/s²","Meter per squaresecond",new BigDecimal("1.0E0"))),
        INCH_PER_SQUARE_SECOND(new UnitBean(Type.ACCELERATION, "in/s²","Inch per squaresecond",new BigDecimal("0.0254"))),
        GRAVITY(new UnitBean(Type.ACCELERATION, "g","Gravity",new BigDecimal("9.80665"))),
        // Pressure
        MILLIPASCAL(new UnitBean(Type.PRESSURE, "mPa","Millipascal",new BigDecimal("1.0E-3"))),
        PASCAL(new UnitBean(Type.PRESSURE, "Pa","Pascal",new BigDecimal("1.0E0"))),
        HECTOPASCAL(new UnitBean(Type.PRESSURE, "hPa","Hectopascal",new BigDecimal("1.0E2"))),
        KILOPASCAL(new UnitBean(Type.PRESSURE, "kPa","Kilopascal",new BigDecimal("1.0E3"))),
        BAR(new UnitBean(Type.PRESSURE, "bar","Bar",new BigDecimal("1.0E5"))),
        MILLIBAR(new UnitBean(Type.PRESSURE, "mbar","Millibar",new BigDecimal("1.0E2"))),
        TORR(new UnitBean(Type.PRESSURE, "Torr","Torr",new BigDecimal("133.322368421"))),
        PSI(new UnitBean(Type.PRESSURE, "psi","Pound per Square Inch",new BigDecimal("6894.757293178"))),
        PSF(new UnitBean(Type.PRESSURE, "psf","Pound per Square Foot",new BigDecimal("47.88026"))),
        ATMOSPHERE(new UnitBean(Type.PRESSURE, "atm","Atmosphere",new BigDecimal("101325.0"))),
        // Torque
        NEWTON_METER(new UnitBean(Type.TORQUE, "Nm","Newton Meter",new BigDecimal("1.0"))),
        METER_KILOGRAM(new UnitBean(Type.TORQUE, "m kg","Meter Kilogram",new BigDecimal("0.101971621"))),
        FOOT_POUND_FORCE(new UnitBean(Type.TORQUE, "ft lbf","Foot-Pound Force",new BigDecimal("1.3558179483"))),
        INCH_POUND_FORCE(new UnitBean(Type.TORQUE, "in lbf","Inch-Pound Force",new BigDecimal("0.112984829"))),
        // Data
        BIT(new UnitBean(Type.DATA, "b","Bit",new BigDecimal("1.0"))),
        KILOBIT(new UnitBean(Type.DATA, "Kb","KiloBit",new BigDecimal("1024"))),
        MEGABIT(new UnitBean(Type.DATA, "Mb","Megabit",new BigDecimal("1048576"))),
        GIGABIT(new UnitBean(Type.DATA, "Gb","Gigabit",new BigDecimal("1073741824"))),
        BYTE(new UnitBean(Type.DATA, "B","Byte",new BigDecimal("8"))),
        KILOBYTE(new UnitBean(Type.DATA, "KB","Kilobyte",new BigDecimal("8192"))),
        MEGABYTE(new UnitBean(Type.DATA, "MB","Megabyte",new BigDecimal("8388608"))),
        GIGABYTE(new UnitBean(Type.DATA, "GB","Gigabyte",new BigDecimal("8.589934592E9"))),
        TERABYTE(new UnitBean(Type.DATA, "TB","Terabyte",new BigDecimal("8.796093E12"))),
        // Luminance
        CANDELA_SQUAREMETER(new UnitBean(Type.LUMINANCE, "cd/m²","Candela per Square Meter",new BigDecimal("1.0"))),
        CANDELA_SQUARECENTIMETER(new UnitBean(Type.LUMINANCE, "cd/cm²","Candela per Square CentiMeter",new BigDecimal("10000.0"))),
        CANDELA_SQUAREINCH(new UnitBean(Type.LUMINANCE, "cd/in²","Candela per Square Inch",new BigDecimal("1550.0031"))),
        CANDELA_SQAUREFOOT(new UnitBean(Type.LUMINANCE, "cd/ft²","Candela per Square Foot",new BigDecimal("10.7639104167"))),
        LAMBERT(new UnitBean(Type.LUMINANCE, "L","Lambert",new BigDecimal("3183.09886183"))),
        FOOTLAMBERT(new UnitBean(Type.LUMINANCE, "fL","Footlambert",new BigDecimal("3.4262590996"))),
        // Luminous flux
        LUX(new UnitBean(Type.LUMINOUS_FLUX, "lm/m²","Lux",new BigDecimal("1.0"))),
        PHOT(new UnitBean(Type.LUMINOUS_FLUX, "lm/cm²","Phot",new BigDecimal("10000.0"))),
        FOOT_CANDLE(new UnitBean(Type.LUMINOUS_FLUX, "lm/ft²","Footcandle",new BigDecimal("10.7639104167"))),
        LUMEN_SQUARE_INCH(new UnitBean(Type.LUMINOUS_FLUX, "lm/in²","Lumen per Square Inch",new BigDecimal("1550.0031"))),
        // Work
        MILLIWATT(new UnitBean(Type.WORK, "mW","Milliwatt",new BigDecimal("1.0E-3"))),
        WATT(new UnitBean(Type.WORK, "W","Watt",new BigDecimal("1.0E0"))),
        KILOWATT(new UnitBean(Type.WORK, "kW","Kilowatt",new BigDecimal("1.0E3"))),
        MEGAWATT(new UnitBean(Type.WORK, "MW","Megawatt",new BigDecimal("1.0E6"))),
        GIGAWATT(new UnitBean(Type.WORK, "GW","Gigawatt",new BigDecimal("1.0E9"))),
        HORSEPOWER(new UnitBean(Type.WORK, "hp","Horsepower",new BigDecimal("735.49875"))),
        JOULE_PER_SECOND(new UnitBean(Type.WORK, "J/s","Joule per second",new BigDecimal("1.0E0"))),
        // Currency
        AED(new UnitBean(Type.CURRENCY, "AED", "UAE Dirham", new BigDecimal("0.27218"), false)),
        AFN(new UnitBean(Type.CURRENCY, "AFA", "Afghanistan Afghani", new BigDecimal("0.01983"), false)),
        ALL(new UnitBean(Type.CURRENCY, "ALL", "Albanian Lek", new BigDecimal("0.00880"), false)),
        ANG(new UnitBean(Type.CURRENCY, "ANG", "Neth Antilles Guilder", new BigDecimal("0.54945"), false)),
        ARS(new UnitBean(Type.CURRENCY, "ARS", "Argentine Peso", new BigDecimal("0.22270"))),
        AUD(new UnitBean(Type.CURRENCY, "AUD", "Australian Dollar", new BigDecimal("0.96937"))),
        AWG(new UnitBean(Type.CURRENCY, "AWG", "Aruba Florin", new BigDecimal("0.55559"), false)),
        BBD(new UnitBean(Type.CURRENCY, "BBD", "Barbados Dollar", new BigDecimal("0.49435"), false)),
        BDT(new UnitBean(Type.CURRENCY, "BDT", "Bangladesh Taka", new BigDecimal("0.01207"), false)),
        BHD(new UnitBean(Type.CURRENCY, "BHD", "Bahraini Dinar", new BigDecimal("2.60756"), false)),
        BIF(new UnitBean(Type.CURRENCY, "BIF", "Burundi Franc", new BigDecimal("0.00070"), false)),
        BMD(new UnitBean(Type.CURRENCY, "BMD", "Bermuda Dollar", new BigDecimal("1.0"), false)),
        BND(new UnitBean(Type.CURRENCY, "BND", "Brunei Dollar", new BigDecimal("0.76705"), false)),
        BOB(new UnitBean(Type.CURRENCY, "BOB", "Bolivian Boliviano", new BigDecimal("0.14065"), false)),
        BRL(new UnitBean(Type.CURRENCY, "BRL", "Brazilian Real", new BigDecimal("0.48988"))),
        BSD(new UnitBean(Type.CURRENCY, "BSD", "Bahamian Dollar", new BigDecimal("0.98572"), false)),
        BTN(new UnitBean(Type.CURRENCY, "BTN", "Bhutan Ngultrum", new BigDecimal("0.01800"), false)),
        BWP(new UnitBean(Type.CURRENCY, "BWP", "Botswana Pula", new BigDecimal("0.12535"), false)),
        BZD(new UnitBean(Type.CURRENCY, "BZD", "Belize Dollar", new BigDecimal("0.51622"), false)),
        CAD(new UnitBean(Type.CURRENCY, "CAD", "Canadian Dollar", new BigDecimal("0.96024"))),
        CHF(new UnitBean(Type.CURRENCY, "CHF", "Swiss Franc", new BigDecimal("1.03391"))),
        CLP(new UnitBean(Type.CURRENCY, "CLP", "Chilean Peso", new BigDecimal("0.00192"), false)),
        CNY(new UnitBean(Type.CURRENCY, "CNY", "Chinese Yuan", new BigDecimal("0.15691"))),
        COP(new UnitBean(Type.CURRENCY, "COP", "Colombian Peso", new BigDecimal("0.00054"), false)),
        CRC(new UnitBean(Type.CURRENCY, "CRC", "Costa Rica Colon", new BigDecimal("0.00197"), false)),
        CUP(new UnitBean(Type.CURRENCY, "CUP", "Cuban Peso", new BigDecimal("0.04320"), false)),
        CVE(new UnitBean(Type.CURRENCY, "CVE", "Cape Verde Escudo", new BigDecimal("0.01123"), false)),
        CYP(new UnitBean(Type.CURRENCY, "CYP", "Cyprus Pound", new BigDecimal("2.12106"), false)),
        CZK(new UnitBean(Type.CURRENCY, "CZK", "Czech Koruna", new BigDecimal("0.04818"), false)),
        DJF(new UnitBean(Type.CURRENCY, "DJF", "Dijibouti Franc", new BigDecimal("0.00563"), false)),
        DKK(new UnitBean(Type.CURRENCY, "DKK", "Danish Krone", new BigDecimal("0.16728"))),
        DOP(new UnitBean(Type.CURRENCY, "DOP", "Dominican Peso", new BigDecimal("0.02551"), false)),
        DZD(new UnitBean(Type.CURRENCY, "DZD", "Algerian Dinar", new BigDecimal("0.01286"), false)),
        EEK(new UnitBean(Type.CURRENCY, "EEK", "Estonian Kroon", new BigDecimal("0.08518"), false)),
        EGP(new UnitBean(Type.CURRENCY, "EGP", "Egyptian Pound", new BigDecimal("0.16403"), false)),
        ETB(new UnitBean(Type.CURRENCY, "ETB", "Ethiopian Birr", new BigDecimal("0.05636"), false)),
        EUR(new UnitBean(Type.CURRENCY, "EUR", "Euro", new BigDecimal("1.24140"))),
        FKP(new UnitBean(Type.CURRENCY, "FKP", "Falkland Islands Pound", new BigDecimal("1.53589"), false)),
        GBP(new UnitBean(Type.CURRENCY, "GBP", "British Pound", new BigDecimal("1.53575"))),
        GHC(new UnitBean(Type.CURRENCY, "GHC", "Ghanian Cedi", new BigDecimal("0.00005"), false)),
        GIP(new UnitBean(Type.CURRENCY, "GIP", "Gibraltar Pound", new BigDecimal("1.53589"), false)),
        GMD(new UnitBean(Type.CURRENCY, "GMD", "Gambian Dalasi", new BigDecimal("0.03190"), false)),
        GNF(new UnitBean(Type.CURRENCY, "GNF", "Guinea Franc", new BigDecimal("0.00014"), false)),
        GTQ(new UnitBean(Type.CURRENCY, "GTQ", "Guatemala Quetzal", new BigDecimal("0.12662"), false)),
        GYD(new UnitBean(Type.CURRENCY, "GYD", "Guyana Dollar", new BigDecimal("0.00500"), false)),
        HKD(new UnitBean(Type.CURRENCY, "HKD", "Hong Kong Dollar", new BigDecimal("0.12883"))),
        HNL(new UnitBean(Type.CURRENCY, "HNL", "Honduras Lempira", new BigDecimal("0.05184"), false)),
        HRK(new UnitBean(Type.CURRENCY, "HRK", "Croatian Kuna", new BigDecimal("0.16453"), false)),
        HTG(new UnitBean(Type.CURRENCY, "HTG", "Haiti Gourde", new BigDecimal("0.02358"), false)),
        HUF(new UnitBean(Type.CURRENCY, "HUF", "Hungarian Forint", new BigDecimal("0.00407"), false)),
        IDR(new UnitBean(Type.CURRENCY, "IDR", "Indonesian Rupiah", new BigDecimal("0.00011"), false)),
        ILS(new UnitBean(Type.CURRENCY, "ILS", "Israeli Shekel", new BigDecimal("0.25572"), false)),
        INR(new UnitBean(Type.CURRENCY, "INR", "Indian Rupee", new BigDecimal("0.01781"), false)),
        IQD(new UnitBean(Type.CURRENCY, "IQD", "Iraqi Dinar", new BigDecimal("0.00085"), false)),
        ISK(new UnitBean(Type.CURRENCY, "ISK", "Iceland Krona", new BigDecimal("0.00762"), false)),
        JMD(new UnitBean(Type.CURRENCY, "JMD", "Jamaican Dollar", new BigDecimal("001134"), false)),
        JOD(new UnitBean(Type.CURRENCY, "JOD", "Jordanian Dinar", new BigDecimal("1.40528"), false)),
        JPY(new UnitBean(Type.CURRENCY, "JPY", "Japanese Yen", new BigDecimal("0.01281"))),
        KES(new UnitBean(Type.CURRENCY, "KES", "Kenyan Shilling", new BigDecimal("0.01144"), false)),
        KHR(new UnitBean(Type.CURRENCY, "KHR", "Cambodia Riel", new BigDecimal("0.00024"), false)),
        KMF(new UnitBean(Type.CURRENCY, "KMF", "Comoros Franc", new BigDecimal("0.00252"), false)),
        KPW(new UnitBean(Type.CURRENCY, "KPW", "North Korean Won", new BigDecimal("0.00741"), false)),
        KRW(new UnitBean(Type.CURRENCY, "KRW", "Korean Won", new BigDecimal("0.00084"), false)),
        KWD(new UnitBean(Type.CURRENCY, "KWD", "Kuwaiti Dinar", new BigDecimal("3.54761"), false)),
        KYD(new UnitBean(Type.CURRENCY, "KYD", "Cayman Islands Dollar", new BigDecimal("1.19910"), false)),
        KZT(new UnitBean(Type.CURRENCY, "KZT", "Kazakhstan Tenge", new BigDecimal("0.00669"), false)),
        LAK(new UnitBean(Type.CURRENCY, "LAK", "Lao Kip", new BigDecimal("0.00012"), false)),
        LBP(new UnitBean(Type.CURRENCY, "LBP", "Lebanese Pound", new BigDecimal("0.00066"), false)),
        LKR(new UnitBean(Type.CURRENCY, "LKR", "Sri Lanka Rupee", new BigDecimal("0.00758"), false)),
        LRD(new UnitBean(Type.CURRENCY, "LRD", "Liberian Dollar", new BigDecimal("0.01333"), false)),
        LSL(new UnitBean(Type.CURRENCY, "LSL", "Lesotho Loti", new BigDecimal("0.11434"), false)),
        LTL(new UnitBean(Type.CURRENCY, "LTL", "Lithuanian Lita", new BigDecimal("0.35564"), false)),
        LVL(new UnitBean(Type.CURRENCY, "LVL", "Latvian Lat", new BigDecimal("1.76019"), false)),
        LYD(new UnitBean(Type.CURRENCY, "LYD", "Libyan Dinar", new BigDecimal("0.77172"), false)),
        MAD(new UnitBean(Type.CURRENCY, "MAD", "Moroccan Dirham", new BigDecimal("0.10980"), false)),
        MDL(new UnitBean(Type.CURRENCY, "MDL", "Moldovan Leu", new BigDecimal("0.08212"), false)),
        MKD(new UnitBean(Type.CURRENCY, "MKD", "Macedonian Denar", new BigDecimal("0.02034"), false)),
        MMK(new UnitBean(Type.CURRENCY, "MMK", "Myanmar Kyat", new BigDecimal("0.00117"), false)),
        MNT(new UnitBean(Type.CURRENCY, "MNT", "Mongolian Tugrik", new BigDecimal("0.00076"), false)),
        MOP(new UnitBean(Type.CURRENCY, "MOP", "Macau Pataca", new BigDecimal("0.12368"), false)),
        MRO(new UnitBean(Type.CURRENCY, "MRO", "Mauritania Ougulya", new BigDecimal("1.0"), false)),
        MUR(new UnitBean(Type.CURRENCY, "MUR", "Mauritius Rupee", new BigDecimal("0.00337"), false)),
        MVR(new UnitBean(Type.CURRENCY, "MVR", "Maldives Rufiyaa", new BigDecimal("0.06341"), false)),
        MWK(new UnitBean(Type.CURRENCY, "MWK", "Malawi Kwacha", new BigDecimal("0.00369"), false)),
        MXN(new UnitBean(Type.CURRENCY, "MXN", "Mexican Peso", new BigDecimal("0.06977"))),
        MYR(new UnitBean(Type.CURRENCY, "MYR", "Malaysian Ringgit", new BigDecimal("0.31102"), false)),
        NAD(new UnitBean(Type.CURRENCY, "NAD", "Namibian Dollar", new BigDecimal("0.11434"), false)),
        NGN(new UnitBean(Type.CURRENCY, "NGN", "Nigerian Naira", new BigDecimal("0.00621"), false)),
        NIO(new UnitBean(Type.CURRENCY, "NIO", "Nicaragua Cordoba", new BigDecimal("0.04246"), false)),
        NOK(new UnitBean(Type.CURRENCY, "NOK", "Norwegian Krone", new BigDecimal("0.16302"))),
        NPR(new UnitBean(Type.CURRENCY, "NPR", "Nepalese Rupee", new BigDecimal("0.01153"), false)),
        NZD(new UnitBean(Type.CURRENCY, "NZD", "New Zealand Dollar", new BigDecimal("0.75391"))),
        OMR(new UnitBean(Type.CURRENCY, "OMR", "Omani Rial", new BigDecimal("2.58893"), false)),
        PAB(new UnitBean(Type.CURRENCY, "PAB", "Panama Balboa", new BigDecimal("0.97124"), false)),
        PEN(new UnitBean(Type.CURRENCY, "PEN", "Peruvian Nuevo Sol", new BigDecimal("0.36385"), false)),
        PGK(new UnitBean(Type.CURRENCY, "PGK", "Papua New Guinea Kina", new BigDecimal("0.48473"), false)),
        PHP(new UnitBean(Type.CURRENCY, "PHP", "Philippine Peso", new BigDecimal("0.02295"), false)),
        PKR(new UnitBean(Type.CURRENCY, "PKR", "Pakistani Rupee", new BigDecimal("0.01063"), false)),
        PLN(new UnitBean(Type.CURRENCY, "PLN", "Polish Zloty", new BigDecimal("0.27811"))),
        PYG(new UnitBean(Type.CURRENCY, "PYG", "Paraguayan Guarani", new BigDecimal("0.00022"), false)),
        QAR(new UnitBean(Type.CURRENCY, "QAR", "Qatar Rial", new BigDecimal("0.27065"), false)),
        RUB(new UnitBean(Type.CURRENCY, "RUB", "Russian Rouble", new BigDecimal("0.02913"))),
        SAR(new UnitBean(Type.CURRENCY, "SAR", "Saudi Arabian Riyal", new BigDecimal("0.26655"), false)),
        SBD(new UnitBean(Type.CURRENCY, "SBD", "Solomon Islands Dollar", new BigDecimal("0.13620"), false)),
        SCR(new UnitBean(Type.CURRENCY, "SCR", "Seychelles Rupee", new BigDecimal("0.06559"), false)),
        SEK(new UnitBean(Type.CURRENCY, "SEK", "Swedish Krona", new BigDecimal("0.13785"))),
        SGD(new UnitBean(Type.CURRENCY, "SGD", "Singapore Dollar", new BigDecimal("0.77322"))),
        SHP(new UnitBean(Type.CURRENCY, "SHP", "St Helena Pound", new BigDecimal("1.53450"), false)),
        SLL(new UnitBean(Type.CURRENCY, "SLL", "Sierra Leone Leone", new BigDecimal("0.00023"), false)),
        SOS(new UnitBean(Type.CURRENCY, "SOS", "Somali Shilling", new BigDecimal("0.00061"), false)),
        STD(new UnitBean(Type.CURRENCY, "STD", "Sao Tome Dobra", new BigDecimal("0.00005"), false)),
        SVC(new UnitBean(Type.CURRENCY, "SVC", "El Salvador Colon", new BigDecimal("0.11280"), false)),
        SYP(new UnitBean(Type.CURRENCY, "SYP", "Syrian Pound", new BigDecimal("0.01551"), false)),
        SZL(new UnitBean(Type.CURRENCY, "SZL", "Swaziland Lilageni", new BigDecimal("0.11465"), false)),
        THB(new UnitBean(Type.CURRENCY, "THB", "Thai Baht", new BigDecimal("0.03117"))),
        TND(new UnitBean(Type.CURRENCY, "TND", "Tunisian Dinar", new BigDecimal("0.61652"), false)),
        TOP(new UnitBean(Type.CURRENCY, "TOP", "Tonga Pa'anga", new BigDecimal("0.55602"), false)),
        TRY(new UnitBean(Type.CURRENCY, "TRY", "Turkey Lira", new BigDecimal("0.53792"))),
        TTD(new UnitBean(Type.CURRENCY, "TTD", "Trinidad/Tobago Dollar", new BigDecimal("0.15467"), false)),
        TWD(new UnitBean(Type.CURRENCY, "TWD", "Taiwan Dollar", new BigDecimal("0.03286"))),
        TZS(new UnitBean(Type.CURRENCY, "TZS", "Tanzanian Shilling", new BigDecimal("0.00063"), false)),
        UAH(new UnitBean(Type.CURRENCY, "UAH", "Ukraine Hryvnia", new BigDecimal("0.12301"), false)),
        UGX(new UnitBean(Type.CURRENCY, "UGX", "Ugandan Shilling", new BigDecimal("0.00040"), false)),
        USD(new UnitBean(Type.CURRENCY, "USD", "U.S. Dollar", new BigDecimal("1.0"))),
        UYU(new UnitBean(Type.CURRENCY, "UYU", "Uruguayan New Peso", new BigDecimal("0.04795"), false)),
        VND(new UnitBean(Type.CURRENCY, "VND", "Vietnam Dong", new BigDecimal("0.00005"), false)),
        VUV(new UnitBean(Type.CURRENCY, "VUV", "Vanuatu Vatu", new BigDecimal("0.01052"), false)),
        WST(new UnitBean(Type.CURRENCY, "WST", "Samoa Tala", new BigDecimal("0.41220"), false)),
        XAF(new UnitBean(Type.CURRENCY, "XAF", "CFA Franc (BEAC)", new BigDecimal("0.00186"), false)),
        XAG(new UnitBean(Type.CURRENCY, "XAG", "Silver Ounces", new BigDecimal("28.3739"))),
        XAU(new UnitBean(Type.CURRENCY, "XAU", "Gold Ounces", new BigDecimal("1621.75"))),
        XCD(new UnitBean(Type.CURRENCY, "XCD", "East Caribbean Dollar", new BigDecimal("0.36807"), false)),
        XOF(new UnitBean(Type.CURRENCY, "XOF", "CFA Franc (BCEAO)", new BigDecimal("0.00186"), false)),
        XPD(new UnitBean(Type.CURRENCY, "XPD", "Palladium Ounces", new BigDecimal("609.900"), false)),
        XPF(new UnitBean(Type.CURRENCY, "XPF", "Pacific Franc", new BigDecimal("0.01022"), false)),
        XPT(new UnitBean(Type.CURRENCY, "XPT", "Platinum Ounces", new BigDecimal("1437.97"))),
        YER(new UnitBean(Type.CURRENCY, "YER", "Yemen Riyal", new BigDecimal("0.00462"), false)),
        ZAR(new UnitBean(Type.CURRENCY, "ZAR", "South African Rand", new BigDecimal("0.11589"), false)),
        ZMK(new UnitBean(Type.CURRENCY, "ZMK", "Zambian Kwacha", new BigDecimal("0.00019"), false));

        public final UnitBean BEAN;

        private Definition(final UnitBean BEAN) {
            this.BEAN = BEAN;
        }
    }

    private static final EnumMap<Type, Definition> BASE_UNITS = new EnumMap<Type, Definition>(Type.class) {
        {
            put(Type.ACCELERATION, Definition.METER_PER_SQUARE_SECOND);
            put(Type.ANGLE, Definition.RADIAN);
            put(Type.AREA, Definition.SQUARE_METER);
            put(Type.CURRENCY, Definition.USD);
            put(Type.CURRENT, Definition.AMPERE);
            put(Type.DATA, Definition.BIT);
            put(Type.ELECTRIC_CHARGE, Definition.ELEMENTARY_CHARGE);
            put(Type.ENERGY, Definition.JOULE);
            put(Type.FORCE, Definition.NEWTON);
            put(Type.HUMIDITY, Definition.PERCENTAGE);
            put(Type.LENGTH, Definition.METER);
            put(Type.LUMINANCE, Definition.CANDELA_SQUAREMETER);
            put(Type.LUMINOUS_FLUX, Definition.LUX);
            put(Type.MASS, Definition.KILOGRAM);
            put(Type.PRESSURE, Definition.PASCAL);
            put(Type.SPEED, Definition.METER_PER_SECOND);
            put(Type.TEMPERATURE, Definition.KELVIN);
            put(Type.TEMPERATURE_GRADIENT, Definition.KELVIN_PER_SECOND);
            put(Type.TIME, Definition.SECOND);
            put(Type.TORQUE, Definition.NEWTON_METER);
            put(Type.VOLUME, Definition.CUBIC_MILLIMETER);
            put(Type.VOLTAGE, Definition.VOLT);
            put(Type.WORK, Definition.WATT);
        }
    };
    private Definition baseUnit;
    private UnitBean   bean;


    // ******************** Constructors **************************************
    public Unit(final Type UNIT_TYPE) {
        this(UNIT_TYPE, BASE_UNITS.get(UNIT_TYPE));
    }
    public Unit(final Type UNIT_TYPE, final Definition BASE_UNIT) {
        baseUnit = BASE_UNIT;
        bean     = BASE_UNITS.get(UNIT_TYPE).BEAN;
    }


    // ******************** Methods *******************************************
    public final Type getUnitType() { return bean.getUnitType(); }

    public final Definition getBaseUnit() { return baseUnit; }
    public final void setBaseUnit(final Definition BASE_UNIT) {
        if (BASE_UNIT.BEAN.getUnitType() == getUnitType()) { baseUnit = BASE_UNIT; }
    }

    public final BigDecimal getFactor() { return bean.getFactor(); }

    public final BigDecimal getOffset() { return bean.getOffset(); }

    public final String getUnitName() { return bean.getUnitName(); }

    public final String getUnitShort() { return bean.getUnitShort(); }

    public final boolean isActive() { return bean.isActive(); }
    public final void setActive(final boolean ACTIVE) { bean.setActive(ACTIVE); }

    public final double convert(final double VALUE, final Definition UNIT) {
        if (UNIT.BEAN.getUnitType() != getUnitType()) { throw new IllegalArgumentException("units have to be of the same type"); }
        return ((((VALUE + baseUnit.BEAN.getOffset().doubleValue()) * baseUnit.BEAN.getFactor().doubleValue()) + bean.getOffset().doubleValue()) * bean.getFactor().doubleValue()) / UNIT.BEAN.getFactor().doubleValue() - UNIT.BEAN.getOffset().doubleValue();
    }

    public final double convertToBaseUnit(final double VALUE, final Definition UNIT) {
        return ((((VALUE + UNIT.BEAN.getOffset().doubleValue()) * UNIT.BEAN.getFactor().doubleValue()) + bean.getOffset().doubleValue()) * bean.getFactor().doubleValue()) / baseUnit.BEAN.getFactor().doubleValue() - baseUnit.BEAN.getOffset().doubleValue();
    }

    public final Pattern getPattern() {
        final StringBuilder PATTERN_BUILDER = new StringBuilder();
        PATTERN_BUILDER.append("^([-+]?\\d*\\.?\\d*)\\s?(");

        for (Definition unit : Definition.values()) {
            PATTERN_BUILDER.append(unit.BEAN.getUnitShort().replace("*", "\\*")).append("|");
        }

        PATTERN_BUILDER.deleteCharAt(PATTERN_BUILDER.length() - 1);

        //PATTERN_BUILDER.append("){1}$");
        PATTERN_BUILDER.append(")?$");

        return Pattern.compile(PATTERN_BUILDER.toString());
    }

    public static final void updateCurrency(final String CURRENCY_SHORT, final BigDecimal FACTOR) {
        for (UnitBean bean : Unit.getAvailableUnits(Unit.Type.CURRENCY)) {
            if (bean.getUnitShort().equals(CURRENCY_SHORT.toUpperCase())) bean.setFactor(FACTOR);
        }
    }

    public static final void updateCurrencies(final Map<String, BigDecimal> CURRENCIES) {
        for (String currencyShort : CURRENCIES.keySet()) { updateCurrency(currencyShort, CURRENCIES.get(currencyShort)); }
    }

    public static final List<UnitBean> getCurrencies() { return getAvailableUnits(Type.CURRENCY); }
    
    public static final List<UnitBean> getAvailableUnits(final Type UNIT_TYPE) {
        List<UnitBean> availableUnits = getAllUnitTypes().get(UNIT_TYPE).stream()
                                                         .map(unit -> unit.BEAN)
                                                         .collect(Collectors.toList());
        return availableUnits;
    }

    public static final EnumMap<Type, ArrayList<Definition>> getAllUnitTypes() {
        final EnumMap<Type, ArrayList<Definition>> UNIT_TYPES = new EnumMap<>(Type.class);
        final ArrayList<Type> TYPE_LIST = new ArrayList<>(Type.values().length);
        TYPE_LIST.addAll(Arrays.asList(Type.values()));
        TYPE_LIST.forEach(type -> UNIT_TYPES.put(type, new ArrayList<>()));
        for (Definition unit : Definition.values()) {
            UNIT_TYPES.get(unit.BEAN.getUnitType()).add(unit);
        }
        return UNIT_TYPES;
    }

    public final EnumMap<Type, ArrayList<Definition>> getAllActiveUnitTypes() {
        final EnumMap<Type, ArrayList<Definition>> UNIT_TYPES = new EnumMap<>(Type.class);
        final ArrayList<Type> TYPE_LIST = new ArrayList<>(Type.values().length);
        TYPE_LIST.addAll(Arrays.asList(Type.values()));
        TYPE_LIST.forEach(type -> UNIT_TYPES.put(type, new ArrayList<>()));
        for (Definition unit : Definition.values()) {
            if (unit.BEAN.isActive()) { UNIT_TYPES.get(unit.BEAN.getUnitType()).add(unit); }
        }
        return UNIT_TYPES;
    }

    @Override public String toString() { return getUnitType().toString(); }
}
