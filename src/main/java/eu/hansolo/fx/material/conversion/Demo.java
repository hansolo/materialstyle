/*
 * Copyright (c) 2015 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.conversion;


import eu.hansolo.fx.material.conversion.Unit.Definition;
import eu.hansolo.fx.material.conversion.Unit.Type;


/**
 * Created by hansolo on 09.12.15.
 */
public class Demo {

    public Demo() {
        Unit   tempUnit              = new Unit(Unit.Type.TEMPERATURE, Unit.Definition.CELSIUS); // Type Temperature with BaseUnit Celsius
        double temperatureCelsius    = 32.0;
        double temperatureFahrenheit = tempUnit.convert(temperatureCelsius, Unit.Definition.FAHRENHEIT);
        double temperatureKelvin     = tempUnit.convert(temperatureCelsius, Definition.KELVIN);
        System.out.println(temperatureCelsius + "°C   =>   " + temperatureFahrenheit + "°F    =>   " + temperatureKelvin + "°K");


        Unit   lengthUnit      = new Unit(Type.LENGTH, Definition.METER); // Type Length with BaseUnit Meter
        double lengthMeter     = 15.0;
        double lengthInches    = lengthUnit.convert(lengthMeter, Definition.INCHES);
        double lengthNanoMeter = lengthUnit.convert(lengthInches, Definition.NANOMETER);
        System.out.println(lengthMeter + " " + lengthUnit.getUnitShort() + "   =>   " + lengthInches + " in   =>   " + lengthNanoMeter + " nm");

        Unit   currencyUnit = new Unit(Type.CURRENCY); // Type Currency with BaseUnit USD
        double currencyUSD  = 15.0;
        double currencyCHF  = currencyUnit.convert(currencyUSD, Definition.CHF);
        double currencyEUR  = currencyUnit.convert(currencyUSD, Definition.EUR);
        System.out.println(currencyUSD + " " + currencyUnit.getUnitShort() + "   =>   " + currencyCHF + " CHF   =>   " + currencyEUR + " EUR");
    }

    public static void main(String[] args) {
        new Demo();
    }
}
