/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.DoublePropertyBase;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;



/**
 * Created by hansolo on 24.06.16.
 */
public class MaterialSplitPane extends SplitPane {
    private DoubleProperty dividerPositionPreset;


    public MaterialSplitPane() {
        this(null);
    }
    public MaterialSplitPane(final Node... NODES) {
        super(NODES);
        dividerPositionPreset = new DoublePropertyBase() {
            @Override public Object getBean() { return MaterialSplitPane.this; }
            @Override public String getName() { return "dividerPositionPreset"; }
        };
    }


    public double getDividerPositionPreset() { return dividerPositionPreset.get(); }
    public void setDividerPositionPreset(final double PRESET) { dividerPositionPreset.set(PRESET); }
    public DoubleProperty dividerPositionPresetProperty() { return dividerPositionPreset; }

    public void resetToDividerPositionPreset() { setDividerPositions(getDividerPositionPreset()); }
}
