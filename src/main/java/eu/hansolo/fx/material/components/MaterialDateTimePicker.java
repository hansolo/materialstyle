/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import com.sun.javafx.css.converters.ColorConverter;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.css.CssMetaData;
import javafx.css.PseudoClass;
import javafx.css.Styleable;
import javafx.css.StyleableObjectProperty;
import javafx.css.StyleableProperty;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.DatePicker;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import javafx.util.StringConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by hansolo on 31.05.16.
 */
public class MaterialDateTimePicker extends DatePicker {
    public  static final String           DEFAULT_FORMAT                = "dd.MM.yyyy HH:mm";
    private static final PseudoClass      INVALID_PSEUDO_CLASS          = PseudoClass.getPseudoClass("invalid");
    private static final PseudoClass      HAS_SELECTION                 = PseudoClass.getPseudoClass("has-selection");
    private static final boolean          VALID                         = false;
    private static final Color            DEFAULT_MATERIAL_DESIGN_COLOR = Color.web("#3f51b5");
    private static final Color            DEFAULT_PROMPT_TEXT_COLOR     = Color.web("#757575");
    private static final Color            DEFAULT_REQUIRED_TEXT_COLOR   = Color.web("#d50000");
    private static final Color            DEFAULT_INVALID_TEXT_COLOR    = Color.web("#d50000");
    private static final double           STD_FONT_SIZE                 = 13;
    private static final double           SMALL_FONT_SIZE               = 10;
    private static final double           DEFAULT_OFFSET_Y              = 4;
    private static final double           TOP_OFFSET_Y                  = 4;
    private static final double           BOTTOM_OFFSET_Y               = 14;
    private static final int              ANIMATION_DURATION            = 60;
    private DateTimeFormatter             formatter;
    private Text                          promptText;
    private Text                          requiredText;
    private Region                        resetIcon;
    private HBox                          text;
    private Text                          hintText;
    private boolean                       _required;
    private BooleanProperty               required;
    private boolean                       _hintTextAlwaysVisible;
    private BooleanProperty               hintTextAlwaysVisible;
    private BooleanProperty               invalid;
    private BooleanProperty               hasSelection;
    private DoubleProperty                fontSize;
    private String                        _hint;
    private StringProperty                hint;
    private ObjectProperty<Color>         materialDesignColor;
    private ObjectProperty<Color>         promptTextColor;
    private ObjectProperty<Color>         requiredTextColor;
    private ObjectProperty<Color>         invalidTextColor;
    private ObjectProperty<LocalDateTime> dateTime;
    private ObjectProperty<String>        format;
    private Timeline                      timeline;
    private EventHandler<MouseEvent>      consumer;


    // ******************** Constructors **************************************
    public MaterialDateTimePicker() {
        this(null, "", false);
    }
    public MaterialDateTimePicker(final LocalDateTime DATE_TIME) {
        this(DATE_TIME, "", false);
    }
    public MaterialDateTimePicker(final LocalDateTime DATE_TIME, final String PROMPT_TEXT) {
        this(DATE_TIME, PROMPT_TEXT, false);
    }
    public MaterialDateTimePicker(final boolean REQUIRED) {
        this(null, "", REQUIRED);
    }
    public MaterialDateTimePicker(final String PROMPT_TEXT) {
        this(null, PROMPT_TEXT, false);
    }
    public MaterialDateTimePicker(final String PROMPT_TEXT, final boolean REQUIRED) {
        this(null, PROMPT_TEXT, REQUIRED);
    }
    public MaterialDateTimePicker(final LocalDateTime DATE_TIME, final String PROMPT_TEXT, final boolean REQUIRED) {
        super(null == DATE_TIME ? null : DATE_TIME.toLocalDate());

        getStylesheets().add(MaterialDateTimePicker.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialDateTimePicker.class.getResource("material-components.css").toExternalForm());
        getStyleClass().addAll("material-field", "material-date-picker");

        setPromptText(PROMPT_TEXT);
        _required              = REQUIRED;
        _hintTextAlwaysVisible = false;
        _hint                  = "";
        formatter              = DateTimeFormatter.ofPattern(DEFAULT_FORMAT);
        dateTime               = new SimpleObjectProperty<>(MaterialDateTimePicker.this, "dateTime", DATE_TIME);
        format                 = new ObjectPropertyBase<String>(DEFAULT_FORMAT) {
            @Override protected void invalidated() {
                formatter = DateTimeFormatter.ofPattern(get());
            }
            @Override public Object getBean() { return MaterialDateTimePicker.this; }
            @Override public String getName() { return "format"; }
        };
        fontSize               = new SimpleDoubleProperty(MaterialDateTimePicker.this, "fontSize", 13);
        timeline               = new Timeline();
        consumer               = e -> e.consume();

        setFormat(DEFAULT_FORMAT);
        setConverter(new InternalConverter());

        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        final String  FONT_FAMILY = "Roboto Regular";
        final boolean IS_EMPTY    = null == getValue();

        promptText = new Text(getPromptText());
        promptText.getStyleClass().add("prompt-text");

        requiredText = new Text(" *");
        requiredText.getStyleClass().add("required-text");
        requiredText.setVisible(isRequired());

        text = new HBox(promptText, requiredText);
        text.getStyleClass().add("material-combo-box");
        text.setTranslateY(DEFAULT_OFFSET_Y);

        hintText = new Text(getHintText());
        hintText.getStyleClass().add("hint-text");
        hintText.setFont(Font.font(FONT_FAMILY, SMALL_FONT_SIZE));
        hintText.setTranslateY(STD_FONT_SIZE + BOTTOM_OFFSET_Y);
        hintText.setTextOrigin(VPos.TOP);
        hintText.setOpacity(0);

        resetIcon = new Region();
        resetIcon.setPrefSize(12, 12);
        resetIcon.getStyleClass().add("reset-icon");
        resetIcon.setPickOnBounds(true);
        resetIcon.setVisible(false);

        if (!isEditable() || isDisabled() || !IS_EMPTY) {
            promptText.setFont(Font.font(FONT_FAMILY, SMALL_FONT_SIZE));
            requiredText.setFont(Font.font(FONT_FAMILY, SMALL_FONT_SIZE));
            text.setTranslateY(DEFAULT_OFFSET_Y - STD_FONT_SIZE - TOP_OFFSET_Y);
        } else {
            promptText.setFont(Font.font(FONT_FAMILY, STD_FONT_SIZE));
            requiredText.setFont(Font.font(FONT_FAMILY, STD_FONT_SIZE));
        }

        getChildren().addAll(hintText, text, resetIcon);
    }

    private void registerListeners() {
        valueProperty().addListener(o -> handleTextAndFocus(isFocused()));
        valueProperty().addListener((o, ov, nv) -> {
            if (null == nv) {
                dateTime.set(null);
            } else {
                dateTime.set(null == dateTime.get() ? LocalDateTime.of(nv, LocalTime.now()) : LocalDateTime.of(nv, dateTime.get().toLocalTime()));
            }
        });
        dateTime.addListener((o, ov, nv) -> setValue(null == nv ? null : nv.toLocalDate()));
        getEditor().textProperty().addListener(o -> handleTextAndFocus(isFocused()));
        promptTextProperty().addListener(o -> promptText.setText(getPromptText()));
        focusedProperty().addListener(o -> handleTextAndFocus(isFocused()));
        promptTextColorProperty().addListener(o -> promptText.setFill(getPromptTextColor()));
        requiredTextColorProperty().addListener(o -> requiredText.setFill(getRequiredTextColor()));
        skinProperty().addListener(o1 -> {
            if (null == getSkin() || null == getScene()) return;
            fixEditableBug();
            editableProperty().addListener(o2 -> fixEditableBug());
        });
        fontSize.addListener(o -> {
            double size = fontSize.get();
            promptText.setFont(Font.font(size));
            requiredText.setFont(Font.font(size));
        });
        timeline.setOnFinished(evt -> {
            final boolean IS_EMPTY = isEditable() ? getEditor().getText().isEmpty() : null == getValue();

            if (!IS_EMPTY && isFocused()) setPromptTextColor(getMaterialDesignColor());

            if (!IS_EMPTY && text.getTranslateY() >= 0) {
                text.setTranslateY(DEFAULT_OFFSET_Y - STD_FONT_SIZE - TOP_OFFSET_Y);
                fontSize.set(SMALL_FONT_SIZE);
            }
        });
        resetIcon.setOnMousePressed(e -> getEditor().clear());
    }


    // ******************** Methods *******************************************
    @Override protected void layoutChildren() {
        super.layoutChildren();
        hintText.setTranslateX(isInvalid() ? hintText.getLayoutX() : -hintText.getLayoutX());
    }

    public boolean isRequired() { return null == required ? _required : required.get(); }
    public void setRequired(final boolean REQUIRED) {
        if (null == required) {
            _required = REQUIRED;
            requiredText.setVisible(_required);
        } else {
            required.set(REQUIRED);
        }
    }
    public BooleanProperty requiredProperty() {
        if (null == required) {
            required = new BooleanPropertyBase(_required) {
                @Override protected void invalidated() { requiredText.setVisible(get()); }
                @Override public Object getBean() { return MaterialDateTimePicker.this; }
                @Override public String getName() { return "required"; }
            };
        }
        return required;
    }

    public boolean isInvalid() { return null == invalid ? VALID : invalid.get(); }
    public void setInvalid(final boolean INVALID) { invalidProperty().set(INVALID); }
    public BooleanProperty invalidProperty() {
        if (null == invalid) {
            invalid = new BooleanPropertyBase(false) {
                @Override protected void invalidated() { pseudoClassStateChanged(INVALID_PSEUDO_CLASS, get()); }
                @Override public Object getBean() { return MaterialDateTimePicker.this; }
                @Override public String getName() { return "invalid"; }
            };
        }
        return invalid;
    }

    public String getHintText() { return null == hint ? _hint : hint.get(); }
    public void setHintText(final String TEXT) {
        if (null == hint) {
            _hint = TEXT;
            hintText.setText(_hint);
        } else {
            hint.set(TEXT);
        }
    }
    public StringProperty hintTextProperty() {
        if (null == hint) {
            hint = new StringPropertyBase(_hint) {
                @Override protected void invalidated() { hintText.setText(get()); }
                @Override public Object getBean() { return MaterialDateTimePicker.this; }
                @Override public String getName() { return "hint"; }
            };
        }
        return hint;
    }

    public boolean isHintTextAlwaysVisible() { return null == hintTextAlwaysVisible ? _hintTextAlwaysVisible : hintTextAlwaysVisible.get(); }
    public void setHintTextAlwaysVisible(final boolean VISIBLE) {
        if (null == hintTextAlwaysVisible) {
            _hintTextAlwaysVisible = VISIBLE;
            if (VISIBLE) hintText.setOpacity(1);
        } else {
            hintTextAlwaysVisible.set(VISIBLE);
        }
    }
    public BooleanProperty hintTextAlwaysVisibleProperty() {
        if (null == hintTextAlwaysVisible) {
            hintTextAlwaysVisible = new BooleanPropertyBase(_hintTextAlwaysVisible) {
                @Override protected void invalidated() { if (get()) hintText.setOpacity(1); }
                @Override public Object getBean() { return MaterialDateTimePicker.this; }
                @Override public String getName() { return "hintTextAlwaysVisible"; }
            };
        }
        return hintTextAlwaysVisible;
    }

    public boolean hasSelection() { return null != hasSelection; }
    public void setHasSelection(final boolean HAS_SELECTION) { hasSelectionProperty().set(HAS_SELECTION); }
    public BooleanProperty hasSelectionProperty() {
        if (null == hasSelection) {
            hasSelection = new BooleanPropertyBase(false) {
                @Override protected void invalidated() { pseudoClassStateChanged(HAS_SELECTION, get()); }
                @Override public Object getBean() { return MaterialDateTimePicker.this; }
                @Override public String getName() { return "hasSelection"; }
            };
        }
        return hasSelection;
    }

    public LocalDateTime getDateTimeValue() { return dateTime.get(); }
    public void setDateTime(final LocalDateTime DATE_TIME) { dateTime.set(DATE_TIME); }
    public ObjectProperty<LocalDateTime> dateTimeProperty() { return dateTime; }

    public String getFormat() { return format.get(); }
    public void setFormat(final String FORMAT) { format.set(FORMAT); }
    public ObjectProperty<String> formatProperty() { return format; }

    private void fixEditableBug() {
        Node arrowButton = super.lookup("#arrow-button");
        if (isEditable()) {
            arrowButton.removeEventFilter(MouseEvent.MOUSE_PRESSED, consumer);
            arrowButton.removeEventFilter(MouseEvent.MOUSE_RELEASED, consumer);
        } else {
            arrowButton.addEventFilter(MouseEvent.MOUSE_PRESSED, consumer);
            arrowButton.addEventFilter(MouseEvent.MOUSE_RELEASED, consumer);
        }
    }

    private void simulateEnterPressed() {
        getEditor().fireEvent(new KeyEvent(getEditor(), getEditor(), KeyEvent.KEY_PRESSED, null, null, KeyCode.ENTER, false, false, false, false));
    }


    // ******************** CSS Stylable Properties ***************************
    public Color getMaterialDesignColor() { return null == materialDesignColor ? DEFAULT_MATERIAL_DESIGN_COLOR : materialDesignColor.get(); }
    public void setMaterialDesignColor(final Color COLOR) { materialDesignColorProperty().set(COLOR); }
    public ObjectProperty<Color> materialDesignColorProperty() {
        if (null == materialDesignColor) {
            materialDesignColor = new StyleableObjectProperty<Color>(DEFAULT_MATERIAL_DESIGN_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialDateTimePicker.StyleableProperties.MATERIAL_DESIGN_COLOR; }
                @Override public Object getBean() { return MaterialDateTimePicker.this; }
                @Override public String getName() { return "materialDesignColor"; }
            };
        }
        return materialDesignColor;
    }

    public Color getPromptTextColor() { return null == promptTextColor ? DEFAULT_PROMPT_TEXT_COLOR : promptTextColor.get(); }
    public void setPromptTextColor(final Color COLOR) { promptTextColorProperty().set(COLOR); }
    public ObjectProperty<Color> promptTextColorProperty() {
        if (null == promptTextColor) {
            promptTextColor = new StyleableObjectProperty<Color>(DEFAULT_PROMPT_TEXT_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialDateTimePicker.StyleableProperties.PROMPT_TEXT_COLOR; }
                @Override public Object getBean() { return MaterialDateTimePicker.this; }
                @Override public String getName() { return "promptTextColor"; }
            };
        }
        return promptTextColor;
    }

    public Color getRequiredTextColor() { return null == requiredTextColor ? DEFAULT_REQUIRED_TEXT_COLOR : requiredTextColor.get(); }
    public void setRequiredTextColor(final Color COLOR) { requiredTextColorProperty().set(COLOR); }
    public ObjectProperty<Color> requiredTextColorProperty() {
        if (null == requiredTextColor) {
            requiredTextColor = new StyleableObjectProperty<Color>(DEFAULT_REQUIRED_TEXT_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialDateTimePicker.StyleableProperties.REQUIRED_TEXT_COLOR; }
                @Override public Object getBean() { return MaterialDateTimePicker.this; }
                @Override public String getName() { return "requiredTextColor"; }
            };
        }
        return requiredTextColor;
    }

    public Color getInvalidTextColor() { return null == invalidTextColor ? DEFAULT_INVALID_TEXT_COLOR : invalidTextColor.get(); }
    public void setInvalidTextColor(final Color COLOR) { invalidTextColorProperty().set(COLOR); }
    public ObjectProperty<Color> invalidTextColorProperty() {
        if (null == invalidTextColor) {
            invalidTextColor = new StyleableObjectProperty<Color>(DEFAULT_INVALID_TEXT_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialDateTimePicker.StyleableProperties.INVALID_TEXT_COLOR; }
                @Override public Object getBean() { return MaterialDateTimePicker.this; }
                @Override public String getName() { return "invalidTextColor"; }
            };
        }
        return invalidTextColor;
    }


    // ******************** Misc **********************************************
    private void handleTextAndFocus(final boolean IS_FOCUSED) {
        final boolean IS_EMPTY = isEditable() ? getEditor().getText().isEmpty() : null == getValue();

        if (IS_FOCUSED && isEditable() && !IS_EMPTY) {
            resetIcon.relocate(getWidth() - 32, 12);
            resetIcon.toFront();
            resetIcon.setVisible(true);
        } else {
            resetIcon.toBack();
            resetIcon.setVisible(false);
        }

        setHasSelection(!IS_EMPTY);

        setInvalid(isRequired() && IS_EMPTY);

        KeyFrame kf0;
        KeyFrame kf1;

        KeyValue kvHintTextOpacity0;
        KeyValue kvHintTextOpacity1;
        KeyValue kvTextY0;
        KeyValue kvTextY1;
        KeyValue kvTextFontSize0;
        KeyValue kvTextFontSize1;
        KeyValue kvPromptTextFill0;
        KeyValue kvPromptTextFill1;
        KeyValue kvRequiredTextFill0;
        KeyValue kvRequiredTextFill1;

        if (IS_FOCUSED) {
            kvHintTextOpacity0 = new KeyValue(hintText.opacityProperty(), (isHintTextAlwaysVisible() || isInvalid()) ? 1 : hintText.getOpacity());
            kvHintTextOpacity1 = new KeyValue(hintText.opacityProperty(), 1);
        } else {
            kvHintTextOpacity0 = new KeyValue(hintText.opacityProperty(), hintText.getOpacity());
            kvHintTextOpacity1 = new KeyValue(hintText.opacityProperty(), (isHintTextAlwaysVisible() || isInvalid()) ? 1 : 0);
        }

        if (IS_FOCUSED | !IS_EMPTY || isDisabled() || !isEditable()) {
            if (Double.compare(text.getTranslateY(), DEFAULT_OFFSET_Y - STD_FONT_SIZE - TOP_OFFSET_Y) == 0) {
                kf0 = new KeyFrame(Duration.ZERO, kvHintTextOpacity0);
                kf1 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kvHintTextOpacity1);
            } else {
                kvTextY0            = new KeyValue(text.translateYProperty(), DEFAULT_OFFSET_Y);
                kvTextY1            = new KeyValue(text.translateYProperty(), DEFAULT_OFFSET_Y - STD_FONT_SIZE - TOP_OFFSET_Y);
                kvTextFontSize0     = new KeyValue(fontSize, STD_FONT_SIZE);
                kvTextFontSize1     = new KeyValue(fontSize, SMALL_FONT_SIZE);
                kvPromptTextFill0   = new KeyValue(promptTextColorProperty(), DEFAULT_PROMPT_TEXT_COLOR);
                kvPromptTextFill1   = new KeyValue(promptTextColorProperty(), IS_FOCUSED ? getMaterialDesignColor() : DEFAULT_PROMPT_TEXT_COLOR);
                kvRequiredTextFill0 = new KeyValue(requiredTextColorProperty(), getPromptTextColor());
                kvRequiredTextFill1 = new KeyValue(requiredTextColorProperty(), IS_FOCUSED ? getRequiredTextColor() : DEFAULT_PROMPT_TEXT_COLOR);

                kf0 = new KeyFrame(Duration.ZERO, kvTextY0, kvTextFontSize0, kvPromptTextFill0, kvRequiredTextFill0, kvHintTextOpacity0);
                kf1 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kvTextY1, kvTextFontSize1, kvPromptTextFill1, kvRequiredTextFill1, kvHintTextOpacity1);
            }
        } else {
            if (Double.compare(text.getTranslateY(), 0) == 0) {
                kf0 = new KeyFrame(Duration.ZERO);
                kf1 = new KeyFrame(Duration.millis(ANIMATION_DURATION));
            } else {
                kvTextY0            = new KeyValue(text.translateYProperty(), text.getTranslateY());
                kvTextY1            = new KeyValue(text.translateYProperty(), DEFAULT_OFFSET_Y);
                kvTextFontSize0     = new KeyValue(fontSize, SMALL_FONT_SIZE);
                kvTextFontSize1     = new KeyValue(fontSize, STD_FONT_SIZE);
                kvPromptTextFill0   = new KeyValue(promptTextColorProperty(), getMaterialDesignColor());
                kvPromptTextFill1   = new KeyValue(promptTextColorProperty(), isInvalid() ? getInvalidTextColor() : DEFAULT_PROMPT_TEXT_COLOR);
                kvRequiredTextFill0 = new KeyValue(requiredTextColorProperty(), getRequiredTextColor());
                kvRequiredTextFill1 = new KeyValue(requiredTextColorProperty(), isInvalid() ? getInvalidTextColor() : DEFAULT_PROMPT_TEXT_COLOR);

                kf0 = new KeyFrame(Duration.ZERO, kvTextY0, kvTextFontSize0, kvPromptTextFill0, kvRequiredTextFill0, kvHintTextOpacity0);
                kf1 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kvTextY1, kvTextFontSize1, kvPromptTextFill1, kvRequiredTextFill1, kvHintTextOpacity1);
            }
        }

        timeline.getKeyFrames().setAll(kf0, kf1);
        timeline.play();
    }


    // ******************** Style related *************************************
    private static class StyleableProperties {
        private static final CssMetaData<MaterialDateTimePicker, Color> MATERIAL_DESIGN_COLOR =
            new CssMetaData<MaterialDateTimePicker, Color>("-material-design-color", ColorConverter.getInstance(), DEFAULT_MATERIAL_DESIGN_COLOR) {
                @Override public boolean isSettable(final MaterialDateTimePicker TEXT_FIELD) { return null == TEXT_FIELD.materialDesignColor || !TEXT_FIELD.materialDesignColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialDateTimePicker TEXT_FIELD) { return (StyleableProperty) TEXT_FIELD.materialDesignColorProperty(); }
                @Override public Color getInitialValue(final MaterialDateTimePicker TEXT_FIELD) { return TEXT_FIELD.getMaterialDesignColor(); }
            };

        private static final CssMetaData<MaterialDateTimePicker, Color> PROMPT_TEXT_COLOR =
            new CssMetaData<MaterialDateTimePicker, Color>("-prompt-text-color", ColorConverter.getInstance(), DEFAULT_PROMPT_TEXT_COLOR) {
                @Override public boolean isSettable(final MaterialDateTimePicker TEXT_FIELD) { return null == TEXT_FIELD.promptTextColor || !TEXT_FIELD.promptTextColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialDateTimePicker TEXT_FIELD) { return (StyleableProperty) TEXT_FIELD.promptTextColorProperty(); }
                @Override public Color getInitialValue(final MaterialDateTimePicker TEXT_FIELD) { return TEXT_FIELD.getPromptTextColor(); }
            };

        private static final CssMetaData<MaterialDateTimePicker, Color> REQUIRED_TEXT_COLOR =
            new CssMetaData<MaterialDateTimePicker, Color>("-required-text-color", ColorConverter.getInstance(), DEFAULT_REQUIRED_TEXT_COLOR) {
                @Override public boolean isSettable(final MaterialDateTimePicker TEXT_FIELD) { return null == TEXT_FIELD.requiredTextColor || !TEXT_FIELD.requiredTextColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialDateTimePicker TEXT_FIELD) { return (StyleableProperty) TEXT_FIELD.requiredTextColorProperty(); }
                @Override public Color getInitialValue(final MaterialDateTimePicker TEXT_FIELD) { return TEXT_FIELD.getRequiredTextColor(); }
            };

        private static final CssMetaData<MaterialDateTimePicker, Color> INVALID_TEXT_COLOR =
            new CssMetaData<MaterialDateTimePicker, Color>("-invalid-text-color", ColorConverter.getInstance(), DEFAULT_INVALID_TEXT_COLOR) {
                @Override public boolean isSettable(final MaterialDateTimePicker TEXT_FIELD) { return null == TEXT_FIELD.invalidTextColor || !TEXT_FIELD.invalidTextColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialDateTimePicker TEXT_FIELD) { return (StyleableProperty) TEXT_FIELD.invalidTextColorProperty(); }
                @Override public Color getInitialValue(final MaterialDateTimePicker TEXT_FIELD) { return TEXT_FIELD.getInvalidTextColor(); }
            };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
        static {
            final List<CssMetaData<? extends Styleable, ?>> styleables = new ArrayList<>(Control.getClassCssMetaData());
            Collections.addAll(styleables,
                               MATERIAL_DESIGN_COLOR,
                               PROMPT_TEXT_COLOR,
                               REQUIRED_TEXT_COLOR,
                               INVALID_TEXT_COLOR);
            STYLEABLES = Collections.unmodifiableList(styleables);
        }
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() { return MaterialDateTimePicker.StyleableProperties.STYLEABLES; }
    @Override public List<CssMetaData<? extends Styleable, ?>> getControlCssMetaData() { return getClassCssMetaData(); }


    // ******************** Internal Classes **********************************
    class InternalConverter extends StringConverter<LocalDate> {
        @Override public String toString(LocalDate object) {
            final LocalDateTime DATE_TIME = getDateTimeValue();
            return null == DATE_TIME ? "" : DATE_TIME.format(formatter);
        }

        @Override public LocalDate fromString(final String VALUE) {
            if (VALUE == null) {
                dateTime.set(null);
                return null;
            }

            dateTime.set(LocalDateTime.parse(VALUE, formatter));
            return dateTime.get().toLocalDate();
        }
    }
}
