/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.util.Duration;


/**
 * Created by hansolo on 16.12.16.
 */
public class SideBar extends VBox {
    public enum State { VISIBLE, HIDDEN }
    private Animation             showTransition;
    private Animation             hideTransition;
    private double                expandedWidth;
    private MaterialGlyphButton   controlButton;
    private ObjectProperty<State> state;


    public SideBar(final double EXPANDED_WIDTH, final Node... NODES) {
        expandedWidth  = EXPANDED_WIDTH;
        state          = new ObjectPropertyBase<State>(State.HIDDEN) {
            @Override public Object getBean() { return SideBar.this; }
            @Override public String getName() { return "state"; }
        };
        controlButton  = new MaterialGlyphButton(FontAwesomeIcons.ARROW_LEFT);

        showTransition = new Transition() {
            { setCycleDuration(Duration.millis(250)); }

            protected void interpolate(final double FRACTION) {
                final double CURRENT_WIDTH = expandedWidth * FRACTION;
                setPrefWidth(CURRENT_WIDTH);
            }
        };
        hideTransition = new Transition() {
            { setCycleDuration(Duration.millis(250)); }

            protected void interpolate(final double FRACTION) {
                final double CURRENT_WIDTH = expandedWidth * (1.0 - FRACTION);
                setPrefWidth(CURRENT_WIDTH);
            }
        };

        setMinWidth(0);
        setPrefWidth(0);
        setAlignment(Pos.CENTER);

        getChildren().setAll(NODES);

        registerListeners();
    }

    private void registerListeners() {
        controlButton.setOnAction(e -> {
            if (State.HIDDEN == getState()) {
                showTransition.play();
            } else {
                hideTransition.play();
            }
        });

        hideTransition.onFinishedProperty().set(e -> {
            controlButton.setGlyphName(FontAwesomeIcons.ARROW_LEFT.name());
            state.set(State.HIDDEN);
        });

        showTransition.onFinishedProperty().set(e -> {
            controlButton.setGlyphName(FontAwesomeIcons.ARROW_RIGHT.name());
            state.set(State.VISIBLE);
        });
    }

    public MaterialGlyphButton getControlButton() { return controlButton; }

    public State getState() { return state.get(); }
    public ReadOnlyObjectProperty<State> stateProperty() { return state; }

    public void open() { showTransition.play(); }
    public void close() { hideTransition.play(); }
}
