/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import com.sun.javafx.scene.control.skin.ButtonSkin;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import eu.hansolo.fx.material.effect.Ripple;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Skin;


/**
 * User: hansolo
 * Date: 06.05.16
 * Time: 16:37
 */
public class MaterialGlyphButton extends Button {
    private FontAwesomeIcon icon;


    // ******************** Constructors **************************************
    public MaterialGlyphButton() {
        this(FontAwesomeIcons.ANCHOR.name());
    }
    public MaterialGlyphButton(final FontAwesomeIcons ICON) {
        this(ICON.name());
    }
    public MaterialGlyphButton(final String ICON_NAME) {
        super("");
        getStylesheets().add(MaterialGlyphButton.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialGlyphButton.class.getResource("material-components.css").toExternalForm());
        getStyleClass().add("material-glyph-button");

        icon = new FontAwesomeIcon();
        icon.setGlyphName(ICON_NAME);
        icon.getStyleClass().setAll("icon");
        setGraphic(icon);
    }


    // ******************** Methods *******************************************
    public String getGlyphName() { return icon.getGlyphName(); }
    public void setGlyphName(final String NAME) { icon.setGlyphName(NAME); }
    public StringProperty glyphNameProperty() { return icon.glyphNameProperty(); }

    public FontAwesomeIcon getIcon() { return icon; }
    public void setIcon(final FontAwesomeIcon ICON) { icon.setGlyphName(ICON.getGlyphName()); }

    @Override protected Skin<?> createDefaultSkin() {
        final ButtonSkin SKIN = new ButtonSkin(this);
        getChildren().add(0, new Ripple());
        return SKIN;
    }
}
