/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import com.sun.javafx.scene.control.skin.DatePickerContent;
import com.sun.javafx.scene.control.skin.DatePickerSkin;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import eu.hansolo.fx.material.controlsfx.RangeSlider;
import javafx.application.Platform;
import javafx.beans.DefaultProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Popup;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;


/**
 * User: hansolo
 * Date: 23.11.16
 * Time: 09:14
 */
@DefaultProperty("children")
public class MaterialDateRangeSelector extends Region {
    private static final double                    PREFERRED_WIDTH  = 450;
    private static final double                    PREFERRED_HEIGHT = 30;
    private static final double                    MINIMUM_WIDTH    = 90;
    private static final double                    MINIMUM_HEIGHT   = 6;
    private static final double                    MAXIMUM_WIDTH    = 1024;
    private static final double                    MAXIMUM_HEIGHT   = 1024;
    private static final DateTimeFormatter         DF               = DateTimeFormatter.ofPattern("dd.MM.YYYY");
    private static       double                    aspectRatio      = PREFERRED_HEIGHT / PREFERRED_WIDTH;
    private              boolean                   keepAspect;
    private              double                    size;
    private              double                    width;
    private              double                    height;
    private              HBox                      pane;
    private              Paint                     backgroundPaint;
    private              Paint                     borderPaint;
    private              double                    borderWidth;
    private              MaterialGlyphButton       glyphButtonMin;
    private              MaterialGlyphButton       glyphButtonMax;
    private              MaterialDatePicker        datePickerMin;
    private              MaterialDatePicker        datePickerMax;
    private              RangeSlider               slider;
    private              Popup                     popupMinDate;
    private              Popup                     popupMaxDate;
    private              Tooltip                   minTooltip;
    private              Tooltip                   maxTooltip;
    private              Tooltip                   sliderTooltip;
    private              ObjectProperty<LocalDate> minDate;
    private              ObjectProperty<LocalDate> maxDate;
    private              ObjectProperty<LocalDate> lowDate;
    private              ObjectProperty<LocalDate> highDate;


    // ******************** Constructors **************************************
    public MaterialDateRangeSelector() {
        keepAspect      = true;
        backgroundPaint = Color.TRANSPARENT;
        borderPaint     = Color.TRANSPARENT;
        borderWidth     = 0d;
        minDate         = new ObjectPropertyBase<LocalDate>(LocalDate.now().minusDays(7)) {
            @Override protected void invalidated() {  }
            @Override public Object getBean() { return MaterialDateRangeSelector.this; }
            @Override public String getName() { return "minDate"; }
        };
        maxDate         = new ObjectPropertyBase<LocalDate>(LocalDate.now()) {
            @Override protected void invalidated() {  }
            @Override public Object getBean() { return MaterialDateRangeSelector.this; }
            @Override public String getName() { return "maxDate"; }
        };
        lowDate         = new ObjectPropertyBase<LocalDate>(LocalDate.now().minusDays(7)) {
            @Override protected void invalidated() {  }
            @Override public Object getBean() { return MaterialDateRangeSelector.this; }
            @Override public String getName() { return "lowDate"; }
        };
        highDate        = new ObjectPropertyBase<LocalDate>(LocalDate.now().minusDays(7)) {
            @Override protected void invalidated() {  }
            @Override public Object getBean() { return MaterialDateRangeSelector.this; }
            @Override public String getName() { return "highDate"; }
        };
        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        if (Double.compare(getPrefWidth(), 0.0) <= 0 || Double.compare(getPrefHeight(), 0.0) <= 0 ||
            Double.compare(getWidth(), 0.0) <= 0 || Double.compare(getHeight(), 0.0) <= 0) {
            if (getPrefWidth() > 0 && getPrefHeight() > 0) {
                setPrefSize(getPrefWidth(), getPrefHeight());
            } else {
                setPrefSize(PREFERRED_WIDTH, PREFERRED_HEIGHT);
            }
        }

        datePickerMin = new MaterialDatePicker(LocalDate.now().minusDays(7), "", false);
        datePickerMin.setVisible(false);
        datePickerMin.setManaged(false);

        datePickerMax = new MaterialDatePicker(LocalDate.now(), "", false);
        datePickerMax.setVisible(false);
        datePickerMax.setManaged(false);

        glyphButtonMin = new MaterialGlyphButton(FontAwesomeIcons.CALENDAR);
        minTooltip = new Tooltip(DF.format(datePickerMin.getValue()));
        Tooltip.install(glyphButtonMin, minTooltip);

        glyphButtonMax = new MaterialGlyphButton(FontAwesomeIcons.CALENDAR);
        maxTooltip = new Tooltip(DF.format(datePickerMax.getValue()));
        Tooltip.install(glyphButtonMax, maxTooltip);

        slider = new RangeSlider(0, 7, 0, 0);
        slider.setBlockIncrement(1);
        slider.setLowThumbVisible(true);
        slider.setLowValueFixedToMin(false);
        slider.setMajorTickUnit(1);
        slider.setMinorTickCount(0);
        slider.setSnapToTicks(true);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);

        sliderTooltip = new Tooltip(String.join(" - ", DF.format(getLowDate()), DF.format(getHighDate())));
        Tooltip.install(slider, sliderTooltip);

        popupMinDate = new Popup();
        popupMinDate.setAutoHide(true);

        popupMaxDate = new Popup();
        popupMaxDate.setAutoHide(true);

        pane = new HBox(glyphButtonMin, datePickerMin, slider, datePickerMax, glyphButtonMax);
        pane.setSpacing(5);
        pane.setBackground(new Background(new BackgroundFill(backgroundPaint, CornerRadii.EMPTY, Insets.EMPTY)));
        pane.setBorder(new Border(new BorderStroke(borderPaint, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(borderWidth))));

        getChildren().setAll(pane);
    }

    private void registerListeners() {
        widthProperty().addListener(o -> resize());
        heightProperty().addListener(o -> resize());

        glyphButtonMin.setOnAction(e -> {
            if (popupMinDate.isShowing()) {
                popupMinDate.hide();
            } else {
                popupMinDate.show(getScene().getWindow());
            }
        });

        glyphButtonMax.setOnAction( e -> {
            if (popupMaxDate.isShowing()) {
                popupMaxDate.hide();
            } else {
                popupMaxDate.show(getScene().getWindow());
            }
        });

        datePickerMin.valueProperty().addListener(o -> {
            minDate.set(datePickerMin.getValue());
            if (popupMinDate.isShowing()) { popupMinDate.hide(); }
            if (datePickerMin.getValue().isAfter(datePickerMax.getValue().minusDays(7))) {
                datePickerMin.setValue(datePickerMax.getValue().minusDays(7));
            }
            adjustSlider();
            minTooltip.setText(DF.format(datePickerMin.getValue()));
        });
        datePickerMin.skinProperty().addListener(o -> {
            DatePickerSkin    datePickerSkin    = (DatePickerSkin) datePickerMin.getSkin();
            DatePickerContent datePickerContent = (DatePickerContent) datePickerSkin.getPopupContent();
            getScene().getRoot().getStyleClass().forEach(styleClass -> { if (!styleClass.equals("root")) datePickerContent.getStyleClass().add(styleClass); });
            popupMinDate.getContent().add(datePickerContent);
        });

        datePickerMax.valueProperty().addListener(o -> {
            maxDate.set(datePickerMax.getValue());
            if (popupMaxDate.isShowing()) { popupMaxDate.hide(); }
            if (datePickerMax.getValue().isBefore(datePickerMin.getValue().plusDays(7))) {
                datePickerMin.setValue(datePickerMax.getValue().minusDays(7));
            } else if (datePickerMax.getValue().isAfter(LocalDate.now())) {
                datePickerMax.setValue(LocalDate.now());
            }
            adjustSlider();
            minTooltip.setText(DF.format(datePickerMin.getValue()));
            maxTooltip.setText(DF.format(datePickerMax.getValue()));
        });
        datePickerMax.skinProperty().addListener(o -> {
            DatePickerSkin    datePickerSkin    = (DatePickerSkin) datePickerMax.getSkin();
            DatePickerContent datePickerContent = (DatePickerContent) datePickerSkin.getPopupContent();
            getScene().getRoot().getStyleClass().forEach(styleClass -> { if (!styleClass.equals("root")) datePickerContent.getStyleClass().add(styleClass); });
            popupMaxDate.getContent().add(datePickerContent);
        });

        slider.lowValueProperty().addListener(o -> {
            lowDate.set(datePickerMin.getValue().plusDays((int) slider.getLowValue()));
            sliderTooltip.setText(String.join(" - ", DF.format(getLowDate()), DF.format(getHighDate())));
        });
        slider.highValueProperty().addListener(o -> {
            highDate.set(datePickerMin.getValue().plusDays((int) slider.getHighValue()));
            sliderTooltip.setText(String.join(" - ", DF.format(getLowDate()), DF.format(getHighDate())));
        });
    }


    // ******************** Methods *******************************************
    @Override protected double computeMinWidth(final double HEIGHT) { return MINIMUM_WIDTH; }
    @Override protected double computeMinHeight(final double WIDTH) { return MINIMUM_HEIGHT; }
    @Override protected double computePrefWidth(final double HEIGHT) { return super.computePrefWidth(HEIGHT); }
    @Override protected double computePrefHeight(final double WIDTH) { return super.computePrefHeight(WIDTH); }
    @Override protected double computeMaxWidth(final double HEIGHT) { return MAXIMUM_WIDTH; }
    @Override protected double computeMaxHeight(final double WIDTH) { return MAXIMUM_HEIGHT; }

    @Override public ObservableList<Node> getChildren() { return super.getChildren(); }

    public LocalDate getMinDate() { return minDate.get(); }
    public void setMinDate(final LocalDate DATE) { datePickerMin.setValue(DATE); }
    public ObjectProperty<LocalDate> minDateProperty() { return minDate; }

    public LocalDate getLowDate() { return lowDate.get(); }
    public void setLowDate(final LocalDate DATE) { slider.setLowValue(datePickerMin.getValue().toEpochDay() - getLowDate().getLong(ChronoField.EPOCH_DAY)); }
    public ObjectProperty<LocalDate> lowDateProperty() { return lowDate; }

    public LocalDate getHighDate() { return highDate.get(); }
    public void setHighDate(final LocalDate DATE) { slider.setHighValue(datePickerMin.getValue().toEpochDay() - getHighDate().getLong(ChronoField.EPOCH_DAY)); }
    public ObjectProperty<LocalDate> highDateProperty() { return highDate; }

    public LocalDate getMaxValue() { return maxDate.get(); }
    public void setMaxValue(final LocalDate DATE) { datePickerMax.setValue(DATE); }
    public ObjectProperty<LocalDate> maxValueProperty() { return maxDate; }

    public String getRangeString() { return String.join(" - ", DF.format(getLowDate()), DF.format(getHighDate())); }

    private void adjustSlider() {
        long range = datePickerMax.getValue().getLong(ChronoField.EPOCH_DAY) - datePickerMin.getValue().getLong(ChronoField.EPOCH_DAY);
        slider.setMax(range);
        if (range > 240) {
            slider.setShowTickLabels(false);
            slider.setMajorTickUnit(30);
        } else if (range > 120) {
            slider.setShowTickLabels(false);
            slider.setMajorTickUnit(14);
        } else if (range > 31) {
            slider.setShowTickLabels(false);
            slider.setMajorTickUnit(7);
        } else if (range > 14) {
            slider.setShowTickLabels(false);
            slider.setMajorTickUnit(2);
        } else {
            slider.setShowTickLabels(true);
            slider.setMajorTickUnit(1);
        }
        sliderTooltip.setText(String.join(" - ", DF.format(getLowDate()), DF.format(getHighDate())));
    }


    // ******************** Resizing ******************************************
    private void resize() {
        width  = getWidth() - getInsets().getLeft() - getInsets().getRight();
        height = getHeight() - getInsets().getTop() - getInsets().getBottom();
        size   = width < height ? width : height;

        if (keepAspect) {
            if (aspectRatio * width > height) {
                width = 1 / (aspectRatio / height);
            } else if (1 / (aspectRatio / height) > width) {
                height = aspectRatio * width;
            }
        }

        if (width > 0 && height > 0) {
            pane.setMaxSize(width, height);
            pane.setPrefSize(width, height);
            pane.relocate((getWidth() - width) * 0.5, (getHeight() - height) * 0.5);


            redraw();
        }
    }

    private void redraw() {
        pane.setBackground(new Background(new BackgroundFill(backgroundPaint, CornerRadii.EMPTY, Insets.EMPTY)));
        pane.setBorder(new Border(new BorderStroke(borderPaint, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(borderWidth / PREFERRED_WIDTH * size))));
    }
}
