/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;
import javafx.scene.chart.XYChart.Data;


/**
 * Created by hansolo on 14.12.16.
 */
public class MaterialStackedBarChartDataEvent<X,Y> extends Event {
    public static final EventType<MaterialStackedBarChartDataEvent> STACKED_SECTION_DATA = new EventType(ANY, "STACKED_SECTION_DATA");
    private String    seriesName;
    private Data<X,Y> data;


    // ******************** Constructors **************************************
    public MaterialStackedBarChartDataEvent(final Data<X,Y> DATA) {
        super(STACKED_SECTION_DATA);
        data = DATA;
    }
    public MaterialStackedBarChartDataEvent(final String SERIES_NAME, final Data<X,Y> DATA) {
        super(STACKED_SECTION_DATA);
        seriesName  = SERIES_NAME;
        data        = DATA;
    }
    public MaterialStackedBarChartDataEvent(final Object SRC, final EventTarget TARGET, final EventType<MaterialStackedBarChartDataEvent> TYPE, final Data<X,Y> DATA) {
        super(SRC, TARGET, TYPE);
        data = DATA;
    }


    // ******************** Methods *******************************************
    public String getSeriesName() { return seriesName; }

    public Data getData() { return data; }
}
