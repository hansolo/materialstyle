/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;
import javafx.scene.chart.PieChart.Data;


/**
 * Created by hansolo on 24.11.16.
 */
public class MaterialPieChartDataEvent extends Event {
    public static final EventType<MaterialPieChartDataEvent> PIE_CHART_DATA = new EventType(ANY, "PIE_CHART_DATA");
    private Data data;


    // ******************** Constructors **************************************
    public MaterialPieChartDataEvent(final Data DATA) {
        super(PIE_CHART_DATA);
        data = DATA;
    }
    public MaterialPieChartDataEvent(final Object SRC, final EventTarget TARGET, final EventType<MaterialPieChartDataEvent> TYPE, final Data DATA) {
        super(SRC, TARGET, TYPE);
        data = DATA;
    }


    // ******************** Methods *******************************************
    public Data getData() { return data; }
}
