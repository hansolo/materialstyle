/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.geometry.VPos;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;


/**
 * Created by hansolo on 20.05.16.
 */
public class MaterialTextArea extends TextArea {
    private String          _title;
    private StringProperty  title;
    private Text            titleText;
    private Text            requiredText;
    private HBox            text;
    private boolean         _required;
    private BooleanProperty required;


    public MaterialTextArea() {
        this("", "", false);
    }
    public MaterialTextArea(final String TEXT) {
        this("", "", false);
    }
    public MaterialTextArea(final String TEXT, final String TITLE) {
        this(TEXT, TITLE, false);
    }
    public MaterialTextArea(final String TEXT, final String TITLE, final boolean REQUIRED) {
        super(TEXT);

        getStylesheets().add(MaterialTextArea.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialTextArea.class.getResource("material-components.css").toExternalForm());
        getStyleClass().addAll("material-text-area");

        _title    = TITLE;
        _required = REQUIRED;

        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        requiredText = new Text(" *");
        requiredText.getStyleClass().add("required-text");
        requiredText.setVisible(isRequired());

        titleText = new Text(getTitle());
        titleText.setTextOrigin(VPos.TOP);
        titleText.getStyleClass().add("title-text");

        text = new HBox(titleText, requiredText);
        text.getStyleClass().add("material-text-area");

        getChildren().addAll(text);
    }

    private void registerListeners() {
        widthProperty().addListener(o -> resize());
        heightProperty().addListener(o -> resize());
    }


    // ******************** Methods *******************************************
    public String getTitle() { return null == title ? _title : title.get(); }
    public void setTitle(final String TEXT) {
        if (null == title) {
            _title = TEXT;
            titleText.setText(TEXT);
        } else {
            title.set(TEXT);
        }
    }
    public StringProperty titleProperty() {
        if (null == title) {
            title = new StringPropertyBase(_title) {
                @Override protected void invalidated() { titleText.setText(get()); }
                @Override public Object getBean() { return MaterialTextArea.this; }
                @Override public String getName() { return "title"; }
            };
        }
        return title;
    }

    public boolean isRequired() { return null == required ? _required : required.get(); }
    public void setRequired(final boolean REQUIRED) {
        if (null == required) {
            _required = REQUIRED;
            requiredText.setVisible(_required);
        } else {
            required.set(REQUIRED);
        }
    }
    public BooleanProperty requiredProperty() {
        if (null == required) {
            required = new BooleanPropertyBase(_required) {
                @Override protected void invalidated() { requiredText.setVisible(get()); }
                @Override public Object getBean() { return MaterialTextArea.this; }
                @Override public String getName() { return "required"; }
            };
        }
        return required;
    }

    private void resize() {
        text.setLayoutX(0);
        text.setLayoutY(0);
    }
}
