/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.beans.DefaultProperty;
import javafx.beans.NamedArg;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;


/**
 * Created by hansolo on 27.06.16.
 */
@DefaultProperty("tabs")
public class MaterialSpecialTabPane extends TabPane {

    MaterialSpecialTabPane() {
        this(null);
    }
    MaterialSpecialTabPane(@NamedArg("tabs") final Tab... TABS) {
        super(TABS);
        getStylesheets().add(MaterialRadioGroup.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialTableCard.class.getResource("material-components.css").toExternalForm());
        getStyleClass().addAll("material-special-tab-pane");
    }
}