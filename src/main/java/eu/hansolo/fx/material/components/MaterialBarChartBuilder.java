/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import com.sun.javafx.charts.Legend;
import javafx.collections.ObservableList;
import javafx.scene.chart.Axis;
import javafx.scene.chart.XYChart.Series;


/**
 * Created by hansolo on 09.06.16.
 */
public class MaterialBarChartBuilder<B> {
    private Axis                                   xAxis;
    private Axis                                   yAxis;
    private Legend                                 legend;
    private ObservableList<Series<String, Number>> data;

    protected MaterialBarChartBuilder() {}

    public static MaterialBarChartBuilder create() {
        return new MaterialBarChartBuilder();
    }

    public B xAxis(final Axis<String> X_AXIS) {
        xAxis = X_AXIS;
        return (B)this;
    }

    public B yAxis(final Axis<Number> Y_AXIS) {
        yAxis = Y_AXIS;
        return (B)this;
    }

    public B legend(final Legend LEGEND) {
        legend = LEGEND;
        return (B)this;
    }

    public B data(final ObservableList<Series<String, Number>> DATA) {
        data = DATA;
        return (B)this ;
    }

    public MaterialBarChart build() {
        if (null == data) {
            if (null == legend) return new MaterialBarChart(xAxis, yAxis);
            return null == legend ? new MaterialBarChart(xAxis, yAxis) : new MaterialBarChart(xAxis, yAxis, legend);
        } else {
            return new MaterialBarChart(xAxis, yAxis, data, legend);
        }
    }
}
