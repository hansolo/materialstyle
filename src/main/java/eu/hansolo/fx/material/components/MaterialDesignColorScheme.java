/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.scene.Node;


/**
 * Created by hansolo on 10.05.16.
 */
public enum MaterialDesignColorScheme {
    INSTANCE;

    public enum MaterialDesignColor {
        RED("material-red"),
        PINK("material-pink"),
        PURPLE("material-purple"),
        DEEP_PURPLE("material-deep-purple"),
        INDIGO("material-indigo"),
        BLUE("material-blue"),
        LIGHT_BLUE("material-light-blue"),
        CYAN("material-cyan"),
        TEAL("material-teal"),
        GREEN("material-green"),
        LIGHT_GREEN("material-light-green"),
        LIME("material-lime"),
        YELLOW("material-yellow"),
        AMBER("material-amber"),
        ORANGE("material-orange"),
        DEEP_ORANGE("material-deep-orange"),
        BROWN("material-brown"),
        GREY("material-grey"),
        BLUE_GREY("material-blue-grey"),
        UBS_RED("material-ubs-red"),
        UBS_GREY("material-ubs-grey");


        public final String STYLE_CLASS;

        MaterialDesignColor(final String STYLE_CLASS) {
            this.STYLE_CLASS = STYLE_CLASS;
        }
    }
    public enum MaterialDesignAccentColor {
        RED("material-accent-red"),
        PINK("material-accent-pink"),
        PURPLE("material-accent-purple"),
        DEEP_PURPLE("material-accent-deep-purple"),
        INDIGO("material-accent-indigo"),
        BLUE("material-accent-blue"),
        LIGHT_BLUE("material-accent-light-blue"),
        CYAN("material-accent-cyan"),
        TEAL("material-accent-teal"),
        GREEN("material-accent-green"),
        LIGHT_GREEN("material-accent-light-green"),
        LIME("material-accent-lime"),
        YELLOW("material-accent-yellow"),
        AMBER("material-accent-amber"),
        ORANGE("material-accent-orange"),
        DEEP_ORANGE("material-accent-deep-orange"),
        UBS_RED("material-accent-ubs-red"),
        UBS_GREY("material-accent-ubs-grey");


        public final String STYLE_CLASS;

        MaterialDesignAccentColor(final String STYLE_CLASS) {
            this.STYLE_CLASS = STYLE_CLASS;
        }
    }


    //******************** Methods ********************************************
    public static void applyTo(final Node NODE, final MaterialDesignColor COLOR, final MaterialDesignAccentColor ACCENT) {
        NODE.getStyleClass().addAll(COLOR.STYLE_CLASS, ACCENT.STYLE_CLASS);
    }

    public static void applyTo(final Node NODE, final MaterialDesignColor COLOR) {
        NODE.getStyleClass().addAll(COLOR.STYLE_CLASS);
    }
}
