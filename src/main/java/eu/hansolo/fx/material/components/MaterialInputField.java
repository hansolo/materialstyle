/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;


import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;

import java.util.regex.Pattern;


/**
 * Created by hansolo on 19.04.16.
 */
public interface MaterialInputField {
    enum Type { TEXT_FIELD, PASSWORD_FIELD, CURRENCY_FIELD }

    String getText();
    void setText(final String TEXT);
    StringProperty textProperty();

    String getPromptText();
    void setPromptText(final String TEXT);
    StringProperty promptTextProperty();

    String getHintText();
    void setHintText(final String TEXT);
    StringProperty hintTextProperty();

    boolean isRequired();
    void setRequired(final boolean REQUIRED);
    BooleanProperty requiredProperty();

    boolean isInvalid();
    void setInvalid(final boolean INVALID);
    BooleanProperty invalidProperty();

    boolean isHintTextAlwaysVisible();
    void setHintTextAlwaysVisible(final boolean VISIBLE);
    BooleanProperty hintTextAlwaysVisibleProperty();

    boolean isEditable();
    void setEditable(final boolean EDITABLE);
    BooleanProperty editableProperty();

    boolean isDisable();
    void setDisable(final boolean DISABLE);
    BooleanProperty disableProperty();

    Pattern getInputPattern();
    void setInputPattern(final Pattern PATTERN);
    ObjectProperty<Pattern> inputPatternProperty();


    // ******************** CSS Stylable Properties ***************************
    Color getMaterialDesignColor();
    void setMaterialDesignColor(final Color COLOR);
    ObjectProperty<Color> materialDesignColorProperty();

    Color getPromptTextColor();
    void setPromptTextColor(final Color COLOR);
    ObjectProperty<Color> promptTextColorProperty();

    Color getRequiredTextColor();
    void setRequiredTextColor(final Color COLOR);
    ObjectProperty<Color> requiredTextColorProperty();

    Color getInvalidTextColor();
    void setInvalidTextColor(final Color COLOR);
    ObjectProperty<Color> invalidTextColorProperty();
}
