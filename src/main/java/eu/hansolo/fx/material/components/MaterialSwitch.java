/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import com.sun.javafx.css.converters.ColorConverter;
import eu.hansolo.fx.material.gauges.tools.Helper;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.css.CssMetaData;
import javafx.css.Styleable;
import javafx.css.StyleableObjectProperty;
import javafx.css.StyleableProperty;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Control;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;


/**
 * Created by hansolo on 13.04.16.
 */
public class MaterialSwitch extends CheckBox {
    public enum State { INDETERMINATE, NOT_SELECTED, SELECTED }
    private static final Color    DEFAULT_REQUIRED_TEXT_COLOR = Color.web("#d50000");
    private static final int      SWITCH_WIDTH = 48;
    private ObjectProperty<Color> requiredTextColor;
    private ObjectProperty<State> state;
    private InvalidationListener  selectionListener;


    // ******************** Constructors **************************************
    public MaterialSwitch() {
        this("");
    }
    public MaterialSwitch(final String TEXT) {
        super(TEXT);
        //getStylesheets().add(MaterialTextField.class.getResource("../styles.css").toExternalForm());
        getStyleClass().setAll("switch");
        state = new ObjectPropertyBase<State>() {
            @Override protected void invalidated() { super.invalidated(); }
            @Override public Object getBean() { return MaterialSwitch.this; }
            @Override public String getName() { return "state"; }
        };
        selectionListener = o -> handleState();
        selectedProperty().addListener(new WeakInvalidationListener(selectionListener));

        indeterminateProperty().addListener(o -> {if (isIndeterminate()) { state.set(State.INDETERMINATE); }});

        addEventFilter(MouseEvent.MOUSE_PRESSED, evt -> {
            if (isIndeterminate()) {
                if (evt.getSceneX() > localToScene(getBoundsInLocal()).getMinX() + SWITCH_WIDTH * 0.5) {
                    setState(State.SELECTED);
                } else {
                    setState(State.NOT_SELECTED);
                }
                setSelected(evt.getSceneX() < localToScene(getBoundsInLocal()).getMinX() + SWITCH_WIDTH * 0.5);
            }
        });
    }


    // ******************** CSS Stylable Properties ***************************
    public Color getRequiredTextColor() { return null == requiredTextColor ? DEFAULT_REQUIRED_TEXT_COLOR : requiredTextColor.get(); }
    public void setRequiredTextColor(final Color COLOR) { requiredTextColorProperty().set(COLOR); }
    public ObjectProperty<Color> requiredTextColorProperty() {
        if (null == requiredTextColor) {
            requiredTextColor = new StyleableObjectProperty<Color>(DEFAULT_REQUIRED_TEXT_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialSwitch.StyleableProperties.REQUIRED_TEXT_COLOR; }
                @Override public Object getBean() { return MaterialSwitch.this; }
                @Override public String getName() { return "requiredTextColor"; }
            };
        }
        return requiredTextColor;
    }

    public State getState() { return state.get(); }
    public void setState(final State STATE) { state.set(STATE); }
    public ObjectProperty<State> stateProperty() { return state; }

    private void handleState() {
        if (isIndeterminate()) return;
        setState(isSelected() ? State.SELECTED : State.NOT_SELECTED);
    }


    // ******************** Style related *************************************
    private static class StyleableProperties {
        private static final CssMetaData<MaterialSwitch, Color> REQUIRED_TEXT_COLOR =
            new CssMetaData<MaterialSwitch, Color>("-required-text-color", ColorConverter.getInstance(), DEFAULT_REQUIRED_TEXT_COLOR) {
                @Override public boolean isSettable(final MaterialSwitch TEXT_FIELD) { return null == TEXT_FIELD.requiredTextColor || !TEXT_FIELD.requiredTextColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialSwitch TEXT_FIELD) { return (StyleableProperty) TEXT_FIELD.requiredTextColorProperty(); }
                @Override public Color getInitialValue(final MaterialSwitch TEXT_FIELD) { return TEXT_FIELD.getRequiredTextColor(); }
            };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
        static {
            final List<CssMetaData<? extends Styleable, ?>> styleables = new ArrayList<>(Control.getClassCssMetaData());
            Collections.addAll(styleables, REQUIRED_TEXT_COLOR);
            STYLEABLES = Collections.unmodifiableList(styleables);
        }
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() { return MaterialSwitch.StyleableProperties.STYLEABLES; }
    @Override public List<CssMetaData<? extends Styleable, ?>> getControlCssMetaData() { return getClassCssMetaData(); }
}
