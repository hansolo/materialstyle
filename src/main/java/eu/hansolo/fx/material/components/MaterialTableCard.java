/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.IntegerPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;


/**
 * Created by hansolo on 24.05.16.
 */
public class MaterialTableCard extends AnchorPane {
    private String                 _title;
    private StringProperty         title;
    private Label                  titleLabel;
    // Top Toolbar
    private ToolBar                topToolBar;
    private Button                 buttonTop1;
    private FontAwesomeIcon        iconButtonTop1;
    private Button                 buttonTop2;
    private FontAwesomeIcon        iconButtonTop2;
    private Button                 buttonTop3;
    private FontAwesomeIcon        iconButtonTop3;
    private ToggleButton           buttonTop4;
    private FontAwesomeIcon        iconButtonTop4;
    // Bottom Pagination Control
    private Pagination             pagination;
    // Center
    private TableView              tableView;
    private TableView              innerTableView;
    private ObservableList<Object> data;
    private IntegerProperty        rowsPerPage;



    // ******************** Constructors **************************************
    public MaterialTableCard() {
        this("", new TableView());
    }
    public MaterialTableCard(final String TITLE) {
        this(TITLE, new TableView());
    }
    public MaterialTableCard(final String TITLE, final TableView TABLE_VIEW) {
        super();

        getStylesheets().add(MaterialTableCard.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialTableCard.class.getResource("material-components.css").toExternalForm());
        getStyleClass().addAll("material-card");

        tableView      = TABLE_VIEW;
        innerTableView = TABLE_VIEW;
        data           = FXCollections.observableArrayList();
        _title         = TITLE;
        rowsPerPage    = new IntegerPropertyBase(5) {
            @Override protected void invalidated() { adjustPagination(); }
            @Override public Object getBean() { return MaterialTableCard.this; }
            @Override public String getName() { return "rowsPerPage"; }
        };

        if (null != tableView) data.setAll(tableView.getItems());

        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        // Title
        titleLabel = new Label(getTitle());
        titleLabel.getStyleClass().add("card-title");

        // Top ToolBar
        iconButtonTop1 = new FontAwesomeIcon();
        iconButtonTop1.setGlyphName(FontAwesomeIcons.BACKWARD.name());
        iconButtonTop1.getStyleClass().add("card-icon");
        buttonTop1 = new Button();
        buttonTop1.setGraphic(iconButtonTop1);
        buttonTop1.setVisible(false);

        iconButtonTop2 = new FontAwesomeIcon();
        iconButtonTop2.setGlyphName(FontAwesomeIcons.FORWARD.name());
        iconButtonTop2.getStyleClass().add("card-icon");
        buttonTop2 = new Button();
        buttonTop2.setGraphic(iconButtonTop2);
        buttonTop2.setVisible(false);

        iconButtonTop3 = new FontAwesomeIcon();
        iconButtonTop3.setGlyphName(FontAwesomeIcons.SORT.name());
        iconButtonTop3.getStyleClass().add("card-icon");
        buttonTop3 = new Button();
        buttonTop3.setGraphic(iconButtonTop3);
        buttonTop3.setVisible(false);

        iconButtonTop4 = new FontAwesomeIcon();
        iconButtonTop4.setGlyphName(FontAwesomeIcons.BARS.name());
        iconButtonTop4.getStyleClass().add("card-icon");
        buttonTop4 = new ToggleButton();
        buttonTop4.setGraphic(iconButtonTop4);

        topToolBar = new ToolBar(buttonTop1, buttonTop2, buttonTop3, buttonTop4);
        topToolBar.setPrefHeight(64);
        topToolBar.getStyleClass().add("card-tool-bar");

        // Bottom Pagination
        pagination = new Pagination();

        //doLayout();
        AnchorPane.setTopAnchor(titleLabel, 0d);
        AnchorPane.setLeftAnchor(titleLabel, 0d);

        AnchorPane.setTopAnchor(topToolBar, 0d);
        AnchorPane.setRightAnchor(topToolBar, 0d);

        AnchorPane.setTopAnchor(tableView, 64d);
        AnchorPane.setRightAnchor(tableView, 0d);
        AnchorPane.setBottomAnchor(tableView, 56d);
        AnchorPane.setLeftAnchor(tableView, 0d);

        AnchorPane.setTopAnchor(pagination, 64d);
        AnchorPane.setRightAnchor(pagination, 14d);
        AnchorPane.setBottomAnchor(pagination, 0d);
        AnchorPane.setLeftAnchor(pagination, 24d);

        getChildren().setAll(titleLabel, topToolBar, pagination, innerTableView);

        adjustPagination();
    }

    private void registerListeners() {
        if (null != tableView) tableView.getItems().addListener((ListChangeListener) c -> {
            data.setAll(tableView.getItems());
            adjustPagination();
        });

        innerTableView.getItems().addListener((ListChangeListener) c -> {
            while(c.next()) {
                if (c.wasPermutated()) {      // Get items that have been permutated in list
                    for (int i = c.getFrom(); i < c.getTo(); ++i) {
                        tableView.getItems().set(i, innerTableView.getItems().get(i));
                    }
                } else if (c.wasUpdated()) {  // Get items that have been updated in list
                    for (int i = c.getFrom(); i < c.getTo(); ++i) {
                        tableView.getItems().set(i, innerTableView.getItems().get(i));
                    }
                } else if (c.wasAdded()) {    // Get items that have been added from list
                    c.getAddedSubList().forEach(addedItem -> tableView.getItems().add(addedItem));
                } else if (c.wasRemoved()) {  // Get items that have been removed from list
                    c.getRemoved().forEach(removedItem -> tableView.getItems().remove(removedItem));
                }
            }
        });
        //pagination.maxPageIndicatorCountProperty().addListener(o -> System.out.println("Max Page Count: " + pagination.getMaxPageIndicatorCount()));
    }


    // ******************** Public Methods ************************************
    public String getTitle() { return null == title ? _title : title.get(); }
    public void setTitle(final String TITLE) {
        if (null == title) {
            _title = TITLE;
            titleLabel.setText(_title);
        } else {
            title.set(TITLE);
        }
    }
    public StringProperty titleProperty() {
        if (null == title) {
            title = new StringPropertyBase(_title) {
                @Override protected void invalidated() { titleLabel.setText(get()); }
                @Override public Object getBean() { return MaterialTableCard.this; }
                @Override public String getName() { return "title"; }
            };
            _title = null;
        }
        return title;
    };

    public void setTableView(final TableView TABLE_VIEW) {
        if (null == TABLE_VIEW) return;
        tableView      = TABLE_VIEW;
        innerTableView = TABLE_VIEW;

        AnchorPane.setTopAnchor(tableView, 64d);
        AnchorPane.setRightAnchor(tableView, 0d);
        AnchorPane.setBottomAnchor(tableView, 56d);
        AnchorPane.setLeftAnchor(tableView, 0d);

        data.setAll(tableView.getItems());
        adjustPagination();
        registerListeners();
        getChildren().setAll(titleLabel, topToolBar, pagination, innerTableView);
    }
    public TableView getTableView() { return tableView; }

    public int getRowsPerPage() { return rowsPerPage.get(); }
    public void setRowsPerPage(final int ROWS) { rowsPerPage.set(ROWS); }
    public IntegerProperty rowsPerPageProperty() { return rowsPerPage; }

    // Top Toolbar Button Visibility
    public boolean isButtonTop1Visible() { return buttonTop1.isVisible(); }
    public void setButtonTop1Visible(final boolean VISIBLE) { buttonTop1.setVisible(VISIBLE); }
    public BooleanProperty buttonTop1VisibleProperty() { return buttonTop1.visibleProperty(); }

    public boolean isButtonTop2Visible() { return buttonTop2.isVisible(); }
    public void setButtonTop2Visible(final boolean VISIBLE) { buttonTop2.setVisible(VISIBLE); }
    public BooleanProperty buttonTop2VisibleProperty() { return buttonTop2.visibleProperty(); }

    public boolean isButtonTop3Visible() { return buttonTop3.isVisible(); }
    public void setButtonTop3Visible(final boolean VISIBLE) { buttonTop3.setVisible(VISIBLE); }
    public BooleanProperty buttonTop3VisibleProperty() { return buttonTop3.visibleProperty(); }

    public boolean isButtonTop4Visible() { return buttonTop4.isVisible(); }
    public void setButtonTop4Visible(final boolean VISIBLE) { buttonTop4.setVisible(VISIBLE); }
    public BooleanProperty buttonTop4VisibleProperty() { return buttonTop4.visibleProperty(); }

    // Top Toolbar Button Enabled/Disabled
    public boolean isButton1Disable() { return buttonTop1.isDisable(); }
    public void setButtonTop1Disable(final boolean DISABLE) { buttonTop1.setDisable(DISABLE); }
    public BooleanProperty button1TopDisabledProperty() { return buttonTop1.disableProperty(); }

    public boolean isButton2Disable() { return buttonTop2.isDisable(); }
    public void setButtonTop2Disable(final boolean DISABLE) { buttonTop2.setDisable(DISABLE); }
    public BooleanProperty button2TopDisabledProperty() { return buttonTop2.disableProperty(); }

    public boolean isButton3Disable() { return buttonTop3.isDisable(); }
    public void setButtonTop3Disable(final boolean DISABLE) { buttonTop3.setDisable(DISABLE); }
    public BooleanProperty button3TopDisabledProperty() { return buttonTop3.disableProperty(); }

    public boolean isButton4Disable() { return buttonTop4.isDisable(); }
    public void setButtonTop4Disable(final boolean DISABLE) { buttonTop4.setDisable(DISABLE); }
    public BooleanProperty button4TopDisabledProperty() { return buttonTop4.disableProperty(); }

    // Top Toolbar Button Text, Tooltip and Icon
    public String getButtonTop1Text() { return buttonTop1.getText(); }
    public void setButtonTop1Text(final String TEXT) { buttonTop1.setText(TEXT); }
    public StringProperty buttonTop1TextProperty() { return buttonTop1.textProperty(); }

    public Tooltip getButtonTop1Tooltip() { return buttonTop1.getTooltip(); }
    public void setButtonTop1Tooltip(final Tooltip TOOLTIP) { buttonTop1.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonTop1TooltipProperty() { return buttonTop1.tooltipProperty(); }

    public void setIconButtonTop1(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonTop1.setGraphic(null);
        } else {
            iconButtonTop1.setGlyphName(ICON.name());
            buttonTop1.setGraphic(iconButtonTop1);
        }
    }

    public String getButtonTop2Text() { return buttonTop2.getText(); }
    public void setButtonTop2Text(final String TEXT) { buttonTop2.setText(TEXT); }
    public StringProperty buttonTop2TextProperty() { return buttonTop2.textProperty(); }

    public Tooltip getButtonTop2Tooltip() { return buttonTop2.getTooltip(); }
    public void setButtonTop2Tooltip(final Tooltip TOOLTIP) { buttonTop2.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonTop2TooltipProperty() { return buttonTop2.tooltipProperty(); }

    public void setIconButtonTop2(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonTop2.setGraphic(null);
        } else {
            iconButtonTop2.setGlyphName(ICON.name());
            buttonTop2.setGraphic(iconButtonTop2);
        }
    }

    public String getButtonTop3Text() { return buttonTop3.getText(); }
    public void setButtonTop3Text(final String TEXT) { buttonTop3.setText(TEXT); }
    public StringProperty buttonTop3TextProperty() { return buttonTop3.textProperty(); }

    public Tooltip getButtonTop3Tooltip() { return buttonTop3.getTooltip(); }
    public void setButtonTop3Tooltip(final Tooltip TOOLTIP) { buttonTop3.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonTop3TooltipProperty() { return buttonTop3.tooltipProperty(); }

    public void setIconButtonTop3(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonTop3.setGraphic(null);
        } else {
            iconButtonTop3.setGlyphName(ICON.name());
            buttonTop3.setGraphic(iconButtonTop3);
        }
    }

    public String getButtonTop4Text() { return buttonTop4.getText(); }
    public void setButtonTop4Text(final String TEXT) {
        if (TEXT.isEmpty())
            buttonTop4.setText(TEXT);
    }
    public StringProperty buttonTop4TextProperty() { return buttonTop4.textProperty(); }

    public Tooltip getButtonTop4Tooltip() { return buttonTop4.getTooltip(); }
    public void setButtonTop4Tooltip(final Tooltip TOOLTIP) { buttonTop4.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonTop4TooltipProperty() { return buttonTop4.tooltipProperty(); }

    public void setIconButtonTop4(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonTop4.setGraphic(null);
        } else {
            iconButtonTop4.setGlyphName(ICON.name());
            buttonTop4.setGraphic(iconButtonTop4);
        }
    }


    // ******************** Private Methods ***********************************
    private void adjustPagination() {
        if (null == tableView) return;
        int maxPageIndicator = (data.size() / getRowsPerPage() + 1);
        pagination.setMaxPageIndicatorCount(maxPageIndicator);
        pagination.setCurrentPageIndex(0);
        pagination.setPageFactory(pageIndex -> pageIndex >= data.size() ? null : createPage(pageIndex));
    }

    private Node createPage(final int PAGE_INDEX) {
        if (null == tableView || (PAGE_INDEX * getRowsPerPage() > data.size())) return null;
        final int FROM_INDEX = PAGE_INDEX * getRowsPerPage();
        final int TO_INDEX   = Math.min(FROM_INDEX + getRowsPerPage(), data.size());
        innerTableView.setItems(FXCollections.observableArrayList(data.subList(FROM_INDEX, TO_INDEX)));
        return new BorderPane(innerTableView);
    }


    // ******************** EventHandling Top Toolbar *************************
    public void setOnButtonTop1Action(final EventHandler<ActionEvent> HANDLER) { buttonTop1.setOnAction(HANDLER); }
    public void addOnButtonTop1Action(final EventHandler<ActionEvent> HANDLER) { buttonTop1.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonTop1Action(final EventHandler<ActionEvent> HANDLER) { buttonTop1.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonTop1ActionProperty() { return buttonTop1.onActionProperty(); }

    public void setOnButtonTop2Action(final EventHandler<ActionEvent> HANDLER) { buttonTop2.setOnAction(HANDLER); }
    public void addOnButtonTop2Action(final EventHandler<ActionEvent> HANDLER) { buttonTop2.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonTop2Action(final EventHandler<ActionEvent> HANDLER) { buttonTop2.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonTop2ActionProperty() { return buttonTop2.onActionProperty(); }

    public void setOnButtonTop3Action(final EventHandler<ActionEvent> HANDLER) { buttonTop3.setOnAction(HANDLER); }
    public void addOnButtonTop3Action(final EventHandler<ActionEvent> HANDLER) { buttonTop3.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonTop3Action(final EventHandler<ActionEvent> HANDLER) { buttonTop3.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonTop3ActionProperty() { return buttonTop3.onActionProperty(); }

    public void setOnButtonTop4Action(final EventHandler<ActionEvent> HANDLER) { buttonTop4.setOnAction(HANDLER); }
    public void addOnButtonTop4Action(final EventHandler<ActionEvent> HANDLER) { buttonTop4.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonTop4Action(final EventHandler<ActionEvent> HANDLER) { buttonTop4.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonTop4ActionProperty() { return buttonTop4.onActionProperty(); }
}
