/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import com.sun.javafx.css.converters.ColorConverter;
import eu.hansolo.fx.material.numberfield.NumberField;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.css.CssMetaData;
import javafx.css.PseudoClass;
import javafx.css.Styleable;
import javafx.css.StyleableObjectProperty;
import javafx.css.StyleableProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by hansolo on 01.06.16.
 */
public class MaterialNumberField extends NumberField implements MaterialInputField {
    public  static final Locale                  DEFAULT_LOCALE                = new Locale("de", "CH");
    private static final PseudoClass             INVALID_PSEUDO_CLASS          = PseudoClass.getPseudoClass("invalid");
    private static final boolean                 VALID                         = false;
    private static final Color                   DEFAULT_MATERIAL_DESIGN_COLOR = Color.web("#3f51b5");
    private static final Color                   DEFAULT_PROMPT_TEXT_COLOR     = Color.web("#757575");
    private static final Color                   DEFAULT_REQUIRED_TEXT_COLOR   = Color.web("#d50000");
    private static final Color                   DEFAULT_INVALID_TEXT_COLOR    = Color.web("#d50000");
    private static final double                  STD_FONT_SIZE                 = 13;
    private static final double                  SMALL_FONT_SIZE               = 10;
    private static final double                  TOP_OFFSET_Y                  = 5;
    private static final double                  BOTTOM_OFFSET_Y               = 8;
    private static final int                     ANIMATION_DURATION            = 60;
    private              Text                    promptText;
    private              Text                    requiredText;
    private              HBox                    text;
    private              Label                   hintText;
    private              boolean                 _required;
    private              BooleanProperty         required;
    private              boolean                 _hintTextAlwaysVisible;
    private              BooleanProperty         hintTextAlwaysVisible;
    private              BooleanProperty         invalid;
    private              DoubleProperty          fontSize;
    private              String                  _hint;
    private              StringProperty          hint;
    private              ObjectProperty<Pattern> inputPattern;
    private              Matcher                 matcher;
    private              ObjectProperty<Color>   materialDesignColor;
    private              ObjectProperty<Color>   promptTextColor;
    private              ObjectProperty<Color>   requiredTextColor;
    private              ObjectProperty<Color>   invalidTextColor;
    private              Timeline                timeline;


    // ******************** Constructors **************************************
    public MaterialNumberField() {
        this(BigDecimal.ZERO, "", "", DEFAULT_LOCALE, false, 2);
    }
    public MaterialNumberField(final Locale LOCALE) {
        this(BigDecimal.ZERO, "", "", LOCALE, false, 2);
    }
    public MaterialNumberField(final BigDecimal VALUE, final String PROMPT_TEXT, final String HINT_TEXT, final Locale LOCALE, final boolean REQUIRED) {
        this(VALUE, PROMPT_TEXT, HINT_TEXT, LOCALE, REQUIRED, 2);
    }
    public MaterialNumberField(final BigDecimal VALUE, final String PROMPT_TEXT, final String HINT_TEXT, final Locale LOCALE, final boolean REQUIRED, final int DECIMALS) {
        super(VALUE, PROMPT_TEXT, HINT_TEXT, LOCALE, DECIMALS);
        getStylesheets().add(MaterialNumberField.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialNumberField.class.getResource("material-components.css").toExternalForm());
        getStyleClass().addAll("material-field");

        _required              = REQUIRED;
        _hintTextAlwaysVisible = false;
        _hint                  = HINT_TEXT;
        inputPattern           = new ObjectPropertyBase<Pattern>(Pattern.compile(".*")) {
            @Override protected void invalidated() { matcher.usePattern(get()); }
            @Override public Object getBean() { return MaterialNumberField.this; }
            @Override public String getName() { return "inputPattern"; }
        };
        matcher                = getInputPattern().matcher("");
        fontSize               = new SimpleDoubleProperty(MaterialNumberField.this, "fontSize", getFont().getSize());
        timeline               = new Timeline();

        setRight(null);
        setAlignment(Pos.CENTER_RIGHT);

        StringBuilder defaultNumber = new StringBuilder("0.");
        for (int i = 0 ; i < getDecimals() ; i++) { defaultNumber.append("0"); }

        setText(VALUE.toString().toString().trim().matches(getNumericPattern().pattern()) ? getNumberFormat().format(VALUE) : defaultNumber.toString());
        setPromptText(PROMPT_TEXT);
        setDecimals(DECIMALS);

        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        final String FONT_FAMILY = getFont().getFamily();
        final int    LENGTH      = getText().length();

        promptText = new Text(getPromptText());
        promptText.getStyleClass().add("prompt-text");
        promptText.setTranslateY(7);

        requiredText = new Text(" *");
        requiredText.getStyleClass().add("required-text");
        requiredText.setVisible(isRequired());
        requiredText.setTranslateY(7);

        text = new HBox(promptText, requiredText);
        text.getStyleClass().add("material-field");
        if(getLeft() instanceof Text) {
            boolean isEmpty = ((Text) getLeft()).getText().isEmpty();
            if (isEmpty) text.setTranslateX(-6);
        };

        hintText = new Label(getHintText());
        hintText.getStyleClass().add("hint-text");
        hintText.setFont(Font.font(FONT_FAMILY, SMALL_FONT_SIZE));
        hintText.setTranslateY(STD_FONT_SIZE + BOTTOM_OFFSET_Y);
        hintText.setOpacity(0);

        if (LENGTH == 0) {
            promptText.setFont(Font.font(FONT_FAMILY, STD_FONT_SIZE));
            requiredText.setFont(Font.font(FONT_FAMILY, STD_FONT_SIZE));
        } else {
            promptText.setFont(Font.font(FONT_FAMILY, SMALL_FONT_SIZE));
            requiredText.setFont(Font.font(FONT_FAMILY, SMALL_FONT_SIZE));
            text.setTranslateY(-STD_FONT_SIZE - TOP_OFFSET_Y);
        }

        getChildren().addAll(hintText, text);
    }

    private void registerListeners() {
        textProperty().addListener(o -> handleTextAndFocus(isFocused()));
        promptTextProperty().addListener(o -> promptText.setText(getPromptText()));
        focusedProperty().addListener(o -> handleTextAndFocus(isFocused()));
        promptTextColorProperty().addListener(o -> promptText.setFill(getPromptTextColor()));
        requiredTextColorProperty().addListener(o -> requiredText.setFill(getRequiredTextColor()));
        percentageModeProperty().addListener(o -> text.setTranslateX(isPercentageMode() ? 0 : -6));
        fontSize.addListener(ov -> {
            double size = fontSize.get();
            promptText.setFont(Font.font(size));
            requiredText.setFont(Font.font(size));
        });
        timeline.setOnFinished(evt -> {
            final int LENGTH = null == getText() ? 0 : getText().length();
            if (LENGTH > 0 && text.getTranslateY() >= 0) {
                text.setTranslateY(-STD_FONT_SIZE - TOP_OFFSET_Y);
                fontSize.set(SMALL_FONT_SIZE);
            }
        });
    }


    // ******************** Methods *******************************************
    @Override protected void layoutChildren() {
        super.layoutChildren();
        hintText.setTranslateX(isInvalid() ? hintText.getLayoutX() : -hintText.getLayoutX());
    }

    public boolean isRequired() { return null == required ? _required : required.get(); }
    public void setRequired(final boolean REQUIRED) {
        if (null == required) {
            _required = REQUIRED;
            requiredText.setVisible(_required);
        } else {
            required.set(REQUIRED);
        }
    }
    public BooleanProperty requiredProperty() {
        if (null == required) {
            required = new BooleanPropertyBase(_required) {
                @Override protected void invalidated() { requiredText.setVisible(get()); }
                @Override public Object getBean() { return MaterialNumberField.this; }
                @Override public String getName() { return "required"; }
            };
        }
        return required;
    }

    public String getHintText() { return null == hint ? _hint : hint.get(); }
    public void setHintText(final String TEXT) {
        if (null == hint) {
            _hint = TEXT;
            hintText.setText(_hint);
        } else {
            hint.set(TEXT);
        }
    }
    public StringProperty hintTextProperty() {
        if (null == hint) {
            hint = new StringPropertyBase(_hint) {
                @Override protected void invalidated() { hintText.setText(get()); }
                @Override public Object getBean() { return MaterialNumberField.this; }
                @Override public String getName() { return "hint"; }
            };
        }
        return hint;
    }

    public boolean isInvalid() { return null == invalid ? VALID : invalid.get(); }
    public void setInvalid(final boolean INVALID) { invalidProperty().set(INVALID); }
    public BooleanProperty invalidProperty() {
        if (null == invalid) {
            invalid = new BooleanPropertyBase(false) {
                @Override protected void invalidated() { pseudoClassStateChanged(INVALID_PSEUDO_CLASS, get()); }
                @Override public Object getBean() { return MaterialNumberField.this; }
                @Override public String getName() { return "invalid"; }
            };
        }
        return invalid;
    }

    public boolean isHintTextAlwaysVisible() { return null == hintTextAlwaysVisible ? _hintTextAlwaysVisible : hintTextAlwaysVisible.get(); }
    public void setHintTextAlwaysVisible(final boolean VISIBLE) {
        if (null == hintTextAlwaysVisible) {
            _hintTextAlwaysVisible = VISIBLE;
            if (VISIBLE) hintText.setOpacity(1);
        } else {
            hintTextAlwaysVisible.set(VISIBLE);
        }
    }
    public BooleanProperty hintTextAlwaysVisibleProperty() {
        if (null == hintTextAlwaysVisible) {
            hintTextAlwaysVisible = new BooleanPropertyBase(_hintTextAlwaysVisible) {
                @Override protected void invalidated() { if (get()) hintText.setOpacity(1); }
                @Override public Object getBean() { return MaterialNumberField.this; }
                @Override public String getName() { return "hintTextAlwaysVisible"; }
            };
        }
        return hintTextAlwaysVisible;
    }

    public Pattern getInputPattern() { return inputPattern.get(); }
    public void setInputPattern(final Pattern PATTERN) { inputPattern.set(PATTERN); }
    public ObjectProperty<Pattern> inputPatternProperty() { return inputPattern; }


    // ******************** CSS Stylable Properties ***************************
    public Color getMaterialDesignColor() { return null == materialDesignColor ? DEFAULT_MATERIAL_DESIGN_COLOR : materialDesignColor.get(); }
    public void setMaterialDesignColor(final Color COLOR) { materialDesignColorProperty().set(COLOR); }
    public ObjectProperty<Color> materialDesignColorProperty() {
        if (null == materialDesignColor) {
            materialDesignColor = new StyleableObjectProperty<Color>(DEFAULT_MATERIAL_DESIGN_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialNumberField.StyleableProperties.MATERIAL_DESIGN_COLOR; }
                @Override public Object getBean() { return MaterialNumberField.this; }
                @Override public String getName() { return "materialDesignColor"; }
            };
        }
        return materialDesignColor;
    }

    public Color getPromptTextColor() { return null == promptTextColor ? DEFAULT_PROMPT_TEXT_COLOR : promptTextColor.get(); }
    public void setPromptTextColor(final Color COLOR) { promptTextColorProperty().set(COLOR); }
    public ObjectProperty<Color> promptTextColorProperty() {
        if (null == promptTextColor) {
            promptTextColor = new StyleableObjectProperty<Color>(DEFAULT_PROMPT_TEXT_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialNumberField.StyleableProperties.PROMPT_TEXT_COLOR; }
                @Override public Object getBean() { return MaterialNumberField.this; }
                @Override public String getName() { return "promptTextColor"; }
            };
        }
        return promptTextColor;
    }

    public Color getRequiredTextColor() { return null == requiredTextColor ? DEFAULT_REQUIRED_TEXT_COLOR : requiredTextColor.get(); }
    public void setRequiredTextColor(final Color COLOR) { requiredTextColorProperty().set(COLOR); }
    public ObjectProperty<Color> requiredTextColorProperty() {
        if (null == requiredTextColor) {
            requiredTextColor = new StyleableObjectProperty<Color>(DEFAULT_REQUIRED_TEXT_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialNumberField.StyleableProperties.REQUIRED_TEXT_COLOR; }
                @Override public Object getBean() { return MaterialNumberField.this; }
                @Override public String getName() { return "requiredTextColor"; }
            };
        }
        return requiredTextColor;
    }

    public Color getInvalidTextColor() { return null == invalidTextColor ? DEFAULT_INVALID_TEXT_COLOR : invalidTextColor.get(); }
    public void setInvalidTextColor(final Color COLOR) { invalidTextColorProperty().set(COLOR); }
    public ObjectProperty<Color> invalidTextColorProperty() {
        if (null == invalidTextColor) {
            invalidTextColor = new StyleableObjectProperty<Color>(DEFAULT_INVALID_TEXT_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialNumberField.StyleableProperties.INVALID_TEXT_COLOR; }
                @Override public Object getBean() { return MaterialNumberField.this; }
                @Override public String getName() { return "invalidTextColor"; }
            };
        }
        return invalidTextColor;
    }


    // ******************** Misc **********************************************
    private void handleTextAndFocus(final boolean IS_FOCUSED) {
        final int LENGTH = null == getText() ? 0 : getText().length();

        matcher.reset(null == getText() ? "" : getText());
        setInvalid((isRequired() && LENGTH == 0) || !matcher.matches() || isInvalidPercentage());

        KeyFrame kf0;
        KeyFrame kf1;

        KeyValue kvHintTextOpacity0;
        KeyValue kvHintTextOpacity1;
        KeyValue kvTextY0;
        KeyValue kvTextY1;
        KeyValue kvTextFontSize0;
        KeyValue kvTextFontSize1;
        KeyValue kvPromptTextFill0;
        KeyValue kvPromptTextFill1;
        KeyValue kvRequiredTextFill0;
        KeyValue kvRequiredTextFill1;

        if (IS_FOCUSED) {
            kvHintTextOpacity0 = new KeyValue(hintText.opacityProperty(), (isHintTextAlwaysVisible() || isInvalid()) ? 1 : hintText.getOpacity());
            kvHintTextOpacity1 = new KeyValue(hintText.opacityProperty(), 1);
        } else {
            kvHintTextOpacity0 = new KeyValue(hintText.opacityProperty(), hintText.getOpacity());
            kvHintTextOpacity1 = new KeyValue(hintText.opacityProperty(), (isHintTextAlwaysVisible() || isInvalid()) ? 1 : 0);
        }

        if (IS_FOCUSED | LENGTH > 0) {
            if (Double.compare(text.getTranslateY(), -STD_FONT_SIZE - TOP_OFFSET_Y) == 0) {
                kf0 = new KeyFrame(Duration.ZERO, kvHintTextOpacity0);
                kf1 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kvHintTextOpacity1);
            } else {
                kvTextY0            = new KeyValue(text.translateYProperty(), 0);
                kvTextY1            = new KeyValue(text.translateYProperty(), -STD_FONT_SIZE - TOP_OFFSET_Y);
                kvTextFontSize0     = new KeyValue(fontSize, STD_FONT_SIZE);
                kvTextFontSize1     = new KeyValue(fontSize, SMALL_FONT_SIZE);
                kvPromptTextFill0   = new KeyValue(promptTextColorProperty(), DEFAULT_PROMPT_TEXT_COLOR);
                kvPromptTextFill1   = new KeyValue(promptTextColorProperty(), IS_FOCUSED ? getMaterialDesignColor() : DEFAULT_PROMPT_TEXT_COLOR);
                kvRequiredTextFill0 = new KeyValue(requiredTextColorProperty(), getPromptTextColor());
                kvRequiredTextFill1 = new KeyValue(requiredTextColorProperty(), IS_FOCUSED ? getRequiredTextColor() : DEFAULT_PROMPT_TEXT_COLOR);

                kf0 = new KeyFrame(Duration.ZERO, kvTextY0, kvTextFontSize0, kvPromptTextFill0, kvRequiredTextFill0, kvHintTextOpacity0);
                kf1 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kvTextY1, kvTextFontSize1, kvPromptTextFill1, kvRequiredTextFill1, kvHintTextOpacity1);
            }
        } else {
            if (Double.compare(text.getTranslateY(), 7) == 0) {
                kf0 = new KeyFrame(Duration.ZERO, kvHintTextOpacity0);
                kf1 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kvHintTextOpacity1);
            } else {
                kvTextY0            = new KeyValue(text.translateYProperty(), text.getTranslateY());
                kvTextY1            = new KeyValue(text.translateYProperty(), 0);
                kvTextFontSize0     = new KeyValue(fontSize, SMALL_FONT_SIZE);
                kvTextFontSize1     = new KeyValue(fontSize, STD_FONT_SIZE);
                kvPromptTextFill0   = new KeyValue(promptTextColorProperty(), getMaterialDesignColor());
                kvPromptTextFill1   = new KeyValue(promptTextColorProperty(), isInvalid() ? getInvalidTextColor() : DEFAULT_PROMPT_TEXT_COLOR);
                kvRequiredTextFill0 = new KeyValue(requiredTextColorProperty(), getRequiredTextColor());
                kvRequiredTextFill1 = new KeyValue(requiredTextColorProperty(), isInvalid() ? getInvalidTextColor() : DEFAULT_PROMPT_TEXT_COLOR);

                kf0 = new KeyFrame(Duration.ZERO, kvTextY0, kvTextFontSize0, kvPromptTextFill0, kvRequiredTextFill0, kvHintTextOpacity0);
                kf1 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kvTextY1, kvTextFontSize1, kvPromptTextFill1, kvRequiredTextFill1, kvHintTextOpacity1);
            }
        }

        timeline.getKeyFrames().setAll(kf0, kf1);
        timeline.play();
    }


    // ******************** Style related *************************************
    private static class StyleableProperties {
        private static final CssMetaData<MaterialNumberField, Color> MATERIAL_DESIGN_COLOR =
            new CssMetaData<MaterialNumberField, Color>("-material-design-color", ColorConverter.getInstance(), DEFAULT_MATERIAL_DESIGN_COLOR) {
                @Override public boolean isSettable(final MaterialNumberField TEXT_FIELD) { return null == TEXT_FIELD.materialDesignColor || !TEXT_FIELD.materialDesignColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialNumberField TEXT_FIELD) { return (StyleableProperty) TEXT_FIELD.materialDesignColorProperty(); }
                @Override public Color getInitialValue(final MaterialNumberField TEXT_FIELD) { return TEXT_FIELD.getMaterialDesignColor(); }
            };

        private static final CssMetaData<MaterialNumberField, Color> PROMPT_TEXT_COLOR =
            new CssMetaData<MaterialNumberField, Color>("-prompt-text-color", ColorConverter.getInstance(), DEFAULT_PROMPT_TEXT_COLOR) {
                @Override public boolean isSettable(final MaterialNumberField TEXT_FIELD) { return null == TEXT_FIELD.promptTextColor || !TEXT_FIELD.promptTextColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialNumberField TEXT_FIELD) { return (StyleableProperty) TEXT_FIELD.promptTextColorProperty(); }
                @Override public Color getInitialValue(final MaterialNumberField TEXT_FIELD) { return TEXT_FIELD.getPromptTextColor(); }
            };

        private static final CssMetaData<MaterialNumberField, Color> REQUIRED_TEXT_COLOR =
            new CssMetaData<MaterialNumberField, Color>("-required-text-color", ColorConverter.getInstance(), DEFAULT_REQUIRED_TEXT_COLOR) {
                @Override public boolean isSettable(final MaterialNumberField TEXT_FIELD) { return null == TEXT_FIELD.requiredTextColor || !TEXT_FIELD.requiredTextColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialNumberField TEXT_FIELD) { return (StyleableProperty) TEXT_FIELD.requiredTextColorProperty(); }
                @Override public Color getInitialValue(final MaterialNumberField TEXT_FIELD) { return TEXT_FIELD.getRequiredTextColor(); }
            };

        private static final CssMetaData<MaterialNumberField, Color> INVALID_TEXT_COLOR =
            new CssMetaData<MaterialNumberField, Color>("-invalid-text-color", ColorConverter.getInstance(), DEFAULT_INVALID_TEXT_COLOR) {
                @Override public boolean isSettable(final MaterialNumberField TEXT_FIELD) { return null == TEXT_FIELD.invalidTextColor || !TEXT_FIELD.invalidTextColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialNumberField TEXT_FIELD) { return (StyleableProperty) TEXT_FIELD.invalidTextColorProperty(); }
                @Override public Color getInitialValue(final MaterialNumberField TEXT_FIELD) { return TEXT_FIELD.getInvalidTextColor(); }
            };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
        static {
            final List<CssMetaData<? extends Styleable, ?>> styleables = new ArrayList<>(Control.getClassCssMetaData());
            Collections.addAll(styleables,
                               MATERIAL_DESIGN_COLOR,
                               PROMPT_TEXT_COLOR,
                               REQUIRED_TEXT_COLOR,
                               INVALID_TEXT_COLOR);
            STYLEABLES = Collections.unmodifiableList(styleables);
        }
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() { return MaterialNumberField.StyleableProperties.STYLEABLES; }
    @Override public List<CssMetaData<? extends Styleable, ?>> getControlCssMetaData() { return getClassCssMetaData(); }
}
