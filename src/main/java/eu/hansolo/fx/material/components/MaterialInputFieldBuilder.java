/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import eu.hansolo.fx.material.components.MaterialInputField.Type;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Pattern;

import static eu.hansolo.fx.material.components.MaterialInputField.Type.CURRENCY_FIELD;
import static eu.hansolo.fx.material.components.MaterialInputField.Type.TEXT_FIELD;


/**
 * Created by hansolo on 15.04.16.
 */
public class MaterialInputFieldBuilder<B extends MaterialInputFieldBuilder<B>> {
    private HashMap<String, Property> properties = new HashMap<>();


    // ******************** Constructors **************************************
    protected MaterialInputFieldBuilder() {}


    // ******************** Methods *******************************************
    public static final MaterialInputFieldBuilder create() {
        return new MaterialInputFieldBuilder();
    }

    public MaterialInputFieldBuilder type(final MaterialInputField.Type TYPE) {
        properties.put("type", new SimpleObjectProperty<>(TYPE));
        return this;
    }

    public MaterialInputFieldBuilder text(final String TEXT) {
        properties.put("text", new SimpleStringProperty(TEXT));
        return this;
    }

    public MaterialInputFieldBuilder promptText(final String TEXT) {
        properties.put("promptText", new SimpleStringProperty(TEXT));
        return this;
    }

    public MaterialInputFieldBuilder hintText(final String TEXT) {
        properties.put("hintText", new SimpleStringProperty(TEXT));
        return this;
    }

    public MaterialInputFieldBuilder hintTextAlwaysVisible(final boolean VISIBLE) {
        properties.put("hintTextAlwaysVisible", new SimpleBooleanProperty(VISIBLE));
        return this;
    }

    public MaterialInputFieldBuilder locale(final Locale LOCALE) {
        properties.put("locale", new SimpleObjectProperty<>(LOCALE));
        return this;
    }

    public MaterialInputFieldBuilder required(final boolean REQUIRED) {
        properties.put("required", new SimpleBooleanProperty(REQUIRED));
        return this;
    }

    public MaterialInputFieldBuilder readOnly(final boolean READ_ONLY) {
        properties.put("readOnly", new SimpleBooleanProperty(READ_ONLY));
        return this;
    }

    public MaterialInputFieldBuilder disabled(final boolean DISABLED) {
        properties.put("disabled", new SimpleBooleanProperty(DISABLED));
        return this;
    }

    public MaterialInputFieldBuilder decimals(final int DECIMALS) {
        properties.put("decimals", new SimpleIntegerProperty(DECIMALS));
        return (B)this;
    }

    public MaterialInputFieldBuilder inputPattern(final Pattern PATTERN) {
        properties.put("inputPattern", new SimpleObjectProperty<>(PATTERN));
        return (B)this;
    }

    public final MaterialInputField build() {
        if (!properties.containsKey("type")) properties.put("type", new SimpleObjectProperty<>(TEXT_FIELD));
        if (!properties.containsKey("locale")) properties.put("locale", new SimpleObjectProperty<>(new Locale("de","CH")));

        final MaterialInputField CONTROL;
        final Locale             LOCALE = ((ObjectProperty<Locale>) properties.get("locale")).get();
        final Type               TYPE   = ((ObjectProperty<Type>) properties.get("type")).get();

        switch (TYPE) {
            case CURRENCY_FIELD: CONTROL = new MaterialCurrencyField(LOCALE); break;
            case PASSWORD_FIELD: CONTROL = new MaterialPasswordField(); break;
            case TEXT_FIELD    :
            default            : CONTROL = new MaterialTextField(); break;
        }

        for (String key : properties.keySet()) {
            if ("text".equals(key)) {
                CONTROL.setText(((StringProperty) properties.get(key)).get());
            } else if("promptText".equals(key)) {
                CONTROL.setPromptText(((StringProperty) properties.get(key)).get());
            } else if("hintText".equals(key)) {
                CONTROL.setHintText(((StringProperty) properties.get(key)).get());
            } else if("hintTextAlwaysVisible".equals(key)) {
                CONTROL.setHintTextAlwaysVisible(((BooleanProperty) properties.get(key)).get());
            } else if("required".equals(key)) {
                CONTROL.setRequired(((BooleanProperty) properties.get(key)).get());
            } else if("readOnly".equals(key)) {
                CONTROL.setEditable(!((BooleanProperty) properties.get(key)).get());
            } else if("disabled".equals(key)) {
                CONTROL.setDisable(((BooleanProperty) properties.get(key)).get());
            } else if ("inputPattern".equals(key)) {
                CONTROL.setInputPattern(((ObjectProperty<Pattern>) properties.get(key)).get());
            } else if ("decimals".equals(key) && TYPE == CURRENCY_FIELD) {
                ((MaterialCurrencyField) CONTROL).setDecimals(((IntegerProperty) properties.get(key)).get());
            }
        }
        return CONTROL;
    }
}
