/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import com.sun.javafx.css.converters.ColorConverter;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.css.CssMetaData;
import javafx.css.Styleable;
import javafx.css.StyleableObjectProperty;
import javafx.css.StyleableProperty;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by hansolo on 19.04.16.
 */
public class MaterialLabel extends Label {
    private static final Color    DEFAULT_MATERIAL_DESIGN_COLOR = Color.web("#3f51b5");
    private static final Color    DEFAULT_TITLE_TEXT_COLOR      = Color.web("#757575");
    private static final double   STD_FONT_SIZE                 = 13;
    private static final double   SMALL_FONT_SIZE               = 10;
    private static final double   TOP_OFFSET_Y                  = 8;
    private Text                  titleText;
    private String                _title;
    private StringProperty        title;
    private DoubleProperty        fontSize;
    private ObjectProperty<Color> materialDesignColor;
    private ObjectProperty<Color> titleTextColor;


    // ******************** Constructors **************************************
    public MaterialLabel() {
        this("", "");
    }
    public MaterialLabel(final String TEXT) {
        this(TEXT, "");
    }
    public MaterialLabel(final String TEXT, final String TITLE) {
        super(TEXT);
        
        getStylesheets().add(MaterialLabel.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialLabel.class.getResource("material-components.css").toExternalForm());
        getStyleClass().addAll("material-label");

        _title   = TITLE;
        fontSize = new SimpleDoubleProperty(MaterialLabel.this, "fontSize", getFont().getSize());

        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        final String FONT_FAMILY = getFont().getFamily();

        titleText = new Text(getTitle());
        titleText.setFill(DEFAULT_TITLE_TEXT_COLOR);
        titleText.setFont(Font.font(FONT_FAMILY, SMALL_FONT_SIZE));
        titleText.setTranslateY(-STD_FONT_SIZE - TOP_OFFSET_Y);
        titleText.getStyleClass().add("title-text");

        setGraphic(titleText);
        setGraphicTextGap(-titleText.getLayoutBounds().getWidth());

        setMinHeight(30);
        setPrefHeight(30);
        setMaxHeight(30);
    }

    private void registerListeners() {
        fontSize.addListener(ov -> titleText.setFont(Font.font(fontSize.get())) );
    }


    // ******************** Methods *******************************************
    @Override protected void layoutChildren() {
        super.layoutChildren();
        titleText.setTranslateX(-titleText.getLayoutX());
        setGraphicTextGap(-titleText.getLayoutBounds().getWidth());
    }

    public String getTitle() { return null == title ? _title : title.get(); }
    public void setTitle(final String TEXT) {
        if (null == title) {
            _title = TEXT;
            titleText.setText(TEXT);
            setGraphicTextGap(-titleText.getLayoutBounds().getWidth());
        } else {
            title.set(TEXT);
        }
    }
    public StringProperty titleProperty() {
        if (null == title) {
            title = new StringPropertyBase(_title) {
                @Override protected void invalidated() {
                    titleText.setText(get());
                    setGraphicTextGap(-titleText.getLayoutBounds().getWidth());
                }
                @Override public Object getBean() { return MaterialLabel.this; }
                @Override public String getName() { return "title"; }
            };
        }
        return title;
    }


    // ******************** CSS Stylable Properties ***************************
    public Color getMaterialDesignColor() { return null == materialDesignColor ? DEFAULT_MATERIAL_DESIGN_COLOR : materialDesignColor.get(); }
    public void setMaterialDesignColor(final Color COLOR) { materialDesignColorProperty().set(COLOR); }
    public ObjectProperty<Color> materialDesignColorProperty() {
        if (null == materialDesignColor) {
            materialDesignColor = new StyleableObjectProperty<Color>(DEFAULT_MATERIAL_DESIGN_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialLabel.StyleableProperties.MATERIAL_DESIGN_COLOR; }
                @Override public Object getBean() { return MaterialLabel.this; }
                @Override public String getName() { return "materialDesignColor"; }
            };
        }
        return materialDesignColor;
    }

    public Color getTitleTextColor() { return null == titleTextColor ? DEFAULT_TITLE_TEXT_COLOR : titleTextColor.get(); }
    public void setTitleTextColor(final Color COLOR) { titleTextColorProperty().set(COLOR); }
    public ObjectProperty<Color> titleTextColorProperty() {
        if (null == titleTextColor) {
            titleTextColor = new StyleableObjectProperty<Color>(DEFAULT_TITLE_TEXT_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialLabel.StyleableProperties.TITLE_TEXT_COLOR; }
                @Override protected void invalidated() { titleText.setFill(get()); }
                @Override public Object getBean() { return MaterialLabel.this; }
                @Override public String getName() { return "titleTextColor"; }
            };
        }
        return titleTextColor;
    }


    // ******************** Style related *************************************
    private static class StyleableProperties {
        private static final CssMetaData<MaterialLabel, Color> MATERIAL_DESIGN_COLOR =
            new CssMetaData<MaterialLabel, Color>("-material-design-color", ColorConverter.getInstance(), DEFAULT_MATERIAL_DESIGN_COLOR) {
                @Override public boolean isSettable(final MaterialLabel LABEL) { return null == LABEL.materialDesignColor || !LABEL.materialDesignColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialLabel LABEL) { return (StyleableProperty) LABEL.materialDesignColorProperty(); }
                @Override public Color getInitialValue(final MaterialLabel LABEL) { return LABEL.getMaterialDesignColor(); }
            };

        private static final CssMetaData<MaterialLabel, Color> TITLE_TEXT_COLOR =
            new CssMetaData<MaterialLabel, Color>("-title-text-color", ColorConverter.getInstance(), DEFAULT_TITLE_TEXT_COLOR) {
                @Override public boolean isSettable(final MaterialLabel LABEL) { return null == LABEL.titleTextColor || !LABEL.titleTextColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialLabel LABEL) { return (StyleableProperty) LABEL.titleTextColorProperty(); }
                @Override public Color getInitialValue(final MaterialLabel LABEL) { return LABEL.getTitleTextColor(); }
            };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
        static {
            final List<CssMetaData<? extends Styleable, ?>> styleables = new ArrayList<>(Control.getClassCssMetaData());
            Collections.addAll(styleables,
                               MATERIAL_DESIGN_COLOR,
                               TITLE_TEXT_COLOR
                              );
            STYLEABLES = Collections.unmodifiableList(styleables);
        }
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() { return MaterialLabel.StyleableProperties.STYLEABLES; }
    @Override public List<CssMetaData<? extends Styleable, ?>> getControlCssMetaData() { return getClassCssMetaData(); }
}
