/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import eu.hansolo.fx.material.components.MaterialButton.MaterialButtonType;
import eu.hansolo.fx.material.components.MaterialDesignColorScheme.MaterialDesignAccentColor;
import eu.hansolo.fx.material.components.MaterialDesignColorScheme.MaterialDesignColor;
import eu.hansolo.fx.material.components.MaterialInputField.Type;
import eu.hansolo.fx.material.currencyfield.CurrencyField;
import eu.hansolo.fx.material.effect.Ripple;
import eu.hansolo.fx.material.numberfield.NumberField;
import javafx.application.Application;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.Scene;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;


/**
 * User: hansolo
 * Date: 14.04.16
 * Time: 09:26
 */
public class DemoMaterial extends Application {
    private static final boolean   IS_REQUIRED = true;
    private MaterialTextField      textField1;
    private MaterialTextField      textField2;
    private MaterialTextField      textField3;
    private MaterialTextField      textField4;
    private MaterialTextField      textField5;
    private MaterialPasswordField  passwordField;
    private MaterialLabel          label1;
    private RippleButton              rippleButton;
    private MaterialGlyphButton       materialGlyphButton;
    private MaterialComboBox          materialComboBox;
    private MaterialCheckComboBox  materialCheckComboBox;
    private MaterialButton            floatingButton;
    private HBox                      buttonBox;
    private MaterialDatePicker        datePicker;
    private MaterialDateTimePicker    dateTimePicker;
    private MaterialNumberField       materialNumberField;
    private MaterialCurrencyField     materialCurrencyField;
    private MaterialDateRangeSelector materialDateRangeSelector;
    private MaterialLabel             dateRangeLabel;


    @Override public void init() {
        textField1 = (MaterialTextField) MaterialInputFieldBuilder.create()
                                                                  .promptText("Legal first name")
                                                                  .required(true)
                                                                  .build();new MaterialTextField(IS_REQUIRED);

        textField2 = (MaterialTextField) MaterialInputFieldBuilder.create()
                                                                  .promptText("Legal middle name")
                                                                  .hintText("Type in your middle name")
                                                                  .build();

        textField3 = (MaterialTextField) MaterialInputFieldBuilder.create()
                                                                  .text("Grunwald")
                                                                  .required(true)
                                                                  .promptText("Legal last name")
                                                                  .hintText("Type in your last name")
                                                                  .build();
        textField3.invalidProperty().addListener(o -> textField3.setHintText(textField3.isInvalid() ? "Invalid" : "Type in your last name"));

        textField4 = (MaterialTextField) MaterialInputFieldBuilder.create()
                                                                  .promptText("Read Only")
                                                                  .text("Text")
                                                                  .readOnly(true)
                                                                  .build();

        textField5 = (MaterialTextField) MaterialInputFieldBuilder.create()
                                                                  .promptText("Disabled")
                                                                  .text("Text")
                                                                  .disabled(true)
                                                                  .build();

        passwordField = (MaterialPasswordField) MaterialInputFieldBuilder.create()
                                                                         .type(Type.PASSWORD_FIELD)
                                                                         .hintText("min. 8 characters")
                                                                         .promptText("Password")
                                                                         .required(true)
                                                                         .build();
        passwordField.focusedProperty().addListener(o -> {
            if (!passwordField.isFocused()) { passwordField.setInvalid(passwordField.getText().length() < 8); }
        });

        label1 = new MaterialLabel("Gerrit", "Vorname");
        label1.setMinHeight(32);
        label1.setPrefHeight(32);
        label1.setMaxHeight(32);

        rippleButton = new RippleButton("Press");

        materialGlyphButton = new MaterialGlyphButton(FontAwesomeIcons.CLOSE);

        materialComboBox = new MaterialComboBox<>("Früchte", false);
        materialComboBox.setEditable(true);
        materialComboBox.getItems().addAll("Apfel", "Birne", "Kirsche", "Melone", "Ananas");
        materialComboBox.setHintTextAlwaysVisible(true);
        materialComboBox.setHintText("This is a hint");

        materialCheckComboBox = new MaterialCheckComboBox("Countries", false);
        materialCheckComboBox.setHintText("Little hint");
        materialCheckComboBox.setHintTextAlwaysVisible(true);
        materialCheckComboBox.getItems().addAll("Germany", "France", "Spain", "Portugal", "Italy", "Netherlands", "Belgium", "Austria");
        materialCheckComboBox.setMinWidth(200);
        materialCheckComboBox.setPrefWidth(250);
        materialCheckComboBox.setMaxWidth(300);
        //materialCheckComboBox.getSelectedItems().addListener((ListChangeListener) c -> System.out.println(materialCheckComboBox.getSelectedItems()));

        materialDateRangeSelector = new MaterialDateRangeSelector();
        dateRangeLabel = new MaterialLabel("", "Selected Range");
        materialDateRangeSelector.highDateProperty().addListener(o -> dateRangeLabel.setText(materialDateRangeSelector.getRangeString()));
        materialDateRangeSelector.lowDateProperty().addListener(o -> dateRangeLabel.setText(materialDateRangeSelector.getRangeString()));

        FontAwesomeIcon icon = new FontAwesomeIcon();
        icon.setGlyphName(FontAwesomeIcons.PLUS.name());
        icon.getStyleClass().add("icon");

        floatingButton = new MaterialButton();
        floatingButton.setGraphic(icon);
        floatingButton.setType(MaterialButtonType.FLOATING);
        floatingButton.setAlignment(Pos.CENTER);

        buttonBox = new HBox(rippleButton, materialGlyphButton, floatingButton, materialComboBox, materialCheckComboBox);
        buttonBox.setSpacing(40);

        ToggleGroup toggleGroup = new ToggleGroup();

        ToggleButton sampleButton1 = new ToggleButton("Task 1");
        sampleButton1.getStyleClass().addAll("first");
        sampleButton1.setToggleGroup(toggleGroup);

        ToggleButton sampleButton2 = new ToggleButton("Task 2");
        sampleButton2.setToggleGroup(toggleGroup);

        ToggleButton sampleButton3 = new ToggleButton("Task 3");
        sampleButton3.setToggleGroup(toggleGroup);

        ToggleButton sampleButton4 = new ToggleButton("Task 4");
        sampleButton4.setToggleGroup(toggleGroup);
        sampleButton4.getStyleClass().addAll("last");

        HBox segmentedButtonBar = new HBox(sampleButton1, sampleButton2, sampleButton3, sampleButton4);
        segmentedButtonBar.getStyleClass().add("segmented-button-bar");

        Region spacer = new Region();
        spacer.getStyleClass().setAll("spacer");

        datePicker = new MaterialDatePicker(LocalDate.now(), "Date", false);
        //datePicker.setDisable(true);
        datePicker.setEditable(false);
        //datePicker.getEditor().setEditable(false);
        datePicker.setHintText("Please select date");

        dateTimePicker = new MaterialDateTimePicker(LocalDateTime.now(), "Date Time", false);

        materialNumberField = new MaterialNumberField(BigDecimal.ZERO, "Number", "Type in a number", NumberField.DEFAULT_LOCALE, false, 2);
        //materialNumberField.setPercentageMode(true);

        materialCurrencyField = new MaterialCurrencyField(BigDecimal.ZERO, "Amount", "Type in your amount", CurrencyField.DEFAULT_LOCALE, false, 2);
    }

    @Override public void start(Stage stage) {
        Label title = new Label("Title");
        title.setFont(Font.font(36));
        title.setTextFill(Color.WHITE);
        title.relocate(10, 10);
        Ripple ripple  = new Ripple();
        Pane   topPane = new Pane(title);
        topPane.setPrefSize(400, 65);
        topPane.setEffect(new DropShadow(BlurType.TWO_PASS_BOX, Color.rgb(0, 0, 0, 0.65), 4, 0, 0, 2));
        topPane.getChildren().add(ripple);
        topPane.getStyleClass().addAll("top-pane");

        //VBox centerPane = new VBox(textField1, textField2, textField3, textField4, textField5, materialNumberField, materialCurrencyField, passwordField, label1, buttonBox, datePicker, dateTimePicker);
        VBox centerPane = new VBox(textField1, textField2, textField3, materialNumberField, passwordField, buttonBox, materialDateRangeSelector, dateRangeLabel);
        centerPane.setSpacing(36);
        centerPane.setPadding(new Insets(30));

        BorderPane pane = new BorderPane();
        pane.setTop(topPane);
        pane.setCenter(centerPane);

        //MaterialDesignColorScheme.applyTo(pane, MaterialDesignColor.UBS_GREY, MaterialDesignAccentColor.ORANGE);
        MaterialDesignColorScheme.applyTo(pane, MaterialDesignColor.PINK);

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(DemoMaterial.class.getResource("../styles.css").toExternalForm());

        stage.setTitle("Material Design TextField");
        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
