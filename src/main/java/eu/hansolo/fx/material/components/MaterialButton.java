/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import com.sun.javafx.css.converters.ColorConverter;
import com.sun.javafx.scene.control.skin.ButtonSkin;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.beans.property.ObjectProperty;
import javafx.css.CssMetaData;
import javafx.css.Styleable;
import javafx.css.StyleableObjectProperty;
import javafx.css.StyleableProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by hansolo on 28.04.16.
 */
public class MaterialButton extends Button {
    public enum MaterialButtonType { FLAT, RAISED, FLOATING }

    private static final Color              DEFAULT_RIPPLE_COLOR = Color.rgb(0, 0, 0, 0.11);
    private static final double             RIPPLE_MAX_RADIUS    = 300;
    private final        Rectangle          CLIP_RECT            = new Rectangle();
    private final        Circle             RIPPLE_CIRCLE        = new Circle(0.1);
    private final        Timeline           TIMELINE             = new Timeline();
    private final        FadeTransition     FADE_TRANSITION      = new FadeTransition();
    private final        ParallelTransition PARALLEL_TRANSITION  = new ParallelTransition();
    private double                          lastHeight;
    private double                          lastWidth;
    private ObjectProperty<Color>           rippleColor;


    // ******************** Constructors **************************************
    public MaterialButton() {
        this("", null);
    }
    public MaterialButton(final String TEXT) {
        this(TEXT, null);
    }
    public MaterialButton(final String TEXT, final Node GRAPHIC) {
        super(TEXT, GRAPHIC);
        getStylesheets().add(MaterialButton.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialButton.class.getResource("material-components.css").toExternalForm());
        getStyleClass().addAll("material-button");

        lastWidth    = 0;
        lastHeight   = 0;

        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        RIPPLE_CIRCLE.setOpacity(0.0);
        RIPPLE_CIRCLE.getStyleClass().setAll("ripple-circle");
    }

    private void registerListeners() {
        setOnMousePressed(evt -> {
            RIPPLE_CIRCLE.setCenterX(evt.getX());
            RIPPLE_CIRCLE.setCenterY(evt.getY());

            if (getWidth() != lastWidth || getHeight() != lastHeight) {
                lastWidth  = getWidth();
                lastHeight = getHeight();
                CLIP_RECT.setWidth(lastWidth);
                CLIP_RECT.setHeight(lastHeight);
                try {
                    final double CORNER_RADIUS = getBackground().getFills().get(0).getRadii().getTopLeftHorizontalRadius();
                    CLIP_RECT.setArcHeight(CORNER_RADIUS);
                    CLIP_RECT.setArcWidth(CORNER_RADIUS);
                    RIPPLE_CIRCLE.setClip(CLIP_RECT);
                } catch (Exception e) {}
                final double   RADIUS   = Math.min(Math.sqrt(Math.pow(lastWidth, 2) + Math.pow(lastHeight, 2)), RIPPLE_MAX_RADIUS) * 1.1 + 5;
                final Duration DURATION = new Duration((1.1 - 0.2 * (RADIUS / RIPPLE_MAX_RADIUS)) * 500);
                final KeyFrame KF_0     = new KeyFrame(DURATION, new KeyValue(RIPPLE_CIRCLE.radiusProperty(), RADIUS, Interpolator.EASE_OUT));
                TIMELINE.getKeyFrames().setAll(KF_0);

                FADE_TRANSITION.setDuration(DURATION);
                FADE_TRANSITION.setNode(RIPPLE_CIRCLE);
                FADE_TRANSITION.setInterpolator(Interpolator.EASE_OUT);
                FADE_TRANSITION.setFromValue(1.0);
                FADE_TRANSITION.setToValue(0.0);
            }

            PARALLEL_TRANSITION.stop();
            if (null != PARALLEL_TRANSITION.getOnFinished()) PARALLEL_TRANSITION.getOnFinished().handle(null);
            PARALLEL_TRANSITION.getChildren().setAll(TIMELINE, FADE_TRANSITION);
            PARALLEL_TRANSITION.setOnFinished(evt1 -> {
                RIPPLE_CIRCLE.setOpacity(0.0);
                RIPPLE_CIRCLE.setRadius(0.1);
            });
            PARALLEL_TRANSITION.playFromStart();
        });
    }


    // ******************** Methods *******************************************
    public void setType(final MaterialButtonType TYPE) {
        switch(TYPE) {
            case RAISED  : getStyleClass().setAll("button", "material-button", "button-raised"); break;
            case FLOATING: getStyleClass().setAll("button", "material-button", "button-floating"); break;
            case FLAT    :
            default      : getStyleClass().setAll("button", "material-button", "button-flat"); break;
        }
    }


    // ******************** CSS Stylable Properties ***************************
    public Color getRippleColor() { return null == rippleColor ? DEFAULT_RIPPLE_COLOR : rippleColor.get(); }
    public void setRippleColor(final Color COLOR) { rippleColorProperty().set(COLOR); }
    public ObjectProperty<Color> rippleColorProperty() {
        if (null == rippleColor) {
            rippleColor = new StyleableObjectProperty<Color>(DEFAULT_RIPPLE_COLOR) {
                @Override public CssMetaData getCssMetaData() { return MaterialButton.StyleableProperties.RIPPLE_COLOR; }
                @Override public Object getBean() { return MaterialButton.this; }
                @Override public String getName() { return "rippleColor"; }
            };
        }
        return rippleColor;
    }


    // ******************** Style related *************************************
    private static class StyleableProperties {
        private static final CssMetaData<MaterialButton, Color> RIPPLE_COLOR =
            new CssMetaData<MaterialButton, Color>("-ripple-color", ColorConverter.getInstance(), DEFAULT_RIPPLE_COLOR) {
                @Override public boolean isSettable(final MaterialButton BUTTON) { return null == BUTTON.rippleColor || !BUTTON.rippleColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final MaterialButton BUTTON) { return (StyleableProperty) BUTTON.rippleColorProperty(); }
                @Override public Color getInitialValue(final MaterialButton BUTTON) { return BUTTON.getRippleColor(); }
            };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
        static {
            final List<CssMetaData<? extends Styleable, ?>> styleables = new ArrayList<>(Control.getClassCssMetaData());
            Collections.addAll(styleables, RIPPLE_COLOR);
            STYLEABLES = Collections.unmodifiableList(styleables);
        }
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() { return MaterialButton.StyleableProperties.STYLEABLES; }
    @Override public List<CssMetaData<? extends Styleable, ?>> getControlCssMetaData() { return getClassCssMetaData(); }

    @Override protected Skin<?> createDefaultSkin() {
        final ButtonSkin SKIN = new ButtonSkin(this);
        getChildren().add(0, RIPPLE_CIRCLE);
        return SKIN;
    }
}
