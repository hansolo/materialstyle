/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.StackPane;


/**
 * Created by hansolo on 02.06.16.
 */
public class MaterialTitledPane extends TitledPane {
    private MaterialGlyphButton button;


    // ******************** Constructors **************************************
    public MaterialTitledPane() {
        super();
        initGraphics();
        registerListeners();
    }
    public MaterialTitledPane(final String TEXT, final Node NODE) {
        this();
        setText(TEXT);
        setContent(NODE);
    }
    public MaterialTitledPane(final String TEXT, final Node NODE, final boolean BUTTON_VISIBLE) {
        this();
        setText(TEXT);
        setContent(NODE);
        setButtonVisible(BUTTON_VISIBLE);
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        button = new MaterialGlyphButton(FontAwesomeIcons.ELLIPSIS_V);
        button.setVisible(false);
        setGraphic(button);
        setContentDisplay(ContentDisplay.RIGHT);
    }

    private void registerListeners() {
        skinProperty().addListener(o1 -> {
            if (null == getSkin() || null == getScene()) return;
            Platform.runLater(() -> {
                Node titleRegion    = lookup(".title");
                if (titleRegion == null) { return; }
                Insets padding      = ((StackPane) titleRegion).getPadding();
                double graphicWidth = getGraphic().getLayoutBounds().getWidth();
                double arrowWidth   = titleRegion.lookup(".arrow-button").getLayoutBounds().getWidth();
                double labelWidth   = titleRegion.lookup(".text").getLayoutBounds().getWidth();
                double nodesWidth   = graphicWidth + padding.getLeft() + padding.getRight() + arrowWidth + labelWidth;
                graphicTextGapProperty().bind(widthProperty().subtract(nodesWidth * 1.05));
            });
        });
    }


    // ******************** Methods *******************************************
    public boolean isButtonVisible() { return button.isVisible(); }
    public void setButtonVisible(final boolean VISIBLE) { button.setVisible(VISIBLE); }

    public FontAwesomeIcon getButtonIcon() { return button.getIcon(); }
    public void setButtonIcon(final FontAwesomeIcon ICON) { button.setIcon(ICON); }

    public void setOnButtonAction(final EventHandler<ActionEvent> HANDLER) { button.setOnAction(HANDLER); }

    public <T extends Event> void addButtonEventHandler(final EventType<T> TYPE, final EventHandler<? super T> HANDLER) { button.addEventHandler(TYPE, HANDLER); }
    public <T extends Event> void removeButtonEventHandler(final EventType<T> TYPE, final EventHandler<? super T> HANDLER) { button.removeEventHandler(TYPE, HANDLER); }

    public <T extends Event> void addButtonEventFilter(final EventType<T> TYPE, final EventHandler<? super T> HANDLER) { button.addEventFilter(TYPE, HANDLER); }
    public <T extends Event> void removeButtonEventFilter(final EventType<T> TYPE, final EventHandler<? super T> HANDLER) { button.removeEventFilter(TYPE, HANDLER); }
}
