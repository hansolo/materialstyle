/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by hansolo on 20.05.16.
 */
public class MaterialRadioGroup extends Region {
    private String                      _title;
    private StringProperty              title;
    private Text                        titleText;
    private VBox                        radioBox;
    private ObservableList<RadioButton> radios;
    private ToggleGroup                 toggleGroup;
    private Text                        requiredText;
    private HBox                        text;
    private boolean                     _required;
    private BooleanProperty             required;


    // ******************** Constructors **************************************
    public MaterialRadioGroup() {
        this("", false, null);
    }
    public MaterialRadioGroup(final String TITLE) {
        this(TITLE, false, null);
    }
    public MaterialRadioGroup(final String TITLE, final RadioButton... RADIOS) {
        this(TITLE, false, RADIOS);
    }
    public MaterialRadioGroup(final String TITLE, final boolean REQUIRED, final RadioButton... RADIOS) {
        getStylesheets().add(MaterialRadioGroup.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialRadioGroup.class.getResource("material-components.css").toExternalForm());
        getStyleClass().addAll("material-radio-group");

        _title    = TITLE;
        _required = REQUIRED;
        radios    = null == RADIOS ? FXCollections.observableArrayList() : FXCollections.observableArrayList(RADIOS);

        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        toggleGroup = new ToggleGroup();
        radios.forEach(radioButton -> radioButton.setToggleGroup(toggleGroup));

        requiredText = new Text(" *");
        requiredText.getStyleClass().add("required-text");
        requiredText.setVisible(isRequired());

        titleText = new Text(getTitle());
        titleText.setTextOrigin(VPos.TOP);
        titleText.getStyleClass().add("title-text");

        text = new HBox(titleText, requiredText);
        text.getStyleClass().add("material-radio-group");

        radioBox = new VBox();
        radioBox.setSpacing(12);
        radioBox.getChildren().addAll(radios);
        radioBox.getStyleClass().add("radio-box");

        setPadding(new Insets(24, 0, 0, 0));

        getChildren().addAll(text, radioBox);
    }

    private void registerListeners() {
        widthProperty().addListener(o -> resize());
        heightProperty().addListener(o -> resize());
        radios.addListener((ListChangeListener<RadioButton>) c -> {
            radioBox.getChildren().setAll(radios);
            radios.forEach(radioButton -> radioButton.setToggleGroup(toggleGroup));
        });
    }


    // ******************** Methods *******************************************
    public String getTitle() { return null == title ? _title : title.get(); }
    public void setTitle(final String TEXT) {
        if (null == title) {
            _title = TEXT;
            titleText.setText(TEXT);
        } else {
            title.set(TEXT);
        }
    }
    public StringProperty titleProperty() {
        if (null == title) {
            title = new StringPropertyBase(_title) {
                @Override protected void invalidated() { titleText.setText(get()); }
                @Override public Object getBean() { return MaterialRadioGroup.this; }
                @Override public String getName() { return "title"; }
            };
        }
        return title;
    }

    public List<RadioButton> getRadios() { return radios; }
    public void setRadios(final List<RadioButton> RADIOS) { radios.setAll(RADIOS); }
    public void setRadios(final RadioButton... RADIOS) { setRadios(Arrays.asList(RADIOS)); }
    public <E extends Enum<E>> void setRadios(final Class<E> ENUM, final E SELECTED_VALUE) { setRadios(ENUM, SELECTED_VALUE.name()); }
    public <E extends Enum<E>> void setRadios(final Class<E> ENUM, final String SELECTED_VALUE) {
        if (ENUM.getEnumConstants().length == 0) return;
        List<RadioButton> radios = new ArrayList<>(ENUM.getEnumConstants().length);
        for (Enum<E> enumValue: ENUM.getEnumConstants()) {
            RadioButton radioButton = new RadioButton(enumValue.name());
            radioButton.setSelected(null != SELECTED_VALUE && !SELECTED_VALUE.isEmpty() && SELECTED_VALUE.equals(enumValue.name()));
            radios.add(radioButton);
        }
        setRadios(radios);
    }
    public <E extends Enum<E>> void setRadios(final Class<E> ENUM) { setRadios(ENUM, ""); }
    public void addRadio(final RadioButton BUTTON) { if (!radios.contains(BUTTON)) radios.add(BUTTON); }
    public void removeRadio(final RadioButton BUTTON) { if (radios.contains(BUTTON)) radios.remove(BUTTON); }

    public ToggleGroup getToggleGroup() { return toggleGroup; }

    public double getSpacing() { return radioBox.getSpacing(); }
    public void setSpacing(final double SPACING) { radioBox.setSpacing(SPACING); }
    public DoubleProperty spacingProperty() { return radioBox.spacingProperty(); }

    public boolean isRequired() { return null == required ? _required : required.get(); }
    public void setRequired(final boolean REQUIRED) {
        if (null == required) {
            _required = REQUIRED;
            requiredText.setVisible(_required);
        } else {
            required.set(REQUIRED);
        }
    }
    public BooleanProperty requiredProperty() {
        if (null == required) {
            required = new BooleanPropertyBase(_required) {
                @Override protected void invalidated() { requiredText.setVisible(get()); }
                @Override public Object getBean() { return MaterialRadioGroup.this; }
                @Override public String getName() { return "required"; }
            };
        }
        return required;
    }

    private void resize() {
        text.setLayoutX(0);
        text.setLayoutY(0);
    }
}
