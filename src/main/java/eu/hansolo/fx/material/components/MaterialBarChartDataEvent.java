/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.paint.Color;


/**
 * Created by hansolo on 13.12.16.
 */
public class MaterialBarChartDataEvent<X,Y> extends Event {
    public static final EventType<MaterialBarChartDataEvent> BAR_CHART_DATA = new EventType(ANY, "BAR_CHART_DATA");
    private String    seriesName;
    private Data<X,Y> data;
    private Color     topColor;
    private Color     bottomColor;


    // ******************** Constructors **************************************
    public MaterialBarChartDataEvent(final Data<X,Y> DATA) {
        super(BAR_CHART_DATA);
        data = DATA;
    }
    public MaterialBarChartDataEvent(final String SERIES_NAME, final Data<X,Y> DATA, final Color TOP_COLOR, final Color BOTTOM_COLOR) {
        super(BAR_CHART_DATA);
        seriesName  = SERIES_NAME;
        data        = DATA;
        topColor    = TOP_COLOR;
        bottomColor = BOTTOM_COLOR;
    }
    public MaterialBarChartDataEvent(final Object SRC, final EventTarget TARGET, final EventType<MaterialBarChartDataEvent> TYPE, final Data<X,Y> DATA) {
        super(SRC, TARGET, TYPE);
        data = DATA;
    }


    // ******************** Methods *******************************************
    public String getSeriesName() { return seriesName; }

    public Data getData() { return data; }

    public Color getTopColor() { return topColor; }

    public Color getBottomColor() { return bottomColor; }
}
