/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import eu.hansolo.fx.material.tableview.CellField;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;


/**
 * Created by hansolo on 01.06.16.
 */
public class MaterialTableView<S> extends TableView<S> {

    public MaterialTableView() {
        this(FXCollections.<S>observableArrayList());
    }
    public MaterialTableView(final ObservableList<S> ITEMS) {
        super(ITEMS);

        getStylesheets().add(MaterialTableView.class.getResource("../styles.css").toExternalForm());
        registerListeners();
    }

    private void registerListeners() {
        setOnMouseClicked(mouseEvent -> {
            int         row = getSelectionModel().getSelectedIndex();
            TableColumn col = getFocusModel().getFocusedCell().getTableColumn();
            edit(row, col);
        });

        setOnKeyPressed(keyEvent -> {
            KeyCode keyCode = keyEvent.getCode();
            if (keyCode == KeyCode.ENTER || keyCode == KeyCode.ESCAPE){
                CellField.clearText();
            }
            if (keyCode.isDigitKey()) {
                int         row = getSelectionModel().getSelectedIndex();
                TableColumn col = getFocusModel().getFocusedCell().getTableColumn();
                edit(row, col);
            }
        });

        final ObservableList<TablePosition> selectedCells = getSelectionModel().getSelectedCells();
        selectedCells.addListener((ListChangeListener<TablePosition>) c -> {
            for (TablePosition pos : selectedCells) { edit(pos.getRow(), pos.getTableColumn()); }
        });
    }
}
