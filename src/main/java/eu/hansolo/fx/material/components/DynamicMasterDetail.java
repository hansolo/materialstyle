/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class DynamicMasterDetail extends Application {
    private StackPane master;
    private SideBar   sideBar;


    @Override public void init() {
        master = new StackPane(new Text("MASTER - PANE"));
        master.setPrefSize(800, 600);

        final Pane detailContentPane = createSidebarContent();
        sideBar = new SideBar(250, detailContentPane);
        VBox.setVgrow(detailContentPane, Priority.ALWAYS);
    }

    @Override public void start(final Stage stage) throws Exception {
        final BorderPane pane = new BorderPane();
        VBox mainPane = new VBox(10, sideBar.getControlButton(), master);
        pane.setRight(sideBar);
        pane.setCenter(mainPane);

        Scene scene = new Scene(pane);

        stage.setTitle("Dynamic Master-Detail");
        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    private BorderPane createSidebarContent() {
        final Label text = new Label("DETAIL - PANE");
        text.setMinWidth(0);


        final BorderPane detailPane = new BorderPane();
        detailPane.setMinWidth(0);
        detailPane.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        detailPane.setCenter(text);

        return detailPane;
    }

    public static void main(String[] args) throws Exception { launch(args); }
}