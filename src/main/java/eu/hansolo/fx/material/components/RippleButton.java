/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import com.sun.javafx.scene.control.skin.ButtonSkin;
import eu.hansolo.fx.material.effect.Ripple;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Skin;


/**
 * Created by hansolo on 28.04.16.
 */
public class RippleButton extends Button {

    public RippleButton() {
        this("", null);
    }
    public RippleButton(final String TEXT) {
        this(TEXT, null);
    }
    public RippleButton(final String TEXT, final Node GRAPHIC) {
        super(TEXT, GRAPHIC);
    }


    @Override protected Skin<?> createDefaultSkin() {
        final ButtonSkin SKIN = new ButtonSkin(this);
        getChildren().add(0, new Ripple());
        return SKIN;
    }
}
