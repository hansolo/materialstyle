/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;


/**
 * Created by hansolo on 18.05.16.
 */
public class MaterialCard extends AnchorPane {
    private String          _title;
    private StringProperty  title;
    private Label           titleLabel;
    // Top Toolbar
    private ToolBar         topToolBar;
    private Button          buttonTop1;
    private FontAwesomeIcon iconButtonTop1;
    private Button          buttonTop2;
    private FontAwesomeIcon iconButtonTop2;
    private Button          buttonTop3;
    private FontAwesomeIcon iconButtonTop3;
    private ToggleButton    buttonTop4;
    private FontAwesomeIcon iconButtonTop4;
    // Bottom Toolbar
    private ToolBar         bottomToolBar;
    private Button          buttonBottom1;
    private FontAwesomeIcon iconButtonBottom1;
    private Button          buttonBottom2;
    private FontAwesomeIcon iconButtonBottom2;
    private Button          buttonBottom3;
    private FontAwesomeIcon iconButtonBottom3;
    private Button          buttonBottom4;
    private FontAwesomeIcon iconButtonBottom4;
    private boolean         _bottomAreaVisible;
    private BooleanProperty bottomAreaVisible;
    // Center
    private Node            center;
    


    // ******************** Constructors **************************************
    public MaterialCard() {
        this("", null);
    }
    public MaterialCard(final String TITLE) {
        this(TITLE, null);
    }
    public MaterialCard(final String TITLE, final Node CENTER) {
        super();

        getStylesheets().add(MaterialCard.class.getResource("../styles.css").toExternalForm());
        getStylesheets().add(MaterialCard.class.getResource("material-components.css").toExternalForm());
        getStyleClass().addAll("material-card");

        center             = CENTER;
        _title             = TITLE;
        _bottomAreaVisible = true;

        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        // Title
        titleLabel = new Label(getTitle());
        titleLabel.getStyleClass().add("card-title");
        AnchorPane.setTopAnchor(titleLabel, 0d);
        AnchorPane.setLeftAnchor(titleLabel, 0d);

        // Top ToolBar
        iconButtonTop1 = new FontAwesomeIcon();
        iconButtonTop1.setGlyphName(FontAwesomeIcons.BACKWARD.name());
        iconButtonTop1.getStyleClass().add("card-icon");
        buttonTop1 = new Button();
        buttonTop1.setGraphic(iconButtonTop1);
        buttonTop1.setVisible(false);
        
        iconButtonTop2 = new FontAwesomeIcon();
        iconButtonTop2.setGlyphName(FontAwesomeIcons.FORWARD.name());
        iconButtonTop2.getStyleClass().add("card-icon");
        buttonTop2 = new Button();
        buttonTop2.setGraphic(iconButtonTop2);
        buttonTop2.setVisible(false);
        
        iconButtonTop3 = new FontAwesomeIcon();
        iconButtonTop3.setGlyphName(FontAwesomeIcons.SORT.name());
        iconButtonTop3.getStyleClass().add("card-icon");
        buttonTop3 = new Button();
        buttonTop3.setGraphic(iconButtonTop3);
        buttonTop3.setVisible(false);
        
        iconButtonTop4 = new FontAwesomeIcon();
        iconButtonTop4.setGlyphName(FontAwesomeIcons.BARS.name());
        iconButtonTop4.getStyleClass().add("card-icon");
        buttonTop4 = new ToggleButton();
        buttonTop4.setGraphic(iconButtonTop4);

        topToolBar = new ToolBar(buttonTop1, buttonTop2, buttonTop3, buttonTop4);
        topToolBar.setPrefHeight(64);
        topToolBar.getStyleClass().add("card-tool-bar");
        AnchorPane.setTopAnchor(topToolBar, 0d);
        AnchorPane.setRightAnchor(topToolBar, 0d);


        // Bottom ToolBar
        iconButtonBottom1 = new FontAwesomeIcon();
        iconButtonBottom1.setGlyphName(FontAwesomeIcons.EYE.name());
        iconButtonBottom1.getStyleClass().add("card-icon");
        buttonBottom1 = new Button();
        buttonBottom1.setGraphic(iconButtonBottom1);
        buttonBottom1.setVisible(false);

        iconButtonBottom2 = new FontAwesomeIcon();
        iconButtonBottom2.setGlyphName(FontAwesomeIcons.USER.name());
        iconButtonBottom2.getStyleClass().add("card-icon");
        buttonBottom2 = new Button();
        buttonBottom2.setGraphic(iconButtonBottom2);
        buttonBottom2.setVisible(false);

        iconButtonBottom3 = new FontAwesomeIcon();
        iconButtonBottom3.setGlyphName(FontAwesomeIcons.ANGLE_LEFT.name());
        iconButtonBottom3.getStyleClass().add("card-icon");
        buttonBottom3 = new Button();
        buttonBottom3.setGraphic(iconButtonBottom3);

        iconButtonBottom4 = new FontAwesomeIcon();
        iconButtonBottom4.setGlyphName(FontAwesomeIcons.ANGLE_RIGHT.name());
        iconButtonBottom4.getStyleClass().add("card-icon");
        buttonBottom4 = new Button();
        buttonBottom4.setGraphic(iconButtonBottom4);

        bottomToolBar = new ToolBar(buttonBottom1, buttonBottom2, buttonBottom3, buttonBottom4);
        bottomToolBar.setPrefHeight(56);
        bottomToolBar.getStyleClass().add("card-tool-bar");
        AnchorPane.setRightAnchor(bottomToolBar, 0d);
        AnchorPane.setBottomAnchor(bottomToolBar, 0d);
        AnchorPane.setLeftAnchor(bottomToolBar, 0d);

        doLayout();
    }

    private void registerListeners() {

    }


    // ******************** Public Methods ************************************
    public String getTitle() { return null == title ? _title : title.get(); }
    public void setTitle(final String TITLE) {
        if (null == title) {
            _title = TITLE;
            titleLabel.setText(_title);
        } else {
            title.set(TITLE);
        }
    }
    public StringProperty titleProperty() {
        if (null == title) {
            title = new StringPropertyBase(_title) {
                @Override protected void invalidated() { titleLabel.setText(get()); }
                @Override public Object getBean() { return MaterialCard.this; }
                @Override public String getName() { return "title"; }
            };
            _title = null;
        }
        return title;
    };
    
    public void setCenter(final Node NODE) {
        center = NODE;
        doLayout();
    }

    // Top Toolbar Button Visibility
    public boolean isButtonTop1Visible() { return buttonTop1.isVisible(); }
    public void setButtonTop1Visible(final boolean VISIBLE) { buttonTop1.setVisible(VISIBLE); }
    public BooleanProperty buttonTop1VisibleProperty() { return buttonTop1.visibleProperty(); }

    public boolean isButtonTop2Visible() { return buttonTop2.isVisible(); }
    public void setButtonTop2Visible(final boolean VISIBLE) { buttonTop2.setVisible(VISIBLE); }
    public BooleanProperty buttonTop2VisibleProperty() { return buttonTop2.visibleProperty(); }

    public boolean isButtonTop3Visible() { return buttonTop3.isVisible(); }
    public void setButtonTop3Visible(final boolean VISIBLE) { buttonTop3.setVisible(VISIBLE); }
    public BooleanProperty buttonTop3VisibleProperty() { return buttonTop3.visibleProperty(); }

    public boolean isButtonTop4Visible() { return buttonTop4.isVisible(); }
    public void setButtonTop4Visible(final boolean VISIBLE) { buttonTop4.setVisible(VISIBLE); }
    public BooleanProperty buttonTop4VisibleProperty() { return buttonTop4.visibleProperty(); }

    // Top Toolbar Button Text, Tooltip and Icon
    public String getButtonTop1Text() { return buttonTop1.getText(); }
    public void setButtonTop1Text(final String TEXT) { buttonTop1.setText(TEXT); }
    public StringProperty buttonTop1TextProperty() { return buttonTop1.textProperty(); }
    
    public Tooltip getButtonTop1Tooltip() { return buttonTop1.getTooltip(); }
    public void setButtonTop1Tooltip(final Tooltip TOOLTIP) { buttonTop1.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonTop1TooltipProperty() { return buttonTop1.tooltipProperty(); }
    
    public void setIconButtonTop1(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonTop1.setGraphic(null);
        } else {
            iconButtonTop1.setGlyphName(ICON.name());
            buttonTop1.setGraphic(iconButtonTop1);
        }
    }

    public String getButtonTop2Text() { return buttonTop2.getText(); }
    public void setButtonTop2Text(final String TEXT) { buttonTop2.setText(TEXT); }
    public StringProperty buttonTop2TextProperty() { return buttonTop2.textProperty(); }

    public Tooltip getButtonTop2Tooltip() { return buttonTop2.getTooltip(); }
    public void setButtonTop2Tooltip(final Tooltip TOOLTIP) { buttonTop2.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonTop2TooltipProperty() { return buttonTop2.tooltipProperty(); }
    
    public void setIconButtonTop2(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonTop2.setGraphic(null);
        } else {
            iconButtonTop2.setGlyphName(ICON.name());
            buttonTop2.setGraphic(iconButtonTop2);
        }
    }

    public String getButtonTop3Text() { return buttonTop3.getText(); }
    public void setButtonTop3Text(final String TEXT) { buttonTop3.setText(TEXT); }
    public StringProperty buttonTop3TextProperty() { return buttonTop3.textProperty(); }

    public Tooltip getButtonTop3Tooltip() { return buttonTop3.getTooltip(); }
    public void setButtonTop3Tooltip(final Tooltip TOOLTIP) { buttonTop3.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonTop3TooltipProperty() { return buttonTop3.tooltipProperty(); }

    public void setIconButtonTop3(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonTop3.setGraphic(null);
        } else {
            iconButtonTop3.setGlyphName(ICON.name());
            buttonTop3.setGraphic(iconButtonTop3);
        }
    }

    public String getButtonTop4Text() { return buttonTop4.getText(); }
    public void setButtonTop4Text(final String TEXT) {
        if (TEXT.isEmpty())
            buttonTop4.setText(TEXT);
    }
    public StringProperty buttonTop4TextProperty() { return buttonTop4.textProperty(); }

    public Tooltip getButtonTop4Tooltip() { return buttonTop4.getTooltip(); }
    public void setButtonTop4Tooltip(final Tooltip TOOLTIP) { buttonTop4.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonTop4TooltipProperty() { return buttonTop4.tooltipProperty(); }

    public void setIconButtonTop4(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonTop4.setGraphic(null);
        } else {
            iconButtonTop4.setGlyphName(ICON.name());
            buttonTop4.setGraphic(iconButtonTop4);
        }
    }
    
    // Bottom Toolbar Button Visibility
    public boolean isButtonBottom1Visible() { return buttonBottom1.isVisible(); }
    public void setButtonBottom1Visible(final boolean VISIBLE) { buttonBottom1.setVisible(VISIBLE); }
    public BooleanProperty buttonBottom1VisibleProperty() { return buttonBottom1.visibleProperty(); }

    public boolean isButtonBottom2Visible() { return buttonBottom2.isVisible(); }
    public void setButtonBottom2Visible(final boolean VISIBLE) { buttonBottom2.setVisible(VISIBLE); }
    public BooleanProperty buttonBottom2VisibleProperty() { return buttonBottom2.visibleProperty(); }

    public boolean isButtonBottom3Visible() { return buttonBottom3.isVisible(); }
    public void setButtonBottom3Visible(final boolean VISIBLE) { buttonBottom3.setVisible(VISIBLE); }
    public BooleanProperty buttonBottom3VisibleProperty() { return buttonBottom3.visibleProperty(); }

    public boolean isButtonBottom4Visible() { return buttonBottom4.isVisible(); }
    public void setButtonBottom4Visible(final boolean VISIBLE) { buttonBottom4.setVisible(VISIBLE); }
    public BooleanProperty buttonBottom4VisibleProperty() { return buttonBottom4.visibleProperty(); }
    
    // Bottom Toolbar Visibility
    public boolean isBottomAreaVisible() { return null == bottomAreaVisible ? _bottomAreaVisible : bottomAreaVisible.get(); }
    public void setBottomAreaVisible(final boolean VISIBLE) {
        if (null == bottomAreaVisible) {
            _bottomAreaVisible = VISIBLE;
            showBottomArea(_bottomAreaVisible);
        } else {
            bottomAreaVisible.set(VISIBLE);
        }
    }
    public BooleanProperty bottomAreaVisibleProperty() {
        if (null == bottomAreaVisible) {
            bottomAreaVisible = new BooleanPropertyBase(_bottomAreaVisible) {
                @Override protected void invalidated() { showBottomArea(get()); }
                @Override public Object getBean() { return MaterialCard.this; }
                @Override public String getName() { return "bottomAreaVisible"; }
            };
        }
        return bottomAreaVisible;
    }

    // Bottom Toolbar Button Text, Tooltip and Icon
    public String getButtonBottom1Text() { return buttonBottom1.getText(); }
    public void setButtonBottom1Text(final String TEXT) { buttonBottom1.setText(TEXT); }
    public StringProperty buttonBottom1TextProperty() { return buttonBottom1.textProperty(); }

    public Tooltip getButtonBottom1Tooltip() { return buttonBottom1.getTooltip(); }
    public void setButtonBottom1Tooltip(final Tooltip TOOLTIP) { buttonBottom1.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonBottom1TooltipProperty() { return buttonBottom1.tooltipProperty(); }

    public void setIconButtonBottom1(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonBottom1.setGraphic(null);
        } else {
            iconButtonBottom1.setGlyphName(ICON.name());
            buttonBottom1.setGraphic(iconButtonBottom1);
        }
    }
    
    public String getButtonBottom2Text() { return buttonBottom2.getText(); }
    public void setButtonBottom2Text(final String TEXT) { buttonBottom2.setText(TEXT); }
    public StringProperty buttonBottom2TextProperty() { return buttonBottom2.textProperty(); }

    public Tooltip getButtonBottom2Tooltip() { return buttonBottom2.getTooltip(); }
    public void setButtonBottom2Tooltip(final Tooltip TOOLTIP) { buttonBottom2.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonBottom2TooltipProperty() { return buttonBottom2.tooltipProperty(); }

    public void setIconButtonBottom2(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonBottom2.setGraphic(null);
        } else {
            iconButtonBottom2.setGlyphName(ICON.name());
            buttonBottom2.setGraphic(iconButtonBottom2);
        }
    }
    
    public String getButtonBottom3Text() { return buttonBottom3.getText(); }
    public void setButtonBottom3Text(final String TEXT) { buttonBottom3.setText(TEXT); }
    public StringProperty buttonBottom3TextProperty() { return buttonBottom3.textProperty(); }

    public Tooltip getButtonBottom3Tooltip() { return buttonBottom3.getTooltip(); }
    public void setButtonBottom3Tooltip(final Tooltip TOOLTIP) { buttonBottom3.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonBottom3TooltipProperty() { return buttonBottom3.tooltipProperty(); }

    public void setIconButtonBottom3(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonBottom3.setGraphic(null);
        } else {
            iconButtonBottom3.setGlyphName(ICON.name());
            buttonBottom3.setGraphic(iconButtonBottom3);
        }
    }

    public String getButtonBottom4Text() { return buttonBottom4.getText(); }
    public void setButtonBottom4Text(final String TEXT) {
        if (TEXT.isEmpty())
        buttonBottom4.setText(TEXT);
    }
    public StringProperty buttonBottom4TextProperty() { return buttonBottom4.textProperty(); }

    public Tooltip getButtonBottom4Tooltip() { return buttonBottom4.getTooltip(); }
    public void setButtonBottom4Tooltip(final Tooltip TOOLTIP) { buttonBottom4.setTooltip(TOOLTIP); }
    public ObjectProperty<Tooltip> buttonBottom4TooltipProperty() { return buttonBottom4.tooltipProperty(); }

    public void setIconButtonBottom4(final FontAwesomeIcons ICON) {
        if (null == ICON) {
            buttonBottom4.setGraphic(null);
        } else {
            iconButtonBottom4.setGlyphName(ICON.name());
            buttonBottom4.setGraphic(iconButtonBottom4);
        }
    }


    // ******************** Private Methods ***********************************
    private void showBottomArea(final boolean SHOW) {
        if (SHOW) {
            bottomToolBar.setManaged(true);
            bottomToolBar.setVisible(true);
            AnchorPane.setBottomAnchor(center, 56d);
        } else {
            bottomToolBar.setVisible(false);
            bottomToolBar.setManaged(false);
            AnchorPane.setBottomAnchor(center, 0d);
        }
    }

    private void doLayout() {
        if (null == center) {
            getChildren().setAll(titleLabel, topToolBar, bottomToolBar);
        } else {
            AnchorPane.setTopAnchor(center, 64d);
            AnchorPane.setRightAnchor(center, 14d);
            AnchorPane.setBottomAnchor(center, isBottomAreaVisible() ? 56d : 0d);
            AnchorPane.setLeftAnchor(center, 24d);
            getChildren().setAll(titleLabel, topToolBar, bottomToolBar, center);
        }
    }


    // ******************** EventHandling Top Toolbar *************************
    public void setOnButtonTop1Action(final EventHandler<ActionEvent> HANDLER) { buttonTop1.setOnAction(HANDLER); }
    public void addOnButtonTop1Action(final EventHandler<ActionEvent> HANDLER) { buttonTop1.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonTop1Action(final EventHandler<ActionEvent> HANDLER) { buttonTop1.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonTop1ActionProperty() { return buttonTop1.onActionProperty(); }
    
    public void setOnButtonTop2Action(final EventHandler<ActionEvent> HANDLER) { buttonTop2.setOnAction(HANDLER); }
    public void addOnButtonTop2Action(final EventHandler<ActionEvent> HANDLER) { buttonTop2.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonTop2Action(final EventHandler<ActionEvent> HANDLER) { buttonTop2.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonTop2ActionProperty() { return buttonTop2.onActionProperty(); }
    
    public void setOnButtonTop3Action(final EventHandler<ActionEvent> HANDLER) { buttonTop3.setOnAction(HANDLER); }
    public void addOnButtonTop3Action(final EventHandler<ActionEvent> HANDLER) { buttonTop3.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonTop3Action(final EventHandler<ActionEvent> HANDLER) { buttonTop3.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonTop3ActionProperty() { return buttonTop3.onActionProperty(); }
    
    public void setOnButtonTop4Action(final EventHandler<ActionEvent> HANDLER) { buttonTop4.setOnAction(HANDLER); }
    public void addOnButtonTop4Action(final EventHandler<ActionEvent> HANDLER) { buttonTop4.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonTop4Action(final EventHandler<ActionEvent> HANDLER) { buttonTop4.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonTop4ActionProperty() { return buttonTop4.onActionProperty(); }

    
    // ******************** EventHandling Bottom Toolbar **********************
    public void setOnButtonBottom1Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom1.setOnAction(HANDLER); }
    public void addOnButtonBottom1Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom1.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonBottom1Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom1.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonBottom1ActionProperty() { return buttonBottom1.onActionProperty(); }
    
    public void setOnButtonBottom2Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom2.setOnAction(HANDLER); }
    public void addOnButtonBottom2Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom2.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonBottom2Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom2.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonBottom2ActionProperty() { return buttonBottom2.onActionProperty(); }
    
    public void setOnButtonBottom3Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom3.setOnAction(HANDLER); }
    public void addOnButtonBottom3Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom3.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonBottom3Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom3.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonBottom3ActionProperty() { return buttonBottom3.onActionProperty(); }

    public void setOnButtonBottom4Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom4.setOnAction(HANDLER); }
    public void addOnButtonBottom4Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom4.addEventHandler(ActionEvent.ANY, HANDLER); }
    public void removeOnButtonBottom4Action(final EventHandler<ActionEvent> HANDLER) { buttonBottom4.removeEventHandler(ActionEvent.ANY, HANDLER); }
    public ObjectProperty<EventHandler<ActionEvent>> onButtonBottom4ActionProperty() { return buttonBottom4.onActionProperty(); }
}
