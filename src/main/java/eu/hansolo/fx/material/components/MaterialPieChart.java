/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

import java.util.List;


/**
 * Created by hansolo on 24.11.16.
 */
public class MaterialPieChart extends PieChart {
    private static final Duration ANIMATION_DURATION = new Duration(200);
    private static final double   ANIMATION_DISTANCE = 0.08;


    // ******************** Constructors **************************************
    public MaterialPieChart() {
        this(FXCollections.<Data>observableArrayList());
    }
    public MaterialPieChart(final ObservableList<Data> DATA) {
        super(DATA);
        if (!DATA.isEmpty()) { addMouseListeners(); }
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void registerListeners() {
        setOnMouseExited(e -> resetChart(MaterialPieChart.this));
        getData().addListener((ListChangeListener<Data>) change -> {
            while (change.next()) {
                if (change.wasPermutated()) {
                    /*
                    for (int i = change.getFrom() ; i < change.getTo() ; ++i) {
                        System.out.println(getData().get(i) + " was permutated");
                    }
                    */
                } else if (change.wasUpdated()) {
                    /*
                    for (int i = change.getFrom(); i < change.getTo(); ++i) {
                        System.out.println(getData().get(i) + " was updated");
                    }
                    */
                } else if (change.wasAdded()) {
                    //change.getAddedSubList().forEach(addedItem -> System.out.println(addedItem + " added"));
                } else if (change.wasRemoved()) {
                    //change.getRemoved().forEach(removedItem -> System.out.println(removedItem + " removed"));
                }
            }
        });
    }


    // ******************** Methods *******************************************
    private void addMouseListeners() {
        getData().forEach(data -> data.getNode().setOnMousePressed(new MousePressAnimation(data, MaterialPieChart.this)));
    }

    private static void resetChart(final PieChart CHART) {
        for (PieChart.Data data : CHART.getData()) {
            Node node = data.getNode();
            if (node.getTranslateX() != 0 || node.getTranslateY() != 0) {
                resetNode(node);
                break;
            }
        }
    }

    private static void resetNode(final Node NODE) {
        TranslateTransition translate = new TranslateTransition(ANIMATION_DURATION, NODE);
        translate.setToX(0);
        translate.setToY(0);
        translate.play();
    }

    private static class MousePressAnimation implements EventHandler<MouseEvent> {
        private double        cos;
        private double        sin;
        private PieChart      chart;
        private PieChart.Data data;
        private List<Data>    dataList;


        public MousePressAnimation(final PieChart.Data DATA, final PieChart CHART) {
            data         = DATA;
            chart        = CHART;
            dataList     = chart.getData();
            double start = 0;
            double angle = calcAngle(data);
            for( PieChart.Data dataItem : dataList ) {
                if(dataItem.equals(data)) { break; }
                start += calcAngle(dataItem);
            }
            angle = Math.toRadians(-start - angle * 0.5);
            cos   = Math.cos(angle);
            sin   = Math.sin(angle);
        }

        @Override public void handle(final MouseEvent EVENT) {
            Node node = (Node) EVENT.getSource();
            if (node.getTranslateX() != 0 || node.getTranslateY() != 0) {
                resetNode(node);
                EVENT.consume();
                return;
            } else {
                resetChart(chart);
            }

            double minX = Double.MAX_VALUE;
            double maxX = -1 * Double.MAX_VALUE;

            for( PieChart.Data data : dataList ) {
                Bounds bounds = data.getNode().getBoundsInParent();
                minX = Math.min(minX, bounds.getMinX());
                maxX = Math.max(maxX, bounds.getMaxX());
            }

            double              radius    = maxX - minX;
            TranslateTransition translate = new TranslateTransition(ANIMATION_DURATION, node);
            translate.setToX((radius * ANIMATION_DISTANCE) * cos);
            translate.setToY(-(radius * ANIMATION_DISTANCE) * sin);
            translate.play();
            chart.fireEvent(new MaterialPieChartDataEvent(data));
        }

        private static double calcAngle(final PieChart.Data DATA) {
            ObservableList<Data> dataList = DATA.getChart().getData();
            double               total    = dataList.stream().mapToDouble(tmp -> tmp.getPieValue()).sum();
            return 360 * (DATA.getPieValue() / total);
        }
    }
}
