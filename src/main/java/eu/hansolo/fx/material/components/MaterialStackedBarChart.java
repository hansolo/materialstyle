/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.beans.NamedArg;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * Created by hansolo on 14.12.16.
 */
public class MaterialStackedBarChart<X, Y> extends StackedBarChart<X, Y> {
    private Map<Node, StackedSectionData> nodeMap = new HashMap<>();


    // ******************** Constructors **********************************
    public MaterialStackedBarChart(@NamedArg("xAxis") final Axis<X> xAxis, @NamedArg("yAxis") final Axis<Y> yAxis) {
        super(xAxis, yAxis);
    }
    public MaterialStackedBarChart(@NamedArg("xAxis") final Axis<X> xAxis, @NamedArg("yAxis") final Axis<Y> yAxis, @NamedArg("data") final ObservableList<Series<X, Y>> data) {
        super(xAxis, yAxis, data);
    }
    public MaterialStackedBarChart(@NamedArg("xAxis") final Axis<X> xAxis, @NamedArg("yAxis") final Axis<Y> yAxis, @NamedArg("data") final ObservableList<Series<X, Y>> data, @NamedArg("categoryGap") final double categoryGap) {
        super(xAxis, yAxis, data, categoryGap);
    }


    // ******************** Methods ***************************************
    @Override protected void seriesAdded(final Series<X, Y> SERIES, final int INDEX) {
        super.seriesAdded(SERIES, INDEX);
        for (int j = 0; j < SERIES.getData().size(); j++) {
            final Data<X, Y>         ITEM         = SERIES.getData().get(j);
            final StackedSectionData SECTION_DATA = new StackedSectionData(ITEM.getXValue().toString(), ITEM.getYValue());
            nodeMap.put(ITEM.getNode(), SECTION_DATA);
            SECTION_DATA.getValueText().toFront();
            getPlotChildren().add(SECTION_DATA.getValueText());
            ITEM.getNode().setOnMousePressed((MouseEvent event) -> fireEvent(new MaterialStackedBarChartDataEvent<>(SECTION_DATA.getName(), ITEM)));
        }
    }

    @Override protected void seriesRemoved(final Series<X, Y> SERIES) {
        for (Node bar : nodeMap.keySet()) {
            final StackedSectionData SECTION_DATA = nodeMap.get(bar);
            getPlotChildren().remove(SECTION_DATA.getValueText());
        }
        nodeMap.clear();
        super.seriesRemoved(SERIES);
    }

    @Override protected void layoutPlotChildren() {
        super.layoutPlotChildren();
        for (Node bar : nodeMap.keySet()) {
            Node text = nodeMap.get(bar).getValueText();

            Bounds bounds = bar.getBoundsInParent();
            text.setLayoutX(Math.round(bounds.getMinX() + bounds.getWidth() / 2 - text.prefWidth(-1) / 2));
            text.setLayoutY(bounds.getMinY() + bounds.getHeight() * 0.5);
        }
    }


    // ******************** Inner Classes *************************************
    private class StackedSectionData {
        private String name;
        private Y      value;
        private Text   valueText;


        // ******************** Constructors **********************************
        public StackedSectionData() { this("", null); }
        public StackedSectionData(final String NAME, final Y VALUE) {
            name  = NAME;
            value = VALUE;
            valueText = new Text(String.format(Locale.US, "%.2f", VALUE));
            valueText.setTextOrigin(VPos.CENTER);
            valueText.setFill(Color.WHITE);
            valueText.setMouseTransparent(true);
        }


        // ******************** Methods ***************************************
        public String getName() { return name; }
        public void setName(final String NAME) { name = NAME; }

        public Y getValue() { return value; }
        public void setValue(final Y VALUE) {
            value = VALUE;
            valueText.setText(null == VALUE ? "" : String.valueOf(VALUE));
        }

        public Text getValueText() { return valueText; }
    }
}
