/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Side;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by hansolo on 27.06.16.
 */
public class MaterialSpecialTabPaneBuilder<B> {
    private List<Tab> tabs = new ArrayList<>();

    private Map<String, Property> properties = new HashMap<>();

    protected MaterialSpecialTabPaneBuilder() {
    }

    public static MaterialSpecialTabPaneBuilder<?> create() { return new MaterialSpecialTabPaneBuilder(); }

    public B rotateGraphic(final boolean ROTATE) {
        properties.put("rotateGraphic", new SimpleBooleanProperty(ROTATE));
        return (B)this;
    }

    public B selectionModel(final SingleSelectionModel<Tab> SELECTION_MODEL) {
        properties.put("selectionModel", new SimpleObjectProperty(SELECTION_MODEL));
        return (B)this;
    }

    public B side(final Side SIDE) {
        properties.put("side", new SimpleObjectProperty(SIDE));
        return (B)this;
    }

    public B closingPolicy(final MaterialSpecialTabPane.TabClosingPolicy TAB_CLOSING_POLICY) {
        properties.put("closingProperty", new SimpleObjectProperty(TAB_CLOSING_POLICY));
        return (B)this;
    }

    public B tabMaxHeight(final double TAB_MAX_HEIGHT) {
        properties.put("tabMaxHeight", new SimpleDoubleProperty(TAB_MAX_HEIGHT));
        return (B)this;
    }

    public B tabMaxWidth(final double TAB_MAX_WIDTH) {
        properties.put("tabMaxWidth", new SimpleDoubleProperty(TAB_MAX_WIDTH));
        return (B)this;
    }

    public B tabMinHeight(final double TAB_MIN_HEIGHT) {
        properties.put("tabMinHeight", new SimpleDoubleProperty(TAB_MIN_HEIGHT));
        return (B)this;
    }

    public B tabMinWidth(final double TAB_MIN_WIDTH) {
        properties.put("tabMinWidth", new SimpleDoubleProperty(TAB_MIN_WIDTH));
        return (B)this;
    }

    public B maxHeight(final double MAX_HEIGHT) {
        properties.put("maxHeight", new SimpleDoubleProperty(MAX_HEIGHT));
        return (B)this;
    }

    public B maxWidth(final double MAX_WIDTH) {
        properties.put("maxWidth", new SimpleDoubleProperty(MAX_WIDTH));
        return (B)this;
    }

    public B minHeight(final double MIN_HEIGHT) {
        properties.put("minHeight", new SimpleDoubleProperty(MIN_HEIGHT));
        return (B)this;
    }

    public B minWidth(final double MIN_WIDTH) {
        properties.put("minWidth", new SimpleDoubleProperty(MIN_WIDTH));
        return (B)this;
    }

    public B tabs(final Collection<? extends Tab> TABS) {
        tabs.addAll(TABS);
        return (B)this;
    }

    public B tabs(final Tab... TABS) { return tabs(Arrays.asList(TABS)); }

    public MaterialSpecialTabPane build() {
        final MaterialSpecialTabPane CONTROL = new MaterialSpecialTabPane(tabs.toArray(new Tab[0]));
        for (String key : properties.keySet()) {
            if ("rotateGraphic".equals(key)) {
                CONTROL.setRotateGraphic(((BooleanProperty) properties.get(key)).get());
            } else if ("selectionModel".equals(key)) {
                CONTROL.setSelectionModel(((ObjectProperty<SingleSelectionModel<Tab>>) properties.get(key)).get());
            } else if ("side".equals(key)) {
                CONTROL.setSide(((ObjectProperty<Side>) properties.get(key)).get());
            } else if ("closingProperty".equals(key)) {
                CONTROL.setTabClosingPolicy(((ObjectProperty<MaterialSpecialTabPane.TabClosingPolicy>) properties.get(key)).get());
            } else if ("tabMaxHeight".equals(key)) {
                CONTROL.setTabMaxHeight(((DoubleProperty) properties.get(key)).get());
            } else if ("tabMaxWidth".equals(key)) {
                CONTROL.setTabMaxWidth(((DoubleProperty) properties.get(key)).get());
            } else if ("tabMinHeight".equals(key)) {
                CONTROL.setTabMinHeight(((DoubleProperty) properties.get(key)).get());
            } else if ("tabMinWidth".equals(key)) {
                CONTROL.setTabMinWidth(((DoubleProperty) properties.get(key)).get());
            } else if ("maxHeight".equals(key)) {
                CONTROL.setMaxHeight(((DoubleProperty) properties.get(key)).get());
            }  else if ("maxWidth".equals(key)) {
                CONTROL.setMaxWidth(((DoubleProperty) properties.get(key)).get());
            }  else if ("minHeight".equals(key)) {
                CONTROL.setMinHeight(((DoubleProperty) properties.get(key)).get());
            }  else if ("maxWidth".equals(key)) {
                CONTROL.setMinWidth(((DoubleProperty) properties.get(key)).get());
            }
        }
        return CONTROL;
    }
}
