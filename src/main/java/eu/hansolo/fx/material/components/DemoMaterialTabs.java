/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.components;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import eu.hansolo.fx.material.Person;
import eu.hansolo.fx.material.components.MaterialDesignColorScheme.MaterialDesignAccentColor;
import eu.hansolo.fx.material.components.MaterialDesignColorScheme.MaterialDesignColor;
import eu.hansolo.fx.material.currencyfield.CurrencyField;
import eu.hansolo.fx.material.numberfield.NumberField;
import eu.hansolo.fx.material.omnibox.OmniBoxSearchField;
import eu.hansolo.fx.material.tableview.EditableCurrencyTableCell;
import eu.hansolo.fx.material.tableview.EditableDateTableCell;
import eu.hansolo.fx.material.tableview.EditableTableCell;
import eu.hansolo.fx.material.tableview.SelectableTableCell;
import eu.hansolo.fx.material.tableview.SelectableTableColumn;
import eu.hansolo.fx.material.tools.TabPaneDetacher;
import javafx.application.Application;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.DoublePropertyBase;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Accordion;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.scene.Scene;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.NumberStringConverter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;


/**
 * User: hansolo
 * Date: 28.04.16
 * Time: 14:54
 */
public class DemoMaterialTabs extends Application {
    private enum radioEnum { RADIO1, RADIO2, RADIO3, RADIO4, RADIO5 };
    private MaterialSpecialTabPane tabPane;

    @Override public void init() {
        // Tab 1
        StackPane pane1 = new StackPane(createTableView());
        pane1.setPadding(new Insets(20));
        pane1.setPrefSize(800, 400);

        Tab tab1 = new Tab("WEB", pane1);
        tab1.setClosable(false);

        // Tab 2
        MaterialTextField firstName = new MaterialTextField();
        firstName.setPromptText("First Name");
        firstName.setHintText("Enter your first name");

        MaterialTextField lastName = new MaterialTextField();
        lastName.setPromptText("Last Name");
        lastName.setHintText("Enter your last name");

        MaterialTextField email = new MaterialTextField();
        email.setPromptText("E-Mail");
        email.setHintText("Enter your e-mail address");

        GridPane grid = new GridPane();
        grid.setVgap(24);
        grid.setPadding(new Insets(14));
        grid.add(firstName, 0, 0);
        grid.add(lastName, 0, 1);
        grid.add(email, 0, 2);

        MaterialTitledPane titledPane1 = new MaterialTitledPane("Title 1", grid, true);
        titledPane1.setOnButtonAction(event -> System.out.println("Hit me baby one more time..."));

        ProgressBar progressBar = new ProgressBar();
        progressBar.setProgress(0.5);
        progressBar.setPrefWidth(200);
        StackPane progressPane = new StackPane(progressBar);
        progressPane.setPadding(new Insets(14));
        MaterialTitledPane titledPane2 = new MaterialTitledPane("Title 2", progressPane, true);

        Accordion accordion = new Accordion(titledPane1, titledPane2);

        VBox pane2 = new VBox(accordion);
        pane2.setPrefSize(600, 400);

        FontAwesomeIcon tabIcon = new FontAwesomeIcon();
        tabIcon.setGlyphName(FontAwesomeIcons.AMBULANCE.name());
        tabIcon.getStyleClass().setAll("tab-icon");

        Tab tab2 = new Tab("", pane2);
        tab2.setClosable(false);
        tab2.setGraphic(tabIcon);

        // Tab 3
        NumberField numberField1 = new NumberField();
        numberField1.setPercentageMode(true);
        NumberField        numberField2       = new NumberField();

        OmniBoxSearchField omniBoxSearchField = new OmniBoxSearchField();
        MaterialProgressIndicator progressIndicator = new MaterialProgressIndicator();
        progressIndicator.setProgress(0.3);

        CurrencyField currencyField = new CurrencyField();
        currencyField.setValue(new BigDecimal(1000));

        CurrencyField currencyField2 = new CurrencyField();
        currencyField2.setValue(new BigDecimal(2000));

        MaterialCurrencyField materialCurrencyField3 = new MaterialCurrencyField();
        materialCurrencyField3.setEditable(false);


        ObjectBinding<BigDecimal> sum = new ObjectBinding<BigDecimal>() {
            { bind(currencyField.valueProperty(), currencyField2.valueProperty()); }
            @Override protected BigDecimal computeValue() {
                return currencyField.valueProperty().get().add(currencyField2.valueProperty().get());
            }
        };

        materialCurrencyField3.valueProperty().bind(sum);

        omniBoxSearchField.focusedProperty().addListener((o, ov, nv) -> {
            progressIndicator.setProgress(nv ? -1 : 0);
        });

        VBox pane3 = new VBox(numberField1, numberField2, omniBoxSearchField, progressIndicator, currencyField, currencyField2, materialCurrencyField3);
        pane3.setSpacing(20);
        pane3.setPadding(new Insets(20));
        pane3.setPrefSize(600, 400);

        Tab tab3 = new Tab("VIDEO", pane3);
        tab3.setClosable(false);

        // Tab 4
        MaterialOmniBoxTextField materialOmniBoxTextField = new MaterialOmniBoxTextField();
        materialOmniBoxTextField.setPromptText("Name");

        MaterialTextField materialTextField = new MaterialTextField();
        materialTextField.setPromptText("Check");

        VBox pane4 = new VBox(materialOmniBoxTextField, materialTextField);
        pane4.setPadding(new Insets(24));
        pane4.setSpacing(24);
        pane4.setPrefSize(600, 400);

        Tab tab4 = new Tab("OmniBox", pane4);
        tab4.setClosable(false);

        // Tab 5
        MaterialCard card = new MaterialCard("Title", createTableView());

        StackPane pane5 = new StackPane(card);
        pane5.setPadding(new Insets(20));

        Tab tab5 = new Tab("Card", pane5);
        tab5.setClosable(false);

        // Tab 6
        RadioButton        radio1             = new RadioButton("Radio 1");
        RadioButton        radio2             = new RadioButton("Radio 2");
        RadioButton        radio3             = new RadioButton("Radio 3");
        RadioButton        radio4             = new RadioButton("Radio 4");
        RadioButton        radio5             = new RadioButton("Radio 5");
        MaterialRadioGroup materialRadioGroup = new MaterialRadioGroup("RadioGroup", true, radio1, radio2, radio3, radio4, radio5);

        MaterialTextArea textArea = new MaterialTextArea("This is an unbelievable TextArea...yeah", "Title", true);
        //textArea.setEditable(false);

        VBox pane6 = new VBox(materialRadioGroup, textArea);
        //pane6.setSpacing();
        pane6.setPadding(new Insets(10));

        Tab tab6 = new Tab("RadioGroup", pane6);
        tab6.setClosable(false);

        // Tab 7
        MaterialTableCard tableCard = new MaterialTableCard("Title", createTableView());
        //MaterialTableCard tableCard = new MaterialTableCard("Title");
        //tableCard.setTableView(createTableView());

        StackPane pane7 = new StackPane(tableCard);
        pane7.setPadding(new Insets(20));

        Tab tab7 = new Tab("TableCard", pane7);
        tab7.setClosable(false);

        // Tab 8
        ChartData data = new ChartData(new Record("Grapefruit", 13),
                                       new Record("Oranges", 25),
                                       new Record("Plums", 10),
                                       new Record("Pears", 22),
                                       new Record("Apples", 30));

        XYChart.Series series = new Series();
        series.setName("2015");
        series.getData().setAll(data.getData());

        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis   yAxis = new NumberAxis();
        BarChart<String, Number> chart = new BarChart<>(xAxis, yAxis);
        chart.setTitle("Fruits");
        chart.getData().add(series);

        MaterialTableView tableView = new MaterialTableView(data.getRecords());
        tableView.setEditable(true);
        tableView.setPrefHeight(200);

        SelectableTableColumn nameColumn = new SelectableTableColumn("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        nameColumn.setCellFactory(p -> new SelectableTableCell<>());
        nameColumn.setPrefWidth(100);

        Callback<TableColumn<Record, Number>, TableCell<Record, Number>> editingCellFactory = (TableColumn<Record, Number> p) -> new EditableTableCell<>(new NumberStringConverter());
        SelectableTableColumn<Record, Number> dataColumn = new SelectableTableColumn<>("Data");
        dataColumn.setCellValueFactory(p -> p.getValue().valueProperty());
        dataColumn.setCellFactory(editingCellFactory);
        dataColumn.setPrefWidth(100);

        tableView.getColumns().addAll(nameColumn, dataColumn);

        VBox pane8 = new VBox(chart, tableView);
        pane8.setSpacing(20);
        pane8.setPadding(new Insets(20));

        Tab tab8 = new Tab("TableChartCard", pane8);
        tab8.setClosable(false);

        // Tab 9
        MaterialRadioGroup materialConvenientRadioGroup = new MaterialRadioGroup("Title");
        materialConvenientRadioGroup.setRadios(radioEnum.class, radioEnum.RADIO3);

        StackPane pane9 = new StackPane(materialConvenientRadioGroup);
        pane9.setPadding(new Insets(10));

        Tab tab9 = new Tab("ConvenientRadioGroup", pane9);
        tab9.setClosable(false);

        //tabPane = new MaterialTabPane(tab1, tab2, tab3, tab4, tab5, tab6, tab7, tab8, tab9);
        tabPane = new MaterialSpecialTabPane();
        tabPane.getTabs().setAll(tab1, tab2, tab3, tab4, tab5, tab6, tab7, tab8, tab9);

        TabPaneDetacher.create()
                       .styleSheets(DemoMaterial.class.getResource("../styles.css").toExternalForm())
                       .styles(MaterialDesignColor.UBS_GREY.STYLE_CLASS, MaterialDesignAccentColor.ORANGE.STYLE_CLASS)
                       .makeTabsDetachable(tabPane);

        //card.setButtonBottom4Text("HELLO");
        //card.setIconButtonButtom4(FontAwesomeIcons.ADJUST);

        //final  StackPane header = (StackPane) tabPane.lookup(".tab-header-area");
        //header.setEffect(new DropShadow(BlurType.TWO_PASS_BOX, Color.rgb(0, 0, 0, 0.65), 4, 0, 0, 2));
    }

    @Override public void start(Stage stage) {
        HBox topPane = new HBox();
        topPane.getStyleClass().add("window-decoration");
        topPane.setEffect(new DropShadow(BlurType.TWO_PASS_BOX, Color.rgb(0, 0, 0, 0.65), 4, 0, 0, 2));

        BorderPane pane = new BorderPane();
        pane.setTop(topPane);
        pane.setCenter(tabPane);
        MaterialDesignColorScheme.applyTo(pane, MaterialDesignColor.UBS_GREY, MaterialDesignAccentColor.ORANGE);

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(DemoMaterial.class.getResource("../styles.css").toExternalForm());

        //stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("TabPane");
        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    private MaterialTableView createTableView() {
        Callback<TableColumn<Person, String>, TableCell<Person, String>> editingCellFactory = (TableColumn<Person, String> p) -> new EditableTableCell<>(new DefaultStringConverter());

        SelectableTableColumn<Person, String> firstNameColumn = new SelectableTableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(p -> p.getValue().firstNameProperty());
        firstNameColumn.setCellFactory(p -> new SelectableTableCell<>());

        SelectableTableColumn<Person, String> lastNameColumn = new SelectableTableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(p -> p.getValue().lastNameProperty());
        lastNameColumn.setCellFactory(p -> new SelectableTableCell<>());
        //lastNameColumn.setOnEditCommit(event -> event.getTableView().getItems().get(event.getTablePosition().getRow()).setInfo(event.getNewValue().toString()));

        SelectableTableColumn<Person, String> infoColumn = new SelectableTableColumn<>("Info");
        infoColumn.setCellValueFactory(p -> p.getValue().infoProperty());
        infoColumn.setCellFactory(editingCellFactory);
        //infoColumn.setOnEditCommit(event -> event.getTableView().getItems().get(event.getTablePosition().getRow()).setLastName(event.getNewValue().toString()));

        SelectableTableColumn<Person, LocalDate> dateColumn = new SelectableTableColumn<>("Date");
        dateColumn.setCellValueFactory(p -> p.getValue().birthdayProperty());
        dateColumn.setCellFactory(p -> new EditableDateTableCell<>());

        SelectableTableColumn<Person, BigDecimal> incomeColumn = new SelectableTableColumn<>("Income");
        incomeColumn.setCellValueFactory(p -> p.getValue().incomeProperty());
        incomeColumn.setCellFactory(p -> new EditableCurrencyTableCell<>());

        MaterialTableView<Person> tableView = new MaterialTableView<>(FXCollections.observableArrayList(
            new Person("Jonathan", "Giles", "TeamLead JavaFX Controls"),
            new Person("Richard", "Bair", "PM IoT"),
            new Person("Jasper", "Potts", "PM IoT Devices"),
            new Person("Gerrit", "Grunwald", "Java Evangelist"),
            new Person("Simon", "Ritter", "Head of Evangelism"),
            new Person("Stephen", "Chin", "OTN Community Manager"),
            new Person("James", "Weaver", "Java Evangelist"),
            new Person("Angela", "Cacaido", "Java Evangelist"),
            new Person("Michele", "DiSerio", "Team Manager"),
            new Person("Mark", "Heckler", "Java Evangelist")));
        tableView.getColumns().addAll(firstNameColumn, lastNameColumn, infoColumn, dateColumn, incomeColumn);
        tableView.setEditable(true);

        tableView.getFocusModel().focusedCellProperty().addListener(o -> System.out.println("Focused Cell: " + tableView.getFocusModel().getFocusedCell().getColumn() + ", " + tableView.getFocusModel().getFocusedCell().getRow()));

        tableView.setPrefSize(600, 400);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        return tableView;
    }

    public static void main(String[] args) {
        launch(args);
    }


    // ******************** Internal Classes **********************************
    public class Record {
        private StringProperty name;
        private DoubleProperty value;


        // ******************** Constructors **********************************
        public Record(final String NAME, final double VALUE) {
            name = new StringPropertyBase(NAME) {
                @Override public Object getBean() { return Record.this; }
                @Override public String getName() { return "name"; }
            };
            value = new DoublePropertyBase(VALUE) {
                @Override public Object getBean() { return Record.this; }
                @Override public String getName() { return "value"; }
            };
        }


        // ******************** Methods ***************************************
        public String getName() { return name.get(); }
        public void setName(final String NAME) { name.set(NAME); }
        public StringProperty nameProperty() { return name; }

        public double getValue() { return value.get(); }
        public void setValue(final double VALUE) { value.set(VALUE); }
        public DoubleProperty valueProperty() { return value; }
    }

    public class ChartData {
        private ObservableList<Record>                       records;
        private ObservableList<XYChart.Data<String, Number>> data;


        // ******************** Constructors **********************************
        public ChartData() {
            this(new Record[]{});
        }
        public ChartData(final Record... RECORDS) {
            records = FXCollections.observableArrayList();
            data    = FXCollections.observableArrayList();
            Arrays.stream(RECORDS).forEach(r -> addRecord(r));
        }


        // ******************** Methods ***************************************
        public ObservableList<Record> getRecords() { return records; }
        public ObservableList<XYChart.Data<String, Number>> getData() { return data; }

        public void addRecord(final Record RECORD) {
            if (records.contains(RECORD)) return;
            records.add(RECORD);
            Data<String, Number> d = new Data();
            d.XValueProperty().bind(RECORD.nameProperty());
            d.YValueProperty().bind(RECORD.valueProperty());
            data.add(d);
        }
        public void removeRecord(final Record RECORD) {
            int i = records.indexOf(RECORD);
            if (i > -1) {
                for (Data<String, Number> d : data) {
                    if (d.getXValue().equals(RECORD.getName()) && Double.compare(RECORD.getValue(), (Double) d.getYValue()) == 0) {
                        d.XValueProperty().unbind();
                        d.YValueProperty().unbind();
                        data.remove(d);
                        break;
                    }
                }
                records.remove(i);
            }
        }
        public void updateRecord(final Record RECORD, final double VALUE) {
            int i = records.indexOf(RECORD);
            if (i > -1) records.get(i).setValue(VALUE);
        }
    }
}
