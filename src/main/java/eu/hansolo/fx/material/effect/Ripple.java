/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.effect;

import com.sun.javafx.css.converters.ColorConverter;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.beans.property.ObjectProperty;
import javafx.css.CssMetaData;
import javafx.css.Styleable;
import javafx.css.StyleableObjectProperty;
import javafx.css.StyleableProperty;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Control;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by hansolo on 28.04.16.
 */
public class Ripple extends Region {
    public  final                 Circle             RIPPLE_CIRCLE        = new Circle(0.1);
    private static final          Color              DEFAULT_RIPPLE_COLOR = Color.rgb(0, 0, 0, 0.11);
    private static final          double             RIPPLE_MAX_RADIUS    = 300;
    private final                 Timeline           TIMELINE             = new Timeline();
    private final                 FadeTransition     FADE_TRANSITION      = new FadeTransition();
    private final                 ParallelTransition PARALLEL_TRANSITION  = new ParallelTransition();
    private final                 Rectangle          CLIP_RECT            = new Rectangle();
    private ObjectProperty<Color> rippleColor;


    // ******************** Constructors **************************************
    public Ripple() {
        super();
        getStylesheets().add(Ripple.class.getResource("/eu/hansolo/fx/material/styles.css").toExternalForm());
        //getStylesheets().add(Ripple.class.getResource("/eu/hansolo/fx/material/components/material-components.css").toExternalForm());
        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        RIPPLE_CIRCLE.setOpacity(0.0);
        RIPPLE_CIRCLE.getStyleClass().addAll("ripple-circle");
        RIPPLE_CIRCLE.setMouseTransparent(true);

        getChildren().add(RIPPLE_CIRCLE);
    }

    private void registerListeners() {
        parentProperty().addListener(o -> {
            if (null == getParent()) return;
            final Parent PARENT = getParent();
            if (PARENT instanceof Region) {
                final Region REGION = (Region) PARENT;
                REGION.setOnMousePressed(evt -> {
                    RIPPLE_CIRCLE.setCenterX(evt.getX());
                    RIPPLE_CIRCLE.setCenterY(evt.getY());

                    double width  = REGION.getWidth();
                    double height = REGION.getHeight();
                    CLIP_RECT.setWidth(width);
                    CLIP_RECT.setHeight(height);
                    try {
                        final double CORNER_RADIUS = REGION.getBackground().getFills().get(0).getRadii().getTopLeftHorizontalRadius();
                        CLIP_RECT.setArcHeight(CORNER_RADIUS);
                        CLIP_RECT.setArcWidth(CORNER_RADIUS);
                        RIPPLE_CIRCLE.setClip(CLIP_RECT);
                    } catch (Exception e) {}
                    final double   RADIUS   = Math.min(Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2)), RIPPLE_MAX_RADIUS) * 1.1 + 5;
                    final Duration DURATION = new Duration((1.1 - 0.2 * (RADIUS / RIPPLE_MAX_RADIUS)) * 500);
                    final KeyFrame KF_0     = new KeyFrame(DURATION, new KeyValue(RIPPLE_CIRCLE.radiusProperty(), RADIUS, Interpolator.EASE_OUT));

                    TIMELINE.getKeyFrames().setAll(KF_0);

                    FADE_TRANSITION.setDuration(DURATION);
                    FADE_TRANSITION.setNode(RIPPLE_CIRCLE);
                    FADE_TRANSITION.setInterpolator(Interpolator.EASE_OUT);
                    FADE_TRANSITION.setFromValue(1.0);
                    FADE_TRANSITION.setToValue(0.0);

                    PARALLEL_TRANSITION.stop();
                    if (null != PARALLEL_TRANSITION.getOnFinished()) PARALLEL_TRANSITION.getOnFinished().handle(null);
                    PARALLEL_TRANSITION.getChildren().setAll(TIMELINE, FADE_TRANSITION);

                    PARALLEL_TRANSITION.setOnFinished(evt1 -> {
                        RIPPLE_CIRCLE.setOpacity(0.0);
                        RIPPLE_CIRCLE.setRadius(0.1);
                    });
                    PARALLEL_TRANSITION.playFromStart();
                });
            }
        });
    }


    // ******************** CSS Stylable Properties ***************************
    public Color getRippleColor() { return null == rippleColor ? DEFAULT_RIPPLE_COLOR : rippleColor.get(); }
    public void setRippleColor(final Color COLOR) { rippleColorProperty().set(COLOR); }
    public ObjectProperty<Color> rippleColorProperty() {
        if (null == rippleColor) {
            rippleColor = new StyleableObjectProperty<Color>(DEFAULT_RIPPLE_COLOR) {
                @Override public CssMetaData getCssMetaData() { return Ripple.StyleableProperties.RIPPLE_COLOR; }
                @Override public Object getBean() { return Ripple.this; }
                @Override public String getName() { return "rippleColor"; }
            };
        }
        return rippleColor;
    }

    public Duration getDuration() {
        final Parent PARENT = getParent();
        if (null == PARENT) return new Duration(0);

        if (PARENT instanceof Region) {
            final Region REGION = (Region) PARENT;
            final double RADIUS = Math.min(Math.sqrt(Math.pow(REGION.getWidth(), 2) + Math.pow(REGION.getHeight(), 2)), RIPPLE_MAX_RADIUS) * 1.1 + 5;
            return new Duration((1.1 - 0.2 * (RADIUS / RIPPLE_MAX_RADIUS)) * 500);
        } else {
            return new Duration(0);
        }
    }


    // ******************** Style related *************************************
    private static class StyleableProperties {
        private static final CssMetaData<Ripple, Color> RIPPLE_COLOR =
            new CssMetaData<Ripple, Color>("-ripple-color", ColorConverter.getInstance(), DEFAULT_RIPPLE_COLOR) {
                @Override public boolean isSettable(final Ripple BUTTON) { return null == BUTTON.rippleColor || !BUTTON.rippleColor.isBound(); }
                @Override public StyleableProperty<Color> getStyleableProperty(final Ripple BUTTON) { return (StyleableProperty) BUTTON.rippleColorProperty(); }
                @Override public Color getInitialValue(final Ripple BUTTON) { return BUTTON.getRippleColor(); }
            };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
        static {
            final List<CssMetaData<? extends Styleable, ?>> styleables = new ArrayList<>(Control.getClassCssMetaData());
            Collections.addAll(styleables, RIPPLE_COLOR);
            STYLEABLES = Collections.unmodifiableList(styleables);
        }
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() { return Ripple.StyleableProperties.STYLEABLES; }
}
