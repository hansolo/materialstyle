package eu.hansolo.fx.material;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.math.BigDecimal;
import java.time.LocalDate;


/**
 * Created by hansolo on 13.04.16.
 */
public class Person {
    private StringProperty             firstName;
    private StringProperty             lastName;
    private StringProperty             street;
    private IntegerProperty            postalCode;
    private StringProperty             city;
    private ObjectProperty<LocalDate>  birthday;
    private StringProperty             info;
    private ObjectProperty<BigDecimal> income;


    public Person() {
        this(null, null, null);
    }
    public Person(final String FIRST_NAME, final String LAST_NAME, final String INFO) {
        firstName  = new SimpleStringProperty(FIRST_NAME);
        lastName   = new SimpleStringProperty(LAST_NAME);
        street     = new SimpleStringProperty("some street");
        postalCode = new SimpleIntegerProperty(1234);
        city       = new SimpleStringProperty("some city");
        birthday   = new SimpleObjectProperty<>(LocalDate.of(1999, 2, 21));
        info       = new SimpleStringProperty(INFO);
        income     = new SimpleObjectProperty<>(BigDecimal.ZERO);
    }

    public String getFirstName() { return firstName.get(); }
    public void setFirstName(final String FIRST_NAME) { firstName.set(FIRST_NAME); }
    public StringProperty firstNameProperty() { return firstName; }

    public String getLastName() { return lastName.get(); }
    public void setLastName(final String LAST_NAME) { lastName.set(LAST_NAME); }
    public StringProperty lastNameProperty() { return lastName; }

    public String getStreet() { return street.get(); }
    public void setStreet(final String STREET) { street.set(STREET); }
    public StringProperty streetProperty() { return street; }

    public int getPostalCode() { return postalCode.get(); }
    public void setPostalCode(final int POSTAL_CODE) { postalCode.set(POSTAL_CODE); }
    public IntegerProperty postalCodeProperty() { return postalCode; }

    public String getCity() { return city.get(); }
    public void setCity(final String CITY) { city.set(CITY); }
    public StringProperty cityProperty() { return city; }

    public LocalDate getBirthday() { return birthday.get(); }
    public void setBirthday(final LocalDate BIRTHDAY) { birthday.set(BIRTHDAY); }
    public ObjectProperty<LocalDate> birthdayProperty() { return birthday; }

    public String getInfo() { return info.get(); }
    public void setInfo(final String INFO) { info.set(INFO); }
    public StringProperty infoProperty() { return info; }

    public BigDecimal getIncome() { return income.get(); }
    public void setIncome(final BigDecimal INCOME) { income.set(INCOME); }
    public ObjectProperty<BigDecimal> incomeProperty() { return income; }
}
