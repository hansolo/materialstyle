package eu.hansolo.fx.material;

import eu.hansolo.fx.material.components.MaterialButton;
import eu.hansolo.fx.material.components.MaterialButton.MaterialButtonType;
import eu.hansolo.fx.material.components.MaterialComboBox;
import eu.hansolo.fx.material.components.MaterialDatePicker;
import eu.hansolo.fx.material.components.MaterialDesignColorScheme;
import eu.hansolo.fx.material.components.MaterialDesignColorScheme.MaterialDesignAccentColor;
import eu.hansolo.fx.material.components.MaterialDesignColorScheme.MaterialDesignColor;
import eu.hansolo.fx.material.components.MaterialSwitch;
import eu.hansolo.fx.material.currencyfield.CurrencyField;
import eu.hansolo.fx.material.components.MaterialLabel;
import eu.hansolo.fx.material.omnibox.MaterialOmniBoxSearchField;
import eu.hansolo.fx.material.components.MaterialProgressIndicator;
import eu.hansolo.fx.material.controlsfx.RangeSlider;
import eu.hansolo.fx.material.tableview.EditableTableCell;
import eu.hansolo.fx.material.components.MaterialCurrencyField;
import eu.hansolo.fx.material.components.MaterialInputField.Type;
import eu.hansolo.fx.material.components.MaterialPasswordField;
import eu.hansolo.fx.material.components.MaterialTextField;
import eu.hansolo.fx.material.components.MaterialInputFieldBuilder;
import javafx.application.Application;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Locale;


public class Main extends Application {
    private static final boolean REQUIRED     = true;
    private static final boolean NOT_REQUIRED = false;

    private BorderPane                 pane;
    private MaterialOmniBoxSearchField omniBoxSearchField;
    private VBox                       centerPane;
    private Button                     button1;
    private Button                     button2;
    private Button                     button3;
    private Button                     roundButton1;
    private Button                     raisedButton1;
    private Button                     raisedButton2;
    private Button                     raisedButton3;
    private Button                     floatingButton1;
    private Button                     floatingButton2;
    private Button                     floatingButton3;
    private MaterialButton             materialButton1;
    private MaterialButton             materialButton2;
    private CheckBox                   checkBox1;
    private CheckBox                   checkBox2;
    private CheckBox                   checkBox3;
    private CheckBox                   checkBox4;
    private MaterialSwitch             switch1;
    private MaterialSwitch             switch2;
    private MaterialSwitch             switch3;
    private MaterialSwitch             switch4;
    private RadioButton                radioButton1;
    private RadioButton                radioButton2;
    private RadioButton                radioButton3;
    private TextField                  textField1;
    private TextField                  textField2;
    private TextField                  textField3;
    private TextField                  textField4;
    private MaterialTextField          materialTextField1;
    private MaterialTextField          materialTextField2;
    private MaterialTextField          materialTextField3;
    private MaterialTextField          materialTextField4;
    private MaterialTextField          materialTextField5;
    private ComboBox<String>           comboBox1;
    private ComboBox<String>           comboBox2;
    private ComboBox<String>           comboBox3;
    private MaterialProgressIndicator  progressIndicator;
    private TableView<Person>          tableView;
    private TreeTableView<String>      treeTable;
    private ListView<String>           listView;
    private RangeSlider                slider;
    private DatePicker                 datePicker1;
    private DatePicker                 datePicker2;
    private CurrencyField              currencyField;
    private MaterialCurrencyField      materialCurrencyField;
    private MaterialPasswordField      materialPasswordField;
    private MaterialLabel              materialLabel;
    private MaterialComboBox<String>   materialComboBox1;
    private MaterialComboBox<String>   materialComboBox2;
    private MaterialDatePicker         materialDatePicker;
    private MaterialDatePicker         materialDatePicker1;
    private DatePicker                 datePicker;


    @Override public void init() {
        // Setup top bar
        Line line = new Line(0, 0, 176, 0);
        line.setTranslateX(-16);
        line.setVisible(false);
        line.getStyleClass().add("underline");

        omniBoxSearchField = new MaterialOmniBoxSearchField();
        omniBoxSearchField.setPromptText("Search");
        omniBoxSearchField.translateXProperty().addListener(o -> line.setVisible(omniBoxSearchField.getTranslateX() < 0));

        AnchorPane.setTopAnchor(omniBoxSearchField, 15d);
        AnchorPane.setRightAnchor(omniBoxSearchField, 0d);

        AnchorPane topPane = new AnchorPane(omniBoxSearchField);
        topPane.getStyleClass().add("top-pane");
        topPane.setPrefHeight(63);


        // Setup center pane
        button1      = createButton("START", false); button1.setOnMouseClicked(e -> progressIndicator.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS));
        button2      = createButton("STOP", false); button2.setOnMouseClicked(e -> progressIndicator.setProgress(0));
        button3      = createButton("TEST", true);
        roundButton1 = createButton("ROUND", false); roundButton1.getStyleClass().add("round-button");

        materialButton1 = new MaterialButton("Press");

        materialButton2 = new MaterialButton();
        materialButton2.setType(MaterialButtonType.FLOATING);

        raisedButton1 = createRaisedButton("START", false);
        raisedButton2 = createRaisedButton("STOP", false);
        raisedButton3 = createRaisedButton("TEST", true);

        floatingButton1 = createFloatingButton("+", false);
        floatingButton2 = createFloatingButton("+", false);
        floatingButton3 = createFloatingButton("+", true);

        checkBox1    = createCheckBox("SELECT ME", false);
        checkBox2    = createCheckBox("NOT ME", false);
        checkBox3    = createCheckBox("PLEASE ME", true);
        checkBox4    = createCheckBox("DUNNO", false);
        checkBox2.setOnMouseClicked(evt -> { if (checkBox2.isSelected()) checkBox4.setIndeterminate(true); });

        switch1      = createSwitch("SELECT ME", false);
        switch2      = createSwitch("NOT ME", false);
        switch3      = createSwitch("NOT ME", true);
        switch4      = createSwitch("DUNNO", false);
        switch2.setOnMouseClicked(evt -> { if (switch2.isSelected()) switch4.setIndeterminate(true); });

        radioButton1 = createRatioButton("CHOICE 1", false);
        radioButton2 = createRatioButton("CHOICE 2", false);
        radioButton3 = createRatioButton("CHOICE 3", true);
        ToggleGroup toggleGroup = new ToggleGroup();
        toggleGroup.getToggles().addAll(radioButton1, radioButton2, radioButton3);

        textField1 = createTextField("", "Vorname", true, false);
        textField2 = createTextField("Grunwald", "Name", false, false);
        textField3 = createTextField("Westfalenstr.", "Strasse", true, false);
        textField4 = createTextField("46", "Alter", false, true);

        HBox textField1WithLabel = createLabelWithControlBox("Vorname", textField1);
        HBox textField2WithLabel = createLabelWithControlBox("Name", textField2);
        HBox textField3WithLabel = createLabelWithControlBox("Strasse", textField3);
        HBox textField4WithLabel = createLabelWithControlBox("Alter", textField4);

        materialTextField1 = (MaterialTextField) MaterialInputFieldBuilder.create().promptText("Vorname").hintText("Bitte Vorname eintragen").build();
        materialTextField2 = (MaterialTextField) MaterialInputFieldBuilder.create().promptText("Name").text("").readOnly(true).build();
        materialTextField3 = (MaterialTextField) MaterialInputFieldBuilder.create().text("Westfalenstr.").promptText("Strasse").required(true).build();
        materialTextField4 = (MaterialTextField) MaterialInputFieldBuilder.create().text("46").promptText("Alter").disabled(true).build();

        comboBox1          = createComboBox(false, false, "Fruit", "Äpfel", "Birnen", "Kirschen");
        comboBox2          = createComboBox(false, true, "Fruit", "Äpfel", "Birnen", "Kirschen");
        comboBox3          = createComboBox(true, false,  "Fruit", "Äpfel", "Birnen", "Kirschen");
        materialTextField5 = (MaterialTextField) MaterialInputFieldBuilder.create().promptText("Test").build();
        datePicker2        = new DatePicker(LocalDate.now());

        progressIndicator = new MaterialProgressIndicator();

        ObservableList<Person> persons = FXCollections.observableArrayList();
        persons.addAll(new Person("Gerrit", "Grunwald", "-"),
                       new Person("Sandra", "Grunwald", "-"),
                       new Person("Lilli", "Grunwald", "-"),
                       new Person("Anton", "Grunwald", "-"));

        //tableView = new TableView<>(persons);
        tableView = createTableView();
        tableView.setPrefHeight(80);

        /*
        TableColumn firstNameCol = new TableColumn("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory("firstName"));
        TableColumn lastNameCol = new TableColumn("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory("lastName"));
        tableView.getColumns().setAll(firstNameCol, lastNameCol);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override public void changed(ObservableValue<? extends Number> o, Number oldRow, Number newRow) {
                System.out.println("selected row: " + newRow);
            }
        });
        */


        TreeItem<String> childNode1 = new TreeItem<>("Node 1");
        TreeItem<String> childNode2 = new TreeItem<>("Node 2");
        TreeItem<String> childNode3 = new TreeItem<>("Node 3");
        TreeItem<String> root = new TreeItem<>("Root");
        root.setExpanded(true);
        root.getChildren().setAll(childNode1, childNode2, childNode3);
        TreeTableColumn<String, String> column = new TreeTableColumn<>("Column");
        column.setPrefWidth(150);
        column.setCellValueFactory((CellDataFeatures<String, String> p) -> new ReadOnlyStringWrapper(p.getValue().getValue()));
        treeTable = new TreeTableView<>(root);
        treeTable.getColumns().add(column);
        treeTable.setPrefHeight(80);

        ObservableList<String> items =FXCollections.observableArrayList ("A", "B", "C", "D");
        listView = new ListView<>();
        listView.setItems(items);
        listView.setPrefHeight(80);

        slider = new RangeSlider(0, 100, 0, 50, true);
        slider.setLowThumbVisible(false);

        datePicker1 = new DatePicker();
        datePicker1.setShowWeekNumbers(false);

        currencyField = new CurrencyField(new BigDecimal(5.5), "Betrag", "Bitte Betrag eingeben", Locale.US, 2);

        materialCurrencyField = (MaterialCurrencyField) MaterialInputFieldBuilder.create()
                                                                                 .type(Type.CURRENCY_FIELD)
                                                                                 .promptText("Gehalt")
                                                                                 .hintText("Bitte Betrag eingeben")
                                                                                 .required(true)
                                                                                 .decimals(2)
                                                                                 .build();

        materialPasswordField = (MaterialPasswordField) MaterialInputFieldBuilder.create()
                                                                                 .type(Type.PASSWORD_FIELD)
                                                                                 .promptText("Password")
                                                                                 .hintText("Bitte nicht den Namen ;)")
                                                                                 .build();

        materialLabel = new MaterialLabel("Label", "Title");

        materialComboBox1 = new MaterialComboBox<>("Früchte", false);
        materialComboBox1.getItems().addAll("Apfel", "Birne", "Kirsche", "Melone", "Ananas");
        materialComboBox1.setHintText("Please select fruit");
        materialComboBox1.setHintTextAlwaysVisible(true);

        materialComboBox2 = new MaterialComboBox<>("Früchte", false);
        materialComboBox2.setEditable(true);
        materialComboBox2.getItems().addAll("Apfel", "Birne", "Kirsche", "Melone", "Ananas");
        materialComboBox2.setHintText("Please select fruit");

        materialDatePicker  = new MaterialDatePicker("Today", false);
        materialDatePicker.setHintText("Please select date");
        materialDatePicker.setHintTextAlwaysVisible(true);

        materialDatePicker1 = new MaterialDatePicker("ReadOnly", false);
        materialDatePicker1.setEditable(false);
        materialDatePicker1.setHintText("Please select date");

        datePicker = new DatePicker(LocalDate.now());

        // Setup layout
        HBox buttons            = createHBox(button1, button2, button3, progressIndicator, datePicker1, roundButton1);
        HBox raisedButtons      = createHBox(materialButton1, materialButton2, raisedButton1, raisedButton2, raisedButton3);
        HBox floatingButtons    = createHBox(floatingButton1, floatingButton2, floatingButton3);
        HBox checkBoxes         = createHBox(checkBox1, checkBox2, checkBox3, checkBox4);
        HBox switches           = createHBox(switch1, switch2, switch3, switch4, datePicker, materialDatePicker, materialDatePicker1);
        HBox radioButtons       = createHBox(radioButton1, radioButton2, radioButton3);
        HBox textFields         = createHBox(textField1WithLabel, textField2WithLabel, textField3WithLabel, textField4WithLabel);
        HBox materialTextFields = createHBox(materialTextField1, materialTextField2, materialTextField3, materialTextField4);
        HBox comboBoxes         = createHBox(comboBox1, comboBox2, datePicker2, comboBox3, materialTextField5, materialComboBox1, materialComboBox2);
        HBox currencyFields     = createHBox(currencyField, materialCurrencyField, materialPasswordField, materialLabel);


        MaterialTextField encapsulatedMaterialTextField = new MaterialTextField("Test Text");
        encapsulatedMaterialTextField.setPromptText("Test Prompt");
        encapsulatedMaterialTextField.setHintText("Test Hint");
        BorderPane borderPane = new BorderPane(encapsulatedMaterialTextField);

        centerPane = new VBox(buttons,
                              raisedButtons,
                              floatingButtons,
                              checkBoxes,
                              switches,
                              radioButtons,
                              textFields,
                              materialTextFields,
                              comboBoxes,
                              tableView,
                              treeTable,
                              listView,
                              slider,
                              currencyFields,
                              borderPane);
        centerPane.setSpacing(20);
        centerPane.setPadding(new Insets(10));

        // Setup main view
        pane = new BorderPane();
        pane.setTop(topPane);
        pane.setCenter(centerPane);
        MaterialDesignColorScheme.applyTo(pane, MaterialDesignColor.BLUE, MaterialDesignAccentColor.PINK);
        pane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    @Override public void start(Stage stage) {
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(Main.class.getResource("styles.css").toExternalForm());

        stage.setTitle("Title");
        stage.setScene(scene);
        stage.show();

        materialCurrencyField.setValue(new BigDecimal("41310.312"));
    }

    @Override public void stop() {
        System.exit(0);
    }

    private HBox createLabelWithControlBox(final String title, final Node node) {
        HBox hBox = new HBox(new Label(title), node);
        hBox.setSpacing(20);
        hBox.setAlignment(Pos.CENTER_LEFT);
        return hBox;
    }
    private Button createButton(final String text, final boolean disabled) {
        Button button = new Button(text);
        button.setDisable(disabled);
        return button;
    }
    private Button createRaisedButton(final String text, final boolean disabled) {
        Button button = new Button(text);
        button.getStyleClass().add("button-raised");
        button.setDisable(disabled);
        return button;
    }
    private Button createFloatingButton(final String text, final boolean disabled) {
        Button button = new Button(text);
        button.getStyleClass().add("button-floating");
        button.setDisable(disabled);
        return button;
    }
    private CheckBox createCheckBox(final String text, final boolean disabled) {
        CheckBox checkBox = new CheckBox(text);
        checkBox.setDisable(disabled);
        return checkBox;
    }
    private MaterialSwitch createSwitch(final String text, final boolean disabled) {
        MaterialSwitch control = new MaterialSwitch(text);
        control.setDisable(disabled);
        return control;
    }
    private RadioButton createRatioButton(final String text, final boolean disabled) {
        RadioButton radioButton = new RadioButton(text);
        radioButton.setDisable(disabled);
        return radioButton;
    }
    private TextField createTextField(final String text, final String promptText, final boolean editable, final boolean disabled) {
        TextField textField = new TextField(text);
        textField.setPromptText(promptText);
        textField.setEditable(editable);
        textField.setDisable(disabled);
        return textField;
    }
    private MaterialTextField createMaterialTextField(final String text, final String promptText, final boolean editable, final boolean disabled) {
        MaterialTextField textField = new MaterialTextField(text);
        textField.setPromptText(promptText);
        textField.setEditable(editable);
        textField.setDisable(disabled);
        return textField;
    }
    private ComboBox<String> createComboBox(final boolean editable, final boolean disabled, final String PROMPT_TEXT, final String... entries) {
        ObservableList<String> items    = FXCollections.observableArrayList(entries);
        ComboBox<String>       comboBox = new ComboBox<>(items);
        comboBox.setPromptText(PROMPT_TEXT);
        comboBox.setEditable(editable);
        comboBox.setDisable(disabled);
        return comboBox;
    }

    private HBox createHBox(final Node... nodes) {
        HBox hBox = new HBox(nodes);
        hBox.setSpacing(20);
        hBox.setPadding(new Insets(10));
        hBox.setAlignment(Pos.CENTER_LEFT);
        return hBox;
    }

    private TableView createTableView() {
        Callback<TableColumn, TableCell> cellFactory = tableColumn -> new EditableTableCell();
        Callback<TableColumn<Person, String>, TableCell<Person, String>> editingCellFactory = (TableColumn<Person, String> p) -> new EditableTableCell<>(new DefaultStringConverter());

        TableColumn<Person, String> firstNameColumn = new TableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Person, String> lastNameColumn = new TableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        lastNameColumn.setEditable(true);
        lastNameColumn.setOnEditCommit(event -> event.getTableView().getItems().get(event.getTablePosition().getRow()).setInfo(event.getNewValue().toString()));

        TableColumn<Person, String> infoColumn = new TableColumn<>("Info");
        infoColumn.setCellValueFactory(new PropertyValueFactory<>("info"));
        infoColumn.setEditable(true);
        //infoColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        infoColumn.setCellFactory(editingCellFactory);
        infoColumn.setOnEditCommit(event -> event.getTableView().getItems().get(event.getTablePosition().getRow()).setLastName(event.getNewValue().toString()));

        TableView<Person> tableView = new TableView<>(FXCollections.observableArrayList(
            new Person("Jonathan", "Giles", "TeamLead JavaFX Controls"),
            new Person("Richard", "Bair", "PM IoT"),
            new Person("Jasper", "Potts", "PM IoT Devices")));
        tableView.getColumns().addAll(firstNameColumn, lastNameColumn, infoColumn);
        tableView.setEditable(true);
        tableView.setOnMouseClicked(mouseEvent -> {

        });

        tableView.setLayoutX(400);
        tableView.setPrefSize(400, 400);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        return tableView;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
