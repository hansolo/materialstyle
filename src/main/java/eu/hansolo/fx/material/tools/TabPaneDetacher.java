/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import com.sun.glass.ui.Application;
import com.sun.glass.ui.Robot;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Transform;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


/**
 * @author Jens Deters (Original Version), Gerrit Grunwald (Reworked Version)
 * @version 1.0.1
 * @since 08-06-2014
 *
 * Usage:
 * TabPaneDetacher.create()
                  .styleSheets(DemoMaterial.class.getResource("../styles.css").toExternalForm())
                  .styles(MaterialDesignColor.UBS_GREY.STYLE_CLASS, MaterialDesignAccentColor.ORANGE.STYLE_CLASS)
                  .makeTabsDetachable(tabPane);
 */
public class TabPaneDetacher {
    private final List<Tab>                 ORIGINAL_TABS;
    private final Map<Integer, Tab>         TAB_TRANSFER_PANE;
    private final BooleanProperty           ALWAYS_ON_TOP;
    private       TabPane                   tabPane;
    private       Tab                       currentTab;
    private       String[]                  stylesheets;
    private       String[]                  styles;
    private       Robot                     robot;


    // ******************** TabPaneDetacher ***********************************
    private TabPaneDetacher() {
        ORIGINAL_TABS     = new ArrayList<>();
        TAB_TRANSFER_PANE = new HashMap<>();
        ALWAYS_ON_TOP     = new SimpleBooleanProperty();
        stylesheets       = new String[]{};
        styles            = new String[]{};

        Platform.runLater(() -> robot = Application.GetApplication().createRobot());
    }


    // ******************** Methods *******************************************
    public static TabPaneDetacher create() { return create(""); }
    public static TabPaneDetacher create(final String... STYLE_SHEETS) {
        final TabPaneDetacher TAB_PANE_DETACHER = new TabPaneDetacher();
        TAB_PANE_DETACHER.styleSheets(STYLE_SHEETS);
        return TAB_PANE_DETACHER;
    }

    public Boolean isAlwaysOnTop() { return ALWAYS_ON_TOP.get(); }
    public void setAlwaysOnTop(final boolean ON_TOP) { ALWAYS_ON_TOP.set(ON_TOP); }
    public TabPaneDetacher alwaysOnTop(boolean alwaysOnTop) {
        alwaysOnTopProperty().set(alwaysOnTop);
        return TabPaneDetacher.this;
    }
    public BooleanProperty alwaysOnTopProperty() { return ALWAYS_ON_TOP; }

    public TabPaneDetacher styleSheets(final String... STYLE_SHEETS) {
        if (null != STYLE_SHEETS && STYLE_SHEETS.length > 0) stylesheets = STYLE_SHEETS;
        return TabPaneDetacher.this;
    }
    public TabPaneDetacher styleSheets(final List<String> STYLE_SHEETS) {
        if (null != STYLE_SHEETS && STYLE_SHEETS.size() > 0) stylesheets = STYLE_SHEETS.toArray(new String[0]);
        return TabPaneDetacher.this;
    }

    public TabPaneDetacher styles(final String... STYLES) {
        if (null != STYLES && STYLES.length > 0) styles = STYLES;
        return TabPaneDetacher.this;
    }
    public TabPaneDetacher styles(final List<String> STYLES) {
        if (null != STYLES && STYLES.size() > 0) styles = STYLES.toArray(new String[0]);
        return TabPaneDetacher.this;
    }

    public TabPaneDetacher makeTabsDetachable(final TabPane TAB_PANE) {
        tabPane = TAB_PANE;

        ORIGINAL_TABS.addAll(TAB_PANE.getTabs());

        for (int i = 0; i < TAB_PANE.getTabs().size(); i++) { TAB_TRANSFER_PANE.put(i, TAB_PANE.getTabs().get(i)); }
        TAB_PANE.getTabs().stream().forEach(t -> t.setClosable(false));
        TAB_PANE.setOnDragDetected(
            (MouseEvent mouseEvent) -> {
                if (mouseEvent.getSource() instanceof TabPane) {
                    Pane rootPane = (Pane) TAB_PANE.getScene().getRoot();
                    rootPane.setOnDragOver((DragEvent dragEvent) -> {
                        dragEvent.acceptTransferModes(TransferMode.ANY);
                        dragEvent.consume();
                    });
                    currentTab = TAB_PANE.getSelectionModel().getSelectedItem();
                    SnapshotParameters snapshotParams = new SnapshotParameters();
                    snapshotParams.setTransform(Transform.scale(0.4, 0.4));
                    WritableImage    snapshot         = currentTab.getContent().snapshot(snapshotParams, null);
                    Dragboard        db               = TAB_PANE.startDragAndDrop(TransferMode.MOVE);
                    ClipboardContent clipboardContent = new ClipboardContent();
                    clipboardContent.put(DataFormat.PLAIN_TEXT, "");
                    db.setDragView(snapshot, 40, 40);
                    db.setContent(clipboardContent);
                }
                mouseEvent.consume();
            });
        TAB_PANE.setOnDragDone(
            (DragEvent dragEvent) -> {
                openTabInStage(currentTab);
                TAB_PANE.setCursor(Cursor.DEFAULT);
                dragEvent.consume();
            });
        return this;
    }

    public void openTabInStage(final Tab TAB) {
        if (TAB == null) return;

        final int ORIGINAL_TAB_INDEX = ORIGINAL_TABS.indexOf(TAB);
        TAB_TRANSFER_PANE.remove(ORIGINAL_TAB_INDEX);
        Pane content = (Pane) TAB.getContent();
        content.getStyleClass().setAll(TAB.getContent().getStyleClass());
        content.getStyleClass().addAll(styles);
        if (content == null) throw new IllegalArgumentException("Can not detach Tab '" + TAB.getText() + "': content is empty (null).");
        TAB.setContent(null);

        Point2D stagePosition = null == robot ? new Point2D(currentTab.getContent().getLayoutX(), currentTab.getContent().getLayoutY()) : new Point2D(robot.getMouseX(), robot.getMouseY());

        final Scene SCENE = new Scene(content, content.getPrefWidth(), content.getPrefHeight());
        SCENE.getStylesheets().addAll(stylesheets);

        final Stage STAGE = new Stage();
        STAGE.setScene(SCENE);
        STAGE.setTitle(TAB.getText());
        STAGE.setAlwaysOnTop(isAlwaysOnTop());
        STAGE.setX(stagePosition.getX());
        STAGE.setY(stagePosition.getY());
        STAGE.setOnCloseRequest((WindowEvent windowEvent) -> {
            STAGE.close();
            TAB.setContent(content);
            final int FORMER_TAB_INDEX = ORIGINAL_TABS.indexOf(TAB);
            TAB_TRANSFER_PANE.put(FORMER_TAB_INDEX, TAB);
            int                index = 0;
            SortedSet<Integer> keys  = new TreeSet<>(TAB_TRANSFER_PANE.keySet());
            for (Integer key : keys) {
                Tab value = TAB_TRANSFER_PANE.get(key);
                if (!tabPane.getTabs().contains(value)) { tabPane.getTabs().add(index, value); }
                index++;
            }
            tabPane.getSelectionModel().select(TAB);
        });
        STAGE.setOnShown((WindowEvent windowEvent) -> TAB.getTabPane().getTabs().remove(TAB));
        STAGE.show();
    }
}
