/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.chart;

import eu.hansolo.fx.material.components.MaterialTableView;
import eu.hansolo.fx.material.tableview.EditableDateTableCell;
import eu.hansolo.fx.material.tableview.EditableTableCell;
import eu.hansolo.fx.material.tableview.SelectableTableCell;
import eu.hansolo.fx.material.tableview.SelectableTableColumn;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.DoublePropertyBase;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.NumberStringConverter;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;


/**
 * Created by hansolo on 01.06.16.
 */
public class TableViewBarChart extends Application {
    private MaterialTableView        tableView;
    private BarChart<String, Number> chart;
    private ChartData                data;

    @Override public void init() {
        data = new ChartData(new Record("Grapefruit", 13),
                             new Record("Oranges", 25),
                             new Record("Plums", 10),
                             new Record("Pears", 22),
                             new Record("Apples", 30));

        XYChart.Series series = new Series();
        series.setName("2015");
        series.getData().setAll(data.getData());

        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis   yAxis = new NumberAxis();
        chart = new BarChart<>(xAxis, yAxis);
        chart.setTitle("Fruits");
        chart.getData().add(series);

        tableView = new MaterialTableView(data.getRecords());
        tableView.setEditable(true);
        tableView.setPrefHeight(200);

        SelectableTableColumn nameColumn = new SelectableTableColumn("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        nameColumn.setCellFactory(p -> new SelectableTableCell<>());
        nameColumn.setPrefWidth(100);

        Callback<TableColumn<Record, Number>, TableCell<Record, Number>> editingCellFactory = (TableColumn<Record, Number> p) -> new EditableTableCell<>(new NumberStringConverter());
        SelectableTableColumn<Record, Number> dataColumn = new SelectableTableColumn<>("Data");
        dataColumn.setCellValueFactory(p -> p.getValue().valueProperty());
        dataColumn.setCellFactory(editingCellFactory);
        dataColumn.setPrefWidth(100);

        tableView.getColumns().addAll(nameColumn, dataColumn);
    }


    @Override public void start(Stage stage) {
        VBox pane = new VBox(chart, tableView);
        pane.setSpacing(20);

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(TableViewPieChart.class.getResource("../styles.css").toExternalForm());

        stage.setScene(scene);
        stage.setTitle("TableView Chart");
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }


    // ******************** Internal Classes **********************************
    public class Record {
        private StringProperty name;
        private DoubleProperty value;


        // ******************** Constructors **********************************
        public Record(final String NAME, final double VALUE) {
            name = new StringPropertyBase(NAME) {
                @Override public Object getBean() { return Record.this; }
                @Override public String getName() { return "name"; }
            };
            value = new DoublePropertyBase(VALUE) {
                @Override public Object getBean() { return Record.this; }
                @Override public String getName() { return "value"; }
            };
        }


        // ******************** Methods ***************************************
        public String getName() { return name.get(); }
        public void setName(final String NAME) { name.set(NAME); }
        public StringProperty nameProperty() { return name; }

        public double getValue() { return value.get(); }
        public void setValue(final double VALUE) { value.set(VALUE); }
        public DoubleProperty valueProperty() { return value; }
    }

    public class ChartData {
        private ObservableList<Record>                       records;
        private ObservableList<XYChart.Data<String, Number>> data;


        // ******************** Constructors **********************************
        public ChartData() {
            this(new Record[]{});
        }
        public ChartData(final Record... RECORDS) {
            records = FXCollections.observableArrayList();
            data    = FXCollections.observableArrayList();
            Arrays.stream(RECORDS).forEach(r -> addRecord(r));
        }


        // ******************** Methods ***************************************
        public ObservableList<Record> getRecords() { return records; }
        public ObservableList<XYChart.Data<String, Number>> getData() { return data; }

        public void addRecord(final Record RECORD) {
            if (records.contains(RECORD)) return;
            records.add(RECORD);
            Data<String, Number> d = new Data();
            d.XValueProperty().bind(RECORD.nameProperty());
            d.YValueProperty().bind(RECORD.valueProperty());
            data.add(d);
        }
        public void removeRecord(final Record RECORD) {
            int i = records.indexOf(RECORD);
            if (i > -1) {
                for (Data<String, Number> d : data) {
                    if (d.getXValue().equals(RECORD.getName()) && Double.compare(RECORD.getValue(), (Double) d.getYValue()) == 0) {
                        d.XValueProperty().unbind();
                        d.YValueProperty().unbind();
                        data.remove(d);
                        break;
                    }
                }
                records.remove(i);
            }
        }
        public void updateRecord(final Record RECORD, final double VALUE) {
            int i = records.indexOf(RECORD);
            if (i > -1) records.get(i).setValue(VALUE);
        }
    }
}

