/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.chart;

import javafx.application.Application;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.scene.Scene;


/**
 * User: hansolo
 * Date: 24.11.16
 * Time: 09:14
 */
public class StackedBarChartDemo extends Application {
    private CategoryAxis    xAxis;
    private NumberAxis      yAxis;
    private StackedBarChart stackedBarChart;


    @Override public void init() {
        xAxis = new CategoryAxis();
        xAxis.setLabel("Category");
        xAxis.getCategories().addAll("Desktop", "Phone", "Tablet");

        yAxis = new NumberAxis();
        yAxis.setLabel("Number");

        XYChart.Series series1 = new Series();
        series1.getData().add(new XYChart.Data("2013", 15));

        XYChart.Series series2 = new Series();
        series2.getData().add(new XYChart.Data("2013", 5));

        XYChart.Series series3 = new Series();
        series3.getData().add(new XYChart.Data("2013", 10));

        stackedBarChart = new StackedBarChart(xAxis, yAxis);
        stackedBarChart.getData().addAll(series1, series2, series3);
    }

    @Override public void start(Stage stage) {
        StackPane pane = new StackPane(stackedBarChart);

        Scene scene = new Scene(pane);

        stage.setTitle("StackedBar Chart");
        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
