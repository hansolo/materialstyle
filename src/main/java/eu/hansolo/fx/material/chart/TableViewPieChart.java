/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.chart;

import eu.hansolo.fx.material.components.MaterialTableView;
import eu.hansolo.fx.material.tableview.EditableTableCell;
import eu.hansolo.fx.material.tableview.SelectableTableCell;
import eu.hansolo.fx.material.tableview.SelectableTableColumn;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.NumberStringConverter;


public class TableViewPieChart extends Application {
    private ObservableList<PieChart.Data> chartData;
    private MaterialTableView             tableView;
    private PieChart                      chart;

    @Override public void init() {
        chartData = FXCollections.observableArrayList(new PieChart.Data("Grapefruit", 13),
                                                      new PieChart.Data("Oranges", 25),
                                                      new PieChart.Data("Plums", 10),
                                                      new PieChart.Data("Pears", 22),
                                                      new PieChart.Data("Apples", 30));

        chart = new PieChart(chartData);
        chart.setTitle("Fruits");

        tableView = new MaterialTableView(chartData);
        tableView.setEditable(true);
        tableView.setPrefHeight(200);

        SelectableTableColumn nameColumn = new SelectableTableColumn("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        nameColumn.setCellFactory(p -> new SelectableTableCell<>());
        nameColumn.setPrefWidth(100);

        Callback<TableColumn, TableCell> editingCellFactory = (TableColumn p) -> new EditableTableCell<>(new NumberStringConverter());
        SelectableTableColumn dataColumn = new SelectableTableColumn("data");
        dataColumn.setCellValueFactory(new PropertyValueFactory<>("pieValue"));
        dataColumn.setCellFactory(editingCellFactory);
        dataColumn.setPrefWidth(100);

        tableView.getColumns().addAll(nameColumn, dataColumn);
    }

    @Override public void start(Stage stage) {
        VBox pane = new VBox(chart, tableView);
        pane.setSpacing(20);

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(TableViewPieChart.class.getResource("../styles.css").toExternalForm());

        stage.setScene(scene);
        stage.setTitle("TableView Chart");
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
