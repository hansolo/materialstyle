/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.chart;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.scene.Scene;


/**
 * User: hansolo
 * Date: 24.11.16
 * Time: 13:17
 */
public class SmoothAreaChartDemo extends Application {
    private SmoothAreaChart<String, Number> smoothAreaChart;

    @Override public void init() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("No of employees");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Revenue per employee");

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("2014");
        series1.getData().add(new XYChart.Data( 1, 567));
        series1.getData().add(new XYChart.Data( 5, 612));
        series1.getData().add(new XYChart.Data(10, 800));
        series1.getData().add(new XYChart.Data(20, 780));
        series1.getData().add(new XYChart.Data(40, 810));
        series1.getData().add(new XYChart.Data(80, 850));

        XYChart.Series series2 = new XYChart.Series();
        series2.setName("2014");
        series2.getData().add(new XYChart.Data( 1, 367));
        series2.getData().add(new XYChart.Data( 5, 412));
        series2.getData().add(new XYChart.Data(10, 500));
        series2.getData().add(new XYChart.Data(20, 480));
        series2.getData().add(new XYChart.Data(40, 610));
        series2.getData().add(new XYChart.Data(80, 350));

        smoothAreaChart = new SmoothAreaChart(xAxis, yAxis, FXCollections.observableArrayList(series1, series2));
        smoothAreaChart.setSelectorColor(Color.MAGENTA);
        smoothAreaChart.setSelectorCircleFill(Color.WHITE);
        //smoothedAreaChart.setSelectedValueDecimals(2);
        smoothAreaChart.setSelectorEnabled(true);
        smoothAreaChart.setAreaVisible(false);
    }

    @Override public void start(Stage stage) {
        StackPane pane = new StackPane(smoothAreaChart);
        pane.setPadding(new Insets(20));

        Scene scene = new Scene(pane);

        stage.setTitle("SmoothAreaChart");
        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
