/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.chart;

import com.sun.javafx.charts.Legend;
import com.sun.javafx.charts.Legend.LegendItem;
import eu.hansolo.fx.material.components.MaterialBarChart;
import eu.hansolo.fx.material.components.MaterialBarChartDataEvent;
import eu.hansolo.fx.material.components.MaterialGlyphButton;
import eu.hansolo.fx.material.components.MaterialStackedBarChart;
import eu.hansolo.fx.material.components.MaterialStackedBarChartDataEvent;
import eu.hansolo.fx.material.components.SideBar;
import eu.hansolo.fx.material.components.SideBar.State;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.DoublePropertyBase;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.WeakEventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Collections;
import java.util.Comparator;
import java.util.Random;


/**
 * Created by hansolo on 16.12.16.
 */
public class DynamicBarChartDemo extends Application {
    private MaterialBarChart          chart;
    private ObservableList<NameValue> nameValueList;
    private StackPane                 master;
    private BorderPane                detail;
    private SideBar                   sideBar;


    @Override public void init() {
        nameValueList = FXCollections.observableArrayList();

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("process name");
        xAxis.setGapStartAndEnd(true);

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Count(work item id)");
        yAxis.setTickUnit(10);
        yAxis.setAutoRanging(false);

        chart = new MaterialBarChart<>(xAxis, yAxis);
        chart.setPrefSize(800, 500);
        chart.setTitle("Processes");
        chart.setCategoryGap(0);
        chart.setAnimated(false);
        chart.getData().setAll(new Series<String, Number>());

        master = new StackPane(chart);
        master.setPadding(new Insets(10));
        master.setPrefSize(800, 600);

        detail = createSidebarContent();
        sideBar = new SideBar(250, detail);
        sideBar.getStyleClass().add("side-bar");
        VBox.setVgrow(detail, Priority.ALWAYS);

        registerListeners();
    }

    private void registerListeners() {
        nameValueList.addListener((ListChangeListener) change -> {
            final Legend                 LEGEND = new Legend();
            final Series<String, Number> SERIES = new Series<>();

            SERIES.setName("My Series");

            // Create chart SERIES and LEGEND
            nameValueList.forEach(process -> {
                SERIES.getData().add(new Data(process.getName(), process.getValue()));
                LEGEND.getItems().add(new LegendItem(process.getName(), new Region()));
            });

            // Set chart SERIES
            chart.getData().setAll(SERIES);

            // Set chart LEGEND
            chart.setNewLegend(LEGEND);

            // Adjust yAxis upper bound to max value
            Comparator<NameValue> cmp = Comparator.comparing(NameValue::getValue);
            ((NumberAxis) chart.getYAxis()).setUpperBound(Collections.max(nameValueList, cmp).getValue() * 1.1);
        });

        chart.addEventHandler(MaterialBarChartDataEvent.BAR_CHART_DATA, e -> {
            if (State.HIDDEN == sideBar.getState()) { sideBar.open(); }
            detail.setCenter(createDetailView(e));
        });
    }

    @Override public void start(final Stage stage) throws Exception {
        MaterialGlyphButton controlButton = sideBar.getControlButton();
        HBox.setMargin(controlButton, new Insets(5, 5, 0, 0));
        HBox                mainPane      = new HBox(5, master, controlButton);

        final BorderPane pane = new BorderPane();
        pane.setRight(sideBar);
        pane.setCenter(mainPane);

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(ListAggregationChart.class.getResource("../styles.css").toExternalForm());

        stage.setTitle("Dynamic Master-Detail");
        stage.setScene(scene);
        stage.show();

        // Add some data to the process list
        nameValueList.add(new NameValue("P0", 12));
        nameValueList.add(new NameValue("P1", 5));
        nameValueList.add(new NameValue("P2", 2));
        nameValueList.add(new NameValue("P3", 15));
        nameValueList.add(new NameValue("P4", 13));
        nameValueList.add(new NameValue("P5", 7));
        nameValueList.add(new NameValue("P6", 9));
        nameValueList.add(new NameValue("P7", 3));
        nameValueList.add(new NameValue("P8", 6));
        nameValueList.add(new NameValue("P9", 4));

        // Exchange a process in the process list
        nameValueList.set(3, new NameValue("P3", 4));
    }

    @Override public void stop() {
        System.exit(0);
    }

    private BorderPane createSidebarContent() {
        final Label text = new Label("DETAIL - PANE");
        text.setMinWidth(0);

        final BorderPane detailPane = new BorderPane();
        detailPane.setMinWidth(0);
        detailPane.getStyleClass().add("details");
        detailPane.setCenter(text);

        return detailPane;
    }

    private Node createDetailView(final MaterialBarChartDataEvent DATA_EVENT) {
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Detail");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Sections");

        String name    = DATA_EVENT.getData().getXValue().toString();
        double value   = Double.parseDouble(DATA_EVENT.getData().getYValue().toString());
        Color topColor = DATA_EVENT.getTopColor();

        double[] randomSectionValues = divideUniformlyRandomly(value, 3);

        // Create a series for each section of the selected bar
        Series series1 = new Series();
        series1.getData().add(new Data(name, randomSectionValues[0]));

        Series series2 = new Series();
        series2.getData().add(new Data(name, randomSectionValues[1]));

        Series series3 = new Series();
        series3.getData().add(new Data(name, randomSectionValues[2]));

        MaterialStackedBarChart stackedBarChart = new MaterialStackedBarChart(xAxis, yAxis);
        stackedBarChart.setMinWidth(0);
        stackedBarChart.setTitle("Details");
        stackedBarChart.setLegendVisible(false);
        stackedBarChart.setAnimated(false);
        stackedBarChart.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        stackedBarChart.getData().addAll(series1, series2, series3);
        stackedBarChart.setStyle("CHART_COLOR_1: " + topColor.toString().replace("0x", "#") +
                                 ";CHART_COLOR_2: " + topColor.toString().replace("0x", "#") +
                                 ";CHART_COLOR_3: " + topColor.toString().replace("0x", "#") +
                                 ";CHART_COLOR_4: " + topColor.toString().replace("0x", "#") +
                                 ";CHART_COLOR_5: " + topColor.darker().toString().replace("0x", "#") + ";");

        StackPane content = new StackPane(stackedBarChart);
        content.setMinWidth(0);
        content.setPadding(new Insets(10));
        AnchorPane.setTopAnchor(content, 0d);
        AnchorPane.setRightAnchor(content, 0d);

        // Add mouse handler to the StackedBarChart
        stackedBarChart.addEventHandler(MaterialStackedBarChartDataEvent.STACKED_SECTION_DATA, new WeakEventHandler<>(e -> System.out.println(e.getData().getXValue() + " : " + e.getData().getYValue())));

        return content;
    }

    public double[] divideUniformlyRandomly(final double NUMBER, final int PARTS) {
        Random random           = new Random();
        double uniformRandoms[] = new double[PARTS];
        double mean             = NUMBER / PARTS;
        double sum              = 0.0;

        for (int i=0; i<PARTS / 2; i++) {
            uniformRandoms[i] = random.nextDouble() * mean;
            uniformRandoms[PARTS - i - 1] = mean + random.nextDouble() * mean;
            sum += uniformRandoms[i] + uniformRandoms[PARTS - i -1];
        }
        uniformRandoms[(int)Math.ceil(PARTS/2)] = uniformRandoms[(int)Math.ceil(PARTS/2)] + NUMBER - sum;
        return uniformRandoms;
    }

    public static void main(String[] args) throws Exception { launch(args); }


    private class NameValue {
        private StringProperty name;
        private DoubleProperty value;

        public NameValue(final String NAME, final double VALUE) {
            name  = new StringPropertyBase(NAME) {
                @Override public Object getBean() { return NameValue.this; }
                @Override public String getName() { return "name"; }
            };
            value = new DoublePropertyBase(VALUE) {
                @Override public Object getBean() { return NameValue.this; }
                @Override public String getName() { return "value"; }
            };
        }

        public String getName() { return name.get(); }
        public void setName(final String NAME) { name.set(NAME); }
        public StringProperty nameProperty() { return name; }

        public double getValue() { return value.get(); }
        public void setValue(final double VALUE) { value.set(VALUE); }
        public DoubleProperty valueProperty() { return value; }
    }
}
