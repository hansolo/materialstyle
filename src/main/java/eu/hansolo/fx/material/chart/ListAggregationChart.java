/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.chart;

import com.sun.javafx.charts.Legend;
import com.sun.javafx.charts.Legend.LegendItem;
import eu.hansolo.fx.material.components.MaterialBarChart;
import eu.hansolo.fx.material.components.MaterialTableView;
import eu.hansolo.fx.material.tableview.EditableTableCell;
import eu.hansolo.fx.material.tableview.SelectableTableCell;
import eu.hansolo.fx.material.tableview.SelectableTableColumn;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.IntegerPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.LocalDateTimeStringConverter;
import javafx.util.converter.NumberStringConverter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * User: hansolo
 * Date: 02.06.16
 * Time: 08:00
 */
public class ListAggregationChart extends Application {
    public enum Status { ALLOCATED, CANCELLED, COMPLETED, OFFERED, OPENED, PENDED }
    public enum ProcessName { EntscheidVorleistungsPflichtIV, RevisionIVFall, MailroomProcess, Todesfall, VTMutationIVFall, InvaliditaetsFall, VerrechnungenIVFall, MutationTodesfall, KorrekturIVFall }
    public enum Filter { STATUS, PROCESS_NAME, WORK_ITEM_ID }

    private Filter                                 filter;
    private ObservableList<Process>                processes;
    private ObservableList<Series<String, Number>> seriesList;
    private CategoryAxis                           xAxis;
    private NumberAxis                             yAxis;
    private MaterialBarChart                       chart;
    private MaterialTableView                      tableView;
    private StringConverter<Status>                statusStringConverter;
    private StringConverter<ProcessName>           processNameStringConverter;


    public ListAggregationChart() {
        filter                = Filter.WORK_ITEM_ID;
        processes             = FXCollections.observableArrayList();
        seriesList            = FXCollections.observableArrayList();
        statusStringConverter = new StringConverter<Status>() {
            @Override public String toString(final Status STATUS) { return STATUS.name(); }
            @Override public Status fromString(final String STATUS) { return Status.valueOf(STATUS.toUpperCase()); }
        };
        processNameStringConverter = new StringConverter<ProcessName>() {
            @Override public String toString(final ProcessName PROCESS_NAME) { return PROCESS_NAME.name(); }
            @Override public ProcessName fromString(final String PROCESS_NAME) { return ProcessName.valueOf(PROCESS_NAME.toUpperCase()); }
        };

        registerListeners();
    }


    @Override public void init() {
        xAxis = new CategoryAxis();
        xAxis.setLabel("process name");
        xAxis.setGapStartAndEnd(true);

        yAxis = new NumberAxis();
        yAxis.setLabel("Count(work item id)");
        yAxis.setTickUnit(10);
        yAxis.setAutoRanging(false);

        chart = new MaterialBarChart<>(xAxis, yAxis);
        chart.setPrefSize(800, 500);
        chart.setTitle("Processes");
        chart.setCategoryGap(0);
        chart.setAnimated(false);
        chart.setData(seriesList);

        tableView = createTableView();
    }

    private void registerListeners() {
        processes.addListener((ListChangeListener<Process>) c -> aggregateData());
    }

    private void aggregateData() {
        Legend        legend       = new Legend();
        List<Process> listDistinct;

        Series<String, Number> series = new Series<>();

        Map<?, Long> aggregated;
        switch(filter) {
            case WORK_ITEM_ID:
                aggregated = processes.stream().collect(Collectors.groupingBy(Process::getWorkItemId, Collectors.counting()));
                aggregated.forEach((object, quantity) -> {
                    series.setName(object.toString());
                    series.getData().add(new Data(object.toString(), quantity));
                });
                listDistinct = processes.stream().filter(distinctByKey(p -> p.getWorkItemId())).collect(Collectors.toList());
                listDistinct.forEach(process -> legend.getItems().add(new LegendItem(process.getWorkItemId(), new Region())));
                break;
            case STATUS:
                aggregated = processes.stream().collect(Collectors.groupingBy(Process::getStatus, Collectors.counting()));
                aggregated.forEach((object, quantity) -> {
                    series.setName(((Status) object).name());
                    series.getData().add(new Data(((Status) object).name(), quantity));
                });
                listDistinct = processes.stream().filter(distinctByKey(p -> p.getStatus())).collect(Collectors.toList());
                listDistinct.forEach(process -> legend.getItems().add(new LegendItem(process.getStatus().name(), new Region())));
                break;
            case PROCESS_NAME:
            default:
                aggregated = processes.stream().collect(Collectors.groupingBy(Process::getName, Collectors.counting()));
                aggregated.forEach((object, quantity) -> {
                    series.setName(((ProcessName) object).name());
                    series.getData().add(new Data(((ProcessName) object).name(), quantity));
                });
                listDistinct = processes.stream().filter(distinctByKey(p -> p.getName())).collect(Collectors.toList());
                listDistinct.forEach(process -> legend.getItems().add(new LegendItem(process.getName().name(), new Region())));
                break;
        }
        seriesList.setAll(series);

        // Create the legend
        chart.setNewLegend(legend);

        // Adjust yAxis upper bound to max value
        yAxis.setUpperBound(aggregated.values().stream().max(Long::compare).get() + 5);
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T,Object> keyExtractor) {
        Map<Object,Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    private void addData() {
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 43074, Status.OFFERED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "duma", 43087, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 43935, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 45379, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 45380, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 52776, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "duma", 64405, Status.PENDED);
        createAndAddProcess(ProcessName.RevisionIVFall, "dech", 67719, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 70120, Status.OPENED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "duma", 72864, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 77771, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 82372, Status.PENDED);
        createAndAddProcess(ProcessName.MailroomProcess, "caje", 84140, Status.OPENED);
        createAndAddProcess(ProcessName.Todesfall, "suga", 86667, Status.PENDED);
        createAndAddProcess(ProcessName.MailroomProcess, "rube", 86797, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "duma", 88580, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "duma", 88582, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "duma", 88583, Status.PENDED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 88585, Status.PENDED);
        createAndAddProcess(ProcessName.VTMutationIVFall, "lich", 93608, Status.ALLOCATED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 90845, Status.OPENED);
        createAndAddProcess(ProcessName.EntscheidVorleistungsPflichtIV, "bage", 90853, Status.PENDED);
        createAndAddProcess(ProcessName.InvaliditaetsFall, "lich", 93608, Status.ALLOCATED);
        createAndAddProcess(ProcessName.InvaliditaetsFall, "alfi", 99209, Status.PENDED);
        createAndAddProcess(ProcessName.VTMutationIVFall, "braf", 100384, Status.PENDED);
        createAndAddProcess(ProcessName.InvaliditaetsFall, "alfi", 105204, Status.PENDED);
        createAndAddProcess(ProcessName.InvaliditaetsFall, "duma", 107545, Status.OFFERED);
    }

    private void createAndAddProcess(final ProcessName NAME, final String USER_ID, final int WORK_ITEM_ID, final Status STATUS) {
        Process process = new Process(NAME, USER_ID, WORK_ITEM_ID);
        process.setStatus(STATUS);

        processes.add(process);
    }

    private MaterialTableView createTableView() {
        Callback<TableColumn<Process, String>, TableCell<Process, String>> editingCellFactory = (TableColumn<Process, String> p) -> new EditableTableCell<>(new DefaultStringConverter());

        SelectableTableColumn<Process, LocalDateTime> firstOfferTimeColumn = new SelectableTableColumn<>("First Offer Time");
        firstOfferTimeColumn.setCellValueFactory(p -> p.getValue().firstOfferTimestampProperty());
        firstOfferTimeColumn.setCellFactory(p -> new SelectableTableCell<>(new LocalDateTimeStringConverter()));

        SelectableTableColumn<Process, LocalDateTime> lastOpenTimeColumn = new SelectableTableColumn<>("Last Open Time");
        lastOpenTimeColumn.setCellValueFactory(p -> p.getValue().lastOpenTimestampProperty());
        lastOpenTimeColumn.setCellFactory(p -> new SelectableTableCell<>(new LocalDateTimeStringConverter()));

        SelectableTableColumn<Process, String> processInstanceColumn = new SelectableTableColumn<>("Process Instance");
        processInstanceColumn.setCellValueFactory(p -> p.getValue().instanceProperty());
        processInstanceColumn.setCellFactory(editingCellFactory);

        SelectableTableColumn<Process, ProcessName> processNameColumn = new SelectableTableColumn<>("Process Name");
        processNameColumn.setCellValueFactory(p -> p.getValue().nameProperty());
        processNameColumn.setCellFactory(p -> new SelectableTableCell<>(processNameStringConverter));

        SelectableTableColumn<Process, LocalDateTime> scheduledEndTimeColumn = new SelectableTableColumn<>("Scheduled End Time");
        scheduledEndTimeColumn.setCellValueFactory(p -> p.getValue().scheduledEndTimestamp);
        scheduledEndTimeColumn.setCellFactory(p -> new SelectableTableCell<>(new LocalDateTimeStringConverter()));

        SelectableTableColumn<Process, Status> statusColumn = new SelectableTableColumn<>("Status");
        statusColumn.setCellValueFactory(p -> p.getValue().statusProperty());
        statusColumn.setCellFactory(p -> new SelectableTableCell<>(statusStringConverter));

        SelectableTableColumn<Process, String> userIdColumn = new SelectableTableColumn<>("User ID");
        userIdColumn.setCellValueFactory(p -> p.getValue().userIdProperty());
        userIdColumn.setCellFactory(editingCellFactory);

        SelectableTableColumn<Process, String> workItemIdColumn = new SelectableTableColumn<>("Work Item ID");
        workItemIdColumn.setCellValueFactory(p -> p.getValue().workItemIdProperty());
        workItemIdColumn.setCellFactory(p -> new SelectableTableCell<>(new DefaultStringConverter()));

        MaterialTableView<Process> tableView = new MaterialTableView<>(processes);
        tableView.getColumns().addAll(firstOfferTimeColumn, lastOpenTimeColumn, processNameColumn, scheduledEndTimeColumn, statusColumn, userIdColumn, workItemIdColumn);
        tableView.setEditable(true);

        tableView.getFocusModel().focusedCellProperty().addListener(o -> System.out.println("Focused Cell: " + tableView.getFocusModel().getFocusedCell().getColumn() + ", " + tableView.getFocusModel().getFocusedCell().getRow()));

        tableView.setPrefSize(600, 400);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        return tableView;
    }
    
    @Override public void start(Stage stage) {
        VBox pane = new VBox(chart, tableView);
        pane.setSpacing(20);
        pane.setPadding(new Insets(20));

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(ListAggregationChart.class.getResource("../styles.css").toExternalForm());

        stage.setTitle("Title");
        stage.setScene(scene);
        stage.show();

        addData();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }


    // ******************** Internal Classes **********************************
    public class Process {
        private ObjectProperty<LocalDateTime> firstOfferTimestamp;
        private ObjectProperty<LocalDateTime> lastOpenTimestamp;
        private StringProperty                instance;
        private ObjectProperty<ProcessName>   name;
        private ObjectProperty<LocalDateTime> scheduledEndTimestamp;
        private ObjectProperty<Status>        status;
        private StringProperty                userId;
        private StringProperty                workItemId;


        // ******************** Constructors **********************************
        public Process(final ProcessName NAME, final String USER_ID, final int WORK_ITEM_ID) {
            firstOfferTimestamp   = new ObjectPropertyBase<LocalDateTime>(LocalDateTime.now()) {
                @Override public Object getBean() { return Process.this; }
                @Override public String getName() { return "firstOpenTimestamp"; }
            };
            lastOpenTimestamp     = new ObjectPropertyBase<LocalDateTime>(LocalDateTime.now()) {
                @Override public Object getBean() { return Process.this; }
                @Override public String getName() { return "lastOpenTimestamp"; }
            };
            instance              = new StringPropertyBase("") {
                @Override public Object getBean() { return Process.this; }
                @Override public String getName() { return "instance"; }
            };
            name                  = new ObjectPropertyBase<ProcessName>(NAME) {
                @Override public Object getBean() { return Process.this; }
                @Override public String getName() { return "name"; }
            };
            scheduledEndTimestamp = new ObjectPropertyBase<LocalDateTime>() {
                @Override public Object getBean() { return Process.this; }
                @Override public String getName() { return "scheduledEndTimestamp"; }
            };
            status                = new ObjectPropertyBase<Status>(Status.OPENED) {
                @Override public Object getBean() { return Process.this; }
                @Override public String getName() { return "status"; }
            };
            userId                = new StringPropertyBase(USER_ID) {
                @Override public Object getBean() { return Process.this; }
                @Override public String getName() { return "userId"; }
            };
            workItemId            = new StringPropertyBase(Integer.toString(WORK_ITEM_ID)) {
                @Override public Object getBean() { return Process.this; }
                @Override public String getName() { return "workItemId"; }
            };
        }


        // ******************** Methods ***************************************
        public LocalDateTime getFirstOfferTimestamp() { return firstOfferTimestamp.get(); }
        public void setFirstOfferTimestamp(final LocalDateTime TIMESTAMP) { firstOfferTimestamp.set(TIMESTAMP); }
        public ObjectProperty<LocalDateTime> firstOfferTimestampProperty() { return firstOfferTimestamp; }

        public LocalDateTime getLastOpenTimestamp() { return lastOpenTimestamp.get(); }
        public void setLastOpenTimestamp(final LocalDateTime TIMESTAMP) { lastOpenTimestamp.set(TIMESTAMP); }
        public ObjectProperty<LocalDateTime> lastOpenTimestampProperty() { return lastOpenTimestamp; }

        public String getInstance() { return instance.get(); }
        public void setInstance(final String INSTANCE) { instance.set(INSTANCE); }
        public StringProperty instanceProperty() { return instance; }

        public ProcessName getName() { return name.get(); }
        public void setName(final ProcessName NAME) { name.set(NAME); }
        public ObjectProperty<ProcessName> nameProperty() { return name; }

        public LocalDateTime getScheduledEndTimestamp() { return scheduledEndTimestamp.get(); }
        public void setScheduledEndTimestamp(final LocalDateTime TIMESTAMP) { scheduledEndTimestamp.set(TIMESTAMP); }
        public ObjectProperty<LocalDateTime> scheduledEndTimestampProperty() { return scheduledEndTimestamp; }

        public Status getStatus() { return status.get(); }
        public void setStatus(final Status STATUS) { status.set(STATUS); }
        public ObjectProperty<Status> statusProperty() { return status; }

        public String getUserId() { return userId.get(); }
        public void setUserId(final String ID) { userId.set(ID); }
        public StringProperty userIdProperty() { return userId; }

        public String getWorkItemId() { return workItemId.get(); }
        public void setWorkItemId(final int ID) { workItemId.set(Integer.toString(ID)); }
        public StringProperty workItemIdProperty() { return workItemId; }
    }
}
