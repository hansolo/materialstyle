/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.chart;

import eu.hansolo.fx.material.components.MaterialPieChartDataEvent;
import eu.hansolo.fx.material.components.MaterialPieChart;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 * Created by hansolo on 24.11.16.
 */
public class PieChartDemo extends Application {
    private MaterialPieChart pieChart;


    @Override public void init() {
        pieChart = new MaterialPieChart(getData());
        pieChart.setTitle("TIOBE 02/2013");
        pieChart.setLegendSide(Side.LEFT);
        //pieChart.setLabelLineLength(10);
        pieChart.setAnimated(true);
        pieChart.addEventHandler(MaterialPieChartDataEvent.PIE_CHART_DATA, evt -> {
            System.out.println("Selected data: " + evt.getData());
        });
    }

    @Override public void start(final Stage stage) {
        final StackPane pane = new StackPane(pieChart);
        pane.setPadding(new Insets(20));

        Scene scene = new Scene(pane, 1024, 300);
        scene.getStylesheets().add(PieChartDemo.class.getResource("pie-chart-styles.css").toExternalForm());

        stage.setScene(scene);
        stage.setTitle("Demo PieChart");
        stage.show();
    }

    private ObservableList<Data> getData() {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
        pieChartData.add(new PieChart.Data("Java", 18.387));
        pieChartData.add(new PieChart.Data("C", 17.080));
        pieChartData.add(new PieChart.Data("Objective-C", 9.803));
        pieChartData.add(new PieChart.Data("C++", 8.758));
        pieChartData.add(new PieChart.Data("C#", 6.680));
        pieChartData.add(new PieChart.Data("PHP", 5.074));
        pieChartData.add(new PieChart.Data("Python", 4.949));
        pieChartData.add(new PieChart.Data("Others", 29.269));
        return pieChartData;
    }

    public static void main(final String[] arguments) {
        launch(arguments);
    }
}