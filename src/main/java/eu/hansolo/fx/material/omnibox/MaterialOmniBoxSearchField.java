/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.omnibox;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.layout.Region;


/**
 * Created by hansolo on 15.04.16.
 */
public class MaterialOmniBoxSearchField extends OmniBoxTextField {
    private DoubleProperty translateX = new SimpleDoubleProperty(188);


    /**
     * Constructs a new instance.
     */
    public MaterialOmniBoxSearchField() {
        getStylesheets().add(OmniBoxSearchField.class.getResource("omnibox.css").toExternalForm());
        getStyleClass().setAll("omni-box-search-field");
        setWidth(188);

        translateXProperty().bind(translateX);

        Region loupe = new Region();
        loupe.getStyleClass().add("search-icon-svg");
        loupe.setOnMouseClicked(event -> { if (getTranslateX() > 0) translateX.set(-3); });
        loupe.setPickOnBounds(true);
        setLeft(loupe);

        Region cross = new Region();
        cross.getStyleClass().add("cross-icon-svg");
        cross.setOnMousePressed(event -> translateX.set(getWidth() - 32));
        cross.setPickOnBounds(true);
        setRight(cross);
    }
}
