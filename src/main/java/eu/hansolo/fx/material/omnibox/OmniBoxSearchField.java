package eu.hansolo.fx.material.omnibox;

import javafx.scene.control.Label;

/**
 * A specialized textfield used for searches. The field displays an icon on its
 * left-hand side to indicate that it can be used for searches.
 *
 * @author Dirk Lemmermann
 */
public class OmniBoxSearchField extends OmniBoxTextField {

	/**
	 * Constructs a new instance.
	 */
	public OmniBoxSearchField() {
		getStylesheets().add(OmniBoxSearchField.class.getResource("omnibox.css").toExternalForm());
		Label icon = new Label();
		icon.getStyleClass().add("search-icon");
		setLeft(icon);
	}
}
