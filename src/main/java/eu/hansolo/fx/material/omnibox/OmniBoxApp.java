package eu.hansolo.fx.material.omnibox;

import eu.hansolo.fx.material.components.DemoMaterial;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * A small test application for verifying the functionality of {@link OmniBox},
 * {@link OmniBoxTextField}, and {@link OmniBoxSearchField}.
 *
 * @author Dirk Lemmermann
 */
public class OmniBoxApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox box = new VBox(20);

        OmniBoxTextField omniBoxTextField1 = new OmniBoxTextField();
        omniBoxTextField1.setPromptText("Search 1");

        OmniBoxSearchField omniBoxTextField2 = new OmniBoxSearchField();
        omniBoxTextField2.setPromptText("Search 2 (Auto search off)");
        omniBoxTextField2.setAutoSearch(false);

        Label selectedLabel = new Label("Selected: ---");

        omniBoxTextField1.getOmniBox().selectedSearchResultProperty().addListener(it -> {
            SearchResult<?> result = omniBoxTextField1.getOmniBox().getSelectedSearchResult();
            selectedLabel.setText("Selected: " + result.getText());
        });

        VBox.setVgrow(omniBoxTextField1, Priority.NEVER);
        VBox.setVgrow(selectedLabel, Priority.NEVER);
        VBox.setVgrow(omniBoxTextField2, Priority.NEVER);

        VBox.setMargin(omniBoxTextField1, new Insets(20));
        VBox.setMargin(selectedLabel, new Insets(20));
        VBox.setMargin(omniBoxTextField2, new Insets(20));

        DummySearchService google = new DummySearchService("Google", null);
        DummySearchService yahoo = new DummySearchService("Yahoo", null);
        DummySearchService bing = new DummySearchService("Bing", null);

        omniBoxTextField1.getOmniBoxServices().addAll(google, yahoo, bing);
        omniBoxTextField2.getOmniBoxServices().addAll(google);

        box.getChildren().addAll(omniBoxTextField1, selectedLabel, omniBoxTextField2);

        Scene scene = new Scene(box);
        scene.getStylesheets().add(DemoMaterial.class.getResource("../styles.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
