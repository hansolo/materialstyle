package eu.hansolo.fx.material.omnibox;

import java.util.*;
import java.util.logging.Logger;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * A dummy search service implementation used for testing.
 *
 * @see OmniBox#getOmniBoxServices()
 *
 * @author Dirk Lemmermann
 */
public class DummySearchService implements OmniBoxService<String> {

    private static final Logger LOG = Logger.getLogger(DummySearchService.class.getName());

    private String name;
    private Image icon;

    private Image icon1 = new Image(DummySearchService.class.getResourceAsStream("document_gear.png"));
    private Image icon2 = new Image(DummySearchService.class.getResourceAsStream("document_heart-1.png"));
    private Image icon3 = new Image(DummySearchService.class.getResourceAsStream("document_pulse.png"));

    private Image badge1 = new Image(DummySearchService.class.getResourceAsStream("heart.png"));
    private Image badge2 = new Image(DummySearchService.class.getResourceAsStream("star.png"));
    private Image badge3 = new Image(DummySearchService.class.getResourceAsStream("pencil.png"));

    public DummySearchService(String name) {
        this(name, null);
    }

    public DummySearchService(String name, Image icon) {
        this.name = Objects.requireNonNull(name);
        this.icon = icon;
    }

    @Override
    public String getServiceName() {
        return name;
    }

    @Override
    public Image getServiceIcon() {
        return icon;
    }

    @Override
    public List<SearchResult<String>> search(SearchContext ctx, String pattern) {
        try {
            Thread.sleep((long)(Math.random() * 3000));
        } catch (InterruptedException e) {
            // don't do anything
        }

        if (!ctx.isCancelled()) {
            List<SearchResult<String>> results = new ArrayList<>();

            int resultCount = (int) (Math.random() * 20);
            LOG.fine("creating " + resultCount + " results in service " + getServiceName());

            for (int i = 0; i < resultCount; i++) {
                if (!ctx.isCancelled()) {
                    results.add(new SearchResult<>(name + ", result " + i, new ImageView(getIcon()), "Category 1",
                                                   Arrays.stream(getBadges()).map(ImageView::new).toArray(ImageView[]::new)));
                } else {
                    LOG.fine(getServiceName() + " search was cancelled");
                }
            }

            return results;
        } else {
            LOG.fine(getServiceName() + " search was cancelled");
        }

        return Collections.emptyList();
    }

    private Image getIcon() {
        switch (((int) (Math.random() * 3))) {
            case 0:
                return icon1;
            case 1:
                return icon2;
            case 2:
            default:
                return icon3;
        }
    }

    private Image[] getBadges() {
        switch (((int) (Math.random() * 4))) {
            case 0:
                return new Image[]{badge1};
            case 1:
                return new Image[]{badge2, badge3};
            case 2:
                return new Image[]{badge1, badge2, badge3};
            case 3:
            default:
                return new Image[0];
        }
    }
}
