package eu.hansolo.fx.material.omnibox;

import java.text.MessageFormat;
import java.util.Objects;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Skin;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * Skin class of {@link OmniBox}.
 *
 * @author Dirk Lemmermann
 */
public class OmniBoxSkin implements Skin<OmniBox> {

	private OmniBox box;
	private VBox vbox;
	private ListView<?> listView;

	public OmniBoxSkin(OmniBox box) {
		this.box = Objects.requireNonNull(box);

		listView = box.getListView();
		listView.setMinHeight(0);
		listView.setMaxWidth(Double.MAX_VALUE);

		this.vbox = new VBox();
		this.vbox.getStyleClass().add("omnibox");

		vbox.getChildren().add(listView);
		vbox.getStylesheets().add(OmniBoxSkin.class.getResource("omnibox.css").toExternalForm());

		Label label = new Label("...");
		label.setMaxWidth(Double.MAX_VALUE);
		label.getStyleClass().add("result-counter");
		vbox.getChildren().add(label);

		VBox.setVgrow(listView, Priority.ALWAYS);
		VBox.setVgrow(label, Priority.NEVER);

		box.searchResultCountProperty().addListener(it -> label.setText(MessageFormat.format("{0} Items", box.getSearchResultCount())));
	}

	@Override
	public OmniBox getSkinnable() {
		return box;
	}

	@Override
	public Node getNode() {
		return vbox;
	}

	@Override
	public void dispose() {
	}
}
