package eu.hansolo.fx.material.omnibox;

import javafx.concurrent.Task;

/**
 * An interface used for giving the search services access to the search
 * context. The context is usually defined by the {@link Task} that is executing
 * the services, hence the context allows the service to check if the task has
 * been cancelled.
 *
 * @author Dirk Lemmermann
 */
public interface SearchContext {

	/**
	 * Returns true if the background search task was cancelled.
	 *
	 * @return true if the search has been cancelled
	 */
	boolean isCancelled();
}
