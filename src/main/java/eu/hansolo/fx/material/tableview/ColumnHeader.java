/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.tableview;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.css.PseudoClass;
import javafx.scene.control.Label;


/**
 * Created by hansolo on 02.05.16.
 */
public class ColumnHeader extends Label {
    private static final PseudoClass     SELECTED_COLUMN_PSEUDO_CLASS = PseudoClass.getPseudoClass("selected-column-header");
    private              BooleanProperty selectedColumnHeader;


    //******************** Constructors ***************************************
    public ColumnHeader() {
        this("");
    }
    public ColumnHeader(final String TEXT) {
        super(TEXT);

        getStyleClass().add("selectable-column-header");
        selectedColumnHeader = new BooleanPropertyBase(false) {
            @Override protected void invalidated() { pseudoClassStateChanged(SELECTED_COLUMN_PSEUDO_CLASS, get()); }
            @Override public Object getBean() { return ColumnHeader.this; }
            @Override public String getName() { return "selectedColumnHeader"; }
        };
    }


    //******************** Methods ********************************************
    public boolean getSelectedColumnHeader() { return selectedColumnHeader.get(); }
    public void setSelectedColumnHeader(final boolean SELECTED) { selectedColumnHeader.set(SELECTED); }
    public BooleanProperty selectedColumnHeaderProperty() { return selectedColumnHeader; }
}
