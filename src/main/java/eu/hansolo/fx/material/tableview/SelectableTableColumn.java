/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.tableview;

import javafx.beans.InvalidationListener;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;


/**
 * Created by hansolo on 02.05.16.
 */
public class SelectableTableColumn<S, T> extends TableColumn<S, T> {
    private ColumnHeader         header;
    private InvalidationListener listener;

    //******************** Constructors ***************************************
    public SelectableTableColumn() {
        this("");
    }
    public SelectableTableColumn(final String TEXT) {
        super();
        header = new ColumnHeader(TEXT);
        setGraphic(header);
        registerListeners();
    }


    //******************** Initialization *************************************
    private void registerListeners() {
        textProperty().addListener(o -> header.setText(getText()));

        listener = o -> {
            TablePosition pos    = getTableView().getFocusModel().getFocusedCell();
            TableColumn   column = pos.getTableColumn();
            if (null == column) return;
            header.setSelectedColumnHeader(column.equals(SelectableTableColumn.this));
        };

        tableViewProperty().addListener(o -> {
            TableView tableView = getTableView();
            if (null == tableView) return;
            tableView.getFocusModel().focusedCellProperty().addListener(new WeakInvalidationListener(listener));
        });
    }


    //******************** Methods ********************************************
    public String getHeaderText() { return header.getText(); }
    public void setHeaderText(final String TEXT) { header.setText(TEXT); }
    public StringProperty headerTextProperty() { return header.textProperty(); }
}
