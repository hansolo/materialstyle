/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.tableview;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.scene.control.ContentDisplay;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;


/**
 * Created by hansolo on 02.05.16.
 */
public class SelectableTableCell<S, T> extends MaterialTableCell<S, T> {
    private ObjectProperty<StringConverter<T>> converter;


    //******************** Constructors ***************************************
    public SelectableTableCell() {
        this(null);
    }
    public SelectableTableCell(final StringConverter<T> CONVERTER) {
        getStyleClass().add("selectable-cell");
        converter = new ObjectPropertyBase<StringConverter<T>>((StringConverter<T>) new DefaultStringConverter()) {
            @Override public Object getBean() { return SelectableTableCell.this; }
            @Override public String getName() { return "converter"; }
        };

        if (CONVERTER != null) setConverter(CONVERTER);
    }


    //******************** Methods ********************************************
    public StringConverter<T> getConverter() { return converterProperty().get(); }
    public void setConverter(final StringConverter CONVERTER) { converterProperty().set(CONVERTER); }
    public ObjectProperty<StringConverter<T>> converterProperty() { return converter; }

    @Override public void updateItem(final T ITEM, final boolean EMPTY) {
        super.updateItem(ITEM, EMPTY);
        if (EMPTY) {
            setText(null);
            setGraphic(null);
        } else {
            setText(getString());
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
    }

    private String getString() {
        return getConverter() == null ? getItem() == null ? "" : getItem().toString() : getConverter().toString(getItem());
    }
}
