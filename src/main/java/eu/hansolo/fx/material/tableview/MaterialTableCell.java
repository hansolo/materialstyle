/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.fx.material.tableview;

import javafx.beans.InvalidationListener;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.css.PseudoClass;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;


/**
 * Created by hansolo on 08.07.16.
 */
public abstract class MaterialTableCell<S, T> extends TableCell<S, T> {
    private static final PseudoClass SELECTED_CELL_PSEUDO_CLASS = PseudoClass.getPseudoClass("selected-cell");
    BooleanProperty      selectedCell;
    TablePosition        tablePos;
    InvalidationListener listener;


    //******************** Constructors ***************************************
    MaterialTableCell() {
        super();
        selectedCell = new BooleanPropertyBase(false) {
            @Override protected void invalidated() { pseudoClassStateChanged(SELECTED_CELL_PSEUDO_CLASS, get()); }
            @Override public Object getBean() { return MaterialTableCell.this; }
            @Override public String getName() { return "selectedCell"; }
        };
        listener = o -> {
            TablePosition pos    = getTableView().getFocusModel().getFocusedCell();
            TableRow      row    = getTableRow();
            TableColumn   column = pos.getTableColumn();
            if (null == row || null == getTableColumn() || null == column) return;
            selectedCell.set(column.equals(getTableColumn()) && row.getIndex() == pos.getRow());
        };

        registerListeners();
    }


    //******************** Initialization *************************************
    private void registerListeners() {
        tableViewProperty().addListener(o -> {
            TableView tableView = getTableView();
            if (null == tableView) return;
            tableView.getFocusModel().focusedCellProperty().addListener(new WeakInvalidationListener(listener));
            if (isEditable()) tableView.editingCellProperty().addListener(c -> tablePos = getTableView().getEditingCell());
        });
    }


    //******************** Methods ********************************************
    public boolean isSelectedCell() { return selectedCell.get(); }
    public void setSelectedCell(final boolean IS_SELECTED) { selectedCell.set(IS_SELECTED); }
    public BooleanProperty selectedCellProperty() { return selectedCell; }
}
